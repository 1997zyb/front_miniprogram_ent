let tdweapp = require('./utils/tdweapp.js');

App({
  onLaunch: function() {
    wx.setStorageSync('im_connect', false)
    const updateManager = wx.getUpdateManager()
    updateManager.onCheckForUpdate(function(res) {
      //  请求完新版本信息的回调
    })
    updateManager.onUpdateReady(function() {
      wx.showModal({
        title: '更新提示',
        content: '新版本已经准备好，是否重启应用？',
        success(res) {
          if (res.confirm) {
            //  新的版本已经下载好，调用 applyUpdate 应用新版本并重启
            updateManager.applyUpdate()
          }
        }
      })
    })
    updateManager.onUpdateFailed(function() {
      wx.showToast({
        title: '新版本下载失败',
        icon: 'none'
      })
    })
    wx.setStorageSync('im_connect', false)
    wx.getSystemInfo({
      success: res => {
        // console.log(res)
        this.globalData.navHeight = res.statusBarHeight + 46;
        if (res.platform == "devtools") {
          this.globalData.platform = 'pc'
        } else if (res.platform == "ios") {
          this.globalData.platform = 'ios'
        } else if (res.platform == "android") {
          this.globalData.platform = 'android'
        }
        // this.globalData.platform = 'ios'
      },
      fail(err) {}
    })

  },
  onPageNotFound(res) {
    wx.showToast({
      title: '页面不存在,将返回首页',
      icon: 'none',
      duration: 2000,
      success: () => {
        wx.switchTab({
          url: '/pages/index/index'
        })
      }
    })
  },
  globalData: {
    // domain: "https://m.jianke.cc",//  正式
    // env:'pro',
    // domain:'http://dev-www.shijianke.com/',//线上办公测试
    // domain :  "http://api1.shijianke.com",//  本地
    // domain :  "http://172.16.1.113:8080/",//  本地

    domain: "http://dev-docker-m.shijianke.com/", //  测试

    // domain: "http://sim-docker-m.shijianke.com/", //  仿真
    // domain: "http://m.shijianke.com", //  测试
    env: 'dev',
    // domain: "http://pre-api.jianke.cc",//  模拟正式
    // domain: "http://192.168.5.166:8080",//  本地(迪迪)
    // domain: "http://192.168.5.156:8080",//  本地(中中)
    navHeight: '', //  导航高度
    platform: '', //  运行环境
    version: '1.5.0',
    locationInfo: {}, //  位置信息
    serviceCity: {}, // 购买套餐所在城市
    upLoadUrl: '/m/fileUpload/ossFileUpload',
    userInfo: {}, //  个人信息
    publishInfo: {}, //  发布信息
    comPanyInfo: {}, //  公司信息
    postInfo: {}, //  当前职位信息
    jobChange: false,
    selectCity: null,
    operaAuthorize: null, //  雇主职位操作权限
    imageSrc: 'https://jianke-life.oss-cn-hangzhou.aliyuncs.com/jianke_miniprogram/images/', //  图片网络地址
    recordTime: {
      startTime: '',
      endTime: ''
    }

  },
  //  保存成功提示并返回上一页
  successGoback: (title) => {
    setTimeout(() => {
      wx.showToast({
        title: title ? title : '保存成功',
        success: () => {
          setTimeout(() => {
            wx.navigateBack()
          }, 1000)
        }
      })
    }, 500)
  },


})