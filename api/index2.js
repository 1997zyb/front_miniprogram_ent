import {request} from '../utils/request.js'

//************************************号码库**********************************/

// 查询雇主号码库
export function findNumberLibraryApi(query) {
    return request({
        url: '/m/zhaopinBusiness/queryNumberLibrary',
        method: 'post',
        params: query
    })
}

// 编辑号码库
export function editNumberLibraryApi(query) {
    return request({
        url: '/m/zhaopinBusiness/editNumberLibrary',
        method: 'post',
        params: query
    })
}

// 编辑岗位联系方式回显
export function showJobContactInfo(query) {
  return request({
    url: '/m/zhaopinBusiness/queryJobContactInfo',
    method: 'get',
    params: query
  })
}