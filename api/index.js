import {request,imRequset} from '../utils/request.js'
//************************************公共接口**********************************/


export function loginApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/dealBindWechatAppletLogin',
        method: 'post',
        params: query
    })
} 

//  获取枚举
export function getEnumApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/getPostParttimeJobEnum',
        method: 'post',
        params: query
    })
}

//  利用微信返回加密的手机号到后台解析
export function getPhoneNumberApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/getPhoneNumberFromWechat',
        method: 'post',
        params: query
    })
}

//  绑定手机进行登录
export function bindPhoneApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/bindPhone',
        method: 'post',
        params: query
    })
}

//  获取职位类别枚举
export function getJobClassifyListApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/getJobClassifyList',
        method: 'post',
        params: query
    })
}

//  获取职位类别枚举
export function getJobClassifyFirstSecondListApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/getJobClassifyFirstSecondList',
        method: 'post',
        params: query
    })
}
//  获取客户经理电话
export function getClaimUserTelphoneApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/getClaimUserTelphone',
        method: 'post',
        params: query
    })
}

//  手机号码登录
export function loginByphoneApi(query) {
    return request({
        url: '/m/wechatApplet/login',
        method: 'post',
        params: query
    })
}

//  获取短信验证码
export function getSmsAuthenticationCodeApi(query) {
    return request({
        url: '/m/wechatApplet/getSmsAuthenticationCode',
        method: 'post',
        params: query
    })
}

//  获取首页数据
export function getIndexInfoApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/global',
        method: 'post',
        params: query
    })
}
//  获取省市区数据
export function getProvinceListApi(query) {
    return request({
        url: '/m/wechatApplet/getProvinceList',
        method: 'post',
        params: query
    })
}

//************************************用户中心接口**********************************/
// 保存城市
export function postEnterpriseRecruitmentCityApi(query){
    return request({
        url: '/m/wechatAppletBusiness/postEnterpriseRecruitmentCity',
        method: 'post',
        params: query
    })
}
//  获取用户信息
export function getUserInfoApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/getCmsEmployerDetail',
        method: 'post',
        params: query
    })
}

//  提交用户信息
export function postUserInfoApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/enterpriseBasicInfo',
        method: 'post',
        params: query
    })
}

//  获取城市列表
export function getCityList(query) {
    return request({
        url: '/m/wechatAppletBusiness/getCityList',
        method: 'post',
        params: query
    })
}

//  获取二级城市列表
export function getCityInfoApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/getCityInfo',
        method: 'post',
        params: query
    })
}

//  获取产业列表
export function getIndustyList(query) {
    return request({
        url: '/m/wechatAppletBusiness/getIndustyList',
        method: 'post',
        params: query
    })
}

// 获取展示姓名列表
export function getEnterpriseShowNameEnum(query) {
    return request({
        url: '/m/wechatApplet/getEnterpriseShowNameEnum',
        method: 'post',
        params: query
    })
}

//  上传公司环境照片
export function uploadCompanyPhotoApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/uploadPhoto',
        method: 'post',
        params: query
    })
}
//  删除公司环境照片
export function deleteCompanyPhotoApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/deletePhoto',
        method: 'post',
        params: query
    })
}

//************************************个人中心接口**********************************/

//  登出
export function logoutApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/logout ',
        method: 'post',
        params: query
    })
}

//  获取认证状态
export function getVerifyInfoApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/getVerifyInfo',
        method: 'post',
        params: query
    })
}

//  上传个人认证信息
export function postVerifyInfoApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/postVerifyInfo',
        method: 'post',
        params: query
    })
}

//  上传企业认证信息
export function postEnterpriseVerifyInfoApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/postEnterpriseVerifyInfo',
        method: 'post',
        params: query
    })
}

//************************************职位详情页面接口**********************************/

//  获取职位详情
export function getJobDetailApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/jobDetail',
        method: 'post',
        params: query
    })
}

//************************************职位管理页面接口**********************************/

export function entConfirmWorkCompleteApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/entConfirmWorkComplete',
        method: 'post',
        params: query
    })
}
// 到岗接口
//  投诉接口
export function entConfirmStuBreakPromiseApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/entConfirmStuBreakPromise',
        method: 'post',
        params: query
    })
}
//  获取已发布的职位信息
export function getPublishedJobListApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/getEnterpriseSelfJobList',
        method: 'post',
        params: query
    })
}

//  暂停职位
export function pauseJobApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/setParttimeJobShowHide',
        method: 'post',
        params: query
    })
}

//  关闭职位
export function closeJobApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/closeJob',
        method: 'post',
        params: query
    })
}

//  获取申请职位的求职者信息
export function getQueryApplyJobListApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/getQueryApplyJobList',
        method: 'post',
        params: query
    })
}

//  雇佣或者拒绝求职者
export function refuseOrEmployApplicantApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/entEmployApplyJob',
        method: 'post',
        params: query
    })
}

//  获取求职者简历信息
export function getResumeDetailApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/getResumeDetail',
        method: 'post',
        params: query
    })
}

//************************************发布职位页面接口**********************************/
//  职位亮点枚举
export function getJobTagListApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/getJobTagList',
        method: 'post',
        params: query
    })
}
//  发布职位信息
// export function postJobInfoApi(query) {
//     return request({
//         url: '/m/wechatAppletBusiness/postJob',
//         method: 'post',
//         params: query
//     })
// }
//  发布职位信息( v370)
export function postJobInfoApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/postJobNew',
        method: 'post',
        params: query
    })
}
//  查询可发布职位数量
export function queryJobNumApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/queryJobNumV2',
        method: 'post',
        params: query
    })
}
//  查询可发布职位数量
export function queryJobNumApiOld(query) {
  return request({
    url: '/m/wechatAppletBusiness/queryJobNum',
    method: 'post',
    params: query
  })
}

//  查询可发布精品岗位数量
export function queryAccurateJobNumApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/queryAccurateJobNum',
        method: 'post',
        params: query
    })
}
//  查询可发布职位数量
export function editParttimeJobApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/editParttimeJobNew',
        method: 'post',
        params: query
    })
}


//************************************IM相关接口****************************************/

// 初始化消息
export function initMessageBody(query) {
  return imRequset({
    url: '/im/wechatApplet/initMessageBody.do',
    method: 'get',
    params: query
  })
}

// IM获取兼客好友信息通过targetId
export function getInfoByUserIdApi(query) {
    return imRequset({
        url: '/im/wechatApplet/getUserInfo.do',
        method: 'get',
        params: query
    })
}

// IM获取兼客好友信息通过accountID
export function getInfoByAccountIdApi(query) {
    return imRequset({
        url: '/im/wechatApplet/getUserPublicInfo.do',
        method: 'get',
        params: query
    })
}

//  IM获取token
export function getImTokenApi(query) {
    return imRequset({
        url: '/im/wechatApplet/openImFunction.do',
        method: 'get',
        params: query
    })
}

//************************************VIP相关接口****************************************/
// 记录销售线索

export function savaClueApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/savaClue',
        method: 'post',
        params: query
    })
}
// 使用明细
export function entQueryVipUseageDetailApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/entQueryVipUseageDetail',
        method: 'post',
        params: query
    })
}

// 获取VIP使用情况
export function getVipUseInfoApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/getAccountVipInfo',
        method: 'get',
        params: query
    })
}

// 获取所在城市的套餐信息
export function getVipInfoApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/getVipPackageEntryList',
        method: 'get',
        params: query
    })
}
// 购买VIP
export function buyVipApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/rechargeVipPackage',
        method: 'get',
        params: query
    })
}
// 购买VIP
export function getPayOrderApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/jianKeZhaoPinPayVipOrder',
        method: 'get',
        params: query
    })
}
//  获取雇主职位操作权限
export function getEmployerAuthorityApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/getEntPrivilege',
        method: 'get',
        params: query
    })
}
//  获取雇主职位操作权限
export function vipOpreteApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/useJobVipSpread',
        method: 'get',
        params: query
    })
}

//************************************人才库相关接口****************************************/

//  获取人才库列表
export function getTalentListApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/queryTalentList',
        method: 'get',
        params: query
    })
}
//  获取人才联系方式
export function getTalentContactApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/getResumeContact',
        method: 'get',
        params: query
    })
}
//  获取雇主已获取的人才信息
export function getContactResumeListApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/queryContactResumeList',
        method: 'get',
        params: query
    })
}
//  收藏人才
export function collectTalentApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/collectStudent',
        method: 'get',
        params: query
    })
}
//  取消收藏人才
export function cancelCollectTalentApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/cancelCollectedStudent',
        method: 'get',
        params: query
    })
}
//  获取已收藏人才列表
export function getCollectedTalentListApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/getCollectedStudentList',
        method: 'get',
        params: query
    })
}
//  获取粉丝列表
export function getFansListApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/queryFocusFansList',
        method: 'get',
        params: query
    })
}


//************************************精品岗位相关接口****************************************/
//  取消精品岗位展示
export function cancelAccurateJobApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/cancelAccurateJob',
        method: 'Post',
        params: query
    })
}
//设置精品岗位展示
export function updateAccurateJobApi(query){
    return request({
        url: '/m/wechatAppletBusiness/updateAccurateJob',
        method: 'Post',
        params: query
    })
}
//更新精准简历
export function updateAccurateResumeApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/updateAccurateResume',
        method: 'Post',
        params: query
    })
}

// 查询精准简历数量
export function queryAccurateResumeNumApi(query) {
    return request({
        url: '/m/wechatAppletBusiness/queryAccurateResumeNum',
        method: 'Post',
        params: query
    })
}

// 查询子母账号体系内的账号是否查看过该简历
export function subAccountContactResumeTipApi(query) {
  return request({
    url: '/m/zhaopinBusiness/sub/account/subAccountContactResumeTip',
    method: 'Post',
    params: query
  })
}

// 解绑微信通知
export function unBandingWechatUserApi(query) {
    return request({
      url: '/m/wyyx/im/zhaopinBusiness/unBandingWechatUser',
      method: 'Post',
      params: query
    })
  }
// 绑定二维码
  export function getBusinessBindWechatPublicQrCodeApi(query) {
    return request({
      url: '/m/wyyx/im/zhaopinBusiness/getBusinessBindWechatPublicQrCode',
      method: 'Post',
      params: query
    })
  }

  //  是否绑定公众号
export function getAccountThirdPlatBindInfo (query) {
    return request({
        url: '/m/wyyx/im/zhaopinBusiness/getAccountThirdPlatBindInfo',
        method: 'post',
        params: query
    })
}