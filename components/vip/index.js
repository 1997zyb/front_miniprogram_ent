var app = getApp();
let open_id = ''
let code = ''
let oauth_id = ''
import {
    loginApi,
    getPhoneNumberApi,
    bindPhoneApi,
    getUserInfoApi,
    getClaimUserTelphoneApi
} from '../../api/index'
import {
    getResume
} from '../../utils/util'

import {
    createSession
} from '../../utils/request'

Component({
    options: {},
    //  组件的属性列表
    properties: {
        option: {
            type: Object,
            value: {}
        }

    },
    lifetimes: {
        attached: function() {
            console.log('login')
            wx.login({
                success: (res) => {
                    code = res.code
                },
            })
        },
    },
    //  组件的初始数据
    data: {
        hideFlag: true,
    },

    //  组件的方法列表
    methods: {
        gotoServiceAgreementPage() {
            let url = encodeURIComponent(app.globalData.domain + '/wap/userAgreementJkzp')
            wx.navigateTo({
                url: '/pages/webView/index?url=' + url
            })
        },
        gotoPrivacyAgreementPage() {
            let url = encodeURIComponent(app.globalData.domain + '/wap/privacyPolicyJkzp')
            wx.navigateTo({
                url: '/pages/webView/index?url=' + url
            })
        },
        cancelLogin() {
            this.triggerEvent('cancelLogin', false)
        },
        openModal(content, but, platform) {
            console.log(content)
            this.setData({
                hideFlag: false,
                content,
                but,
                platform
            })
        },
        confirm() {
            if (this.data.platform == 'ios') {
                getClaimUserTelphoneApi().then(res => {
                    let value = res.data.content.contractPhone
                    wx.makePhoneCall({
                        phoneNumber: value,
                        success: function() {
                            console.log("拨打电话成功！")
                        },
                        fail: function() {
                            console.log("拨打电话失败！")
                        }
                    })
                })
            } else {
                wx.navigateTo({
                    url: '/pages/buyVip/index'
                })
            }
        },


        closeModal() {
            this.setData({
                hideFlag: true
            })
            this.triggerEvent("close")
        },
    }
})