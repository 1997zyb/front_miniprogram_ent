Component({
    /**
     * 组件的属性列表
     */
    properties: {
        message: Object,
        typeName: {
            type: String,
            value: 'message'
        },
    },

    /**
     * 组件的初始数据
     */
    data: {
        experienceType: ['', '1-3年', '3-5年', '5-10年', '10年以上'],
        certificateType: ['高中', '大专', '本科', '硕士研究生', '博士研究生', '其他', '初中及以下', '中专'],
    },

    lifetimes: {
        ready: function() {
            if (this.data.typeName == 'message' && this.data.message.content&&this.data.message.content.content) {
                this.setData({
                    messageSee: JSON.parse(this.data.message.content.content)
                })
            }
        }
    },

    observers: {
        'message': function (message) {
            if (message.dead_time_start_end_str)
            message.dead_time_start_end_str = message.dead_time_start_end_str.replace(/-/, "至")
            this.setData({
                messageSee: message
            })
        }
    },

    /**
     * 组件的方法列表
     */
    methods: {
        skip: function(e) {
            let page=getCurrentPages()
            var url = e.currentTarget.dataset.url;
            this.triggerEvent('onskip', url);
            if (page[page.length - 1].route == 'pages/jobDetail/jobDetail')
                return
            wx.navigateTo({
                url: '/pages/jobDetail/jobDetail?job_uuid=' + this.data.content.job_uuid,
            })
        }
    }
})