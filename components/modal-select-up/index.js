var app = getApp();
let open_id = ''
let code = ''
let oauth_id = ''
let modalAccurate={}
import {
    loginApi,
    getPhoneNumberApi,
    bindPhoneApi,
    getUserInfoApi,
    getClaimUserTelphoneApi
} from '../../api/index'
import {
    getResume,
    checkLogin
} from '../../utils/util'

import {
    createSession
} from '../../utils/request'

Component({
    options: {},
    //  组件的属性列表
    properties: {
        option: {
            type: Object,
            value: {}
        },
        showType:{
            type:String,
            value:''
        }
    },
    lifetimes: {
        attached: function() {
            console.log('login')
            wx.login({
                success: (res) => {
                    code = res.code
                },
            })
        },
    },
    //  组件的初始数据
    data: {
        hideFlag: true,
      content:'文案文案文案文案文案文案文案文案文案文案文案文案文案文案文案'
    },

    //  组件的方法列表
    methods: {
        openModal() {
            this.setData({
                hideFlag: false,
            })
            getClaimUserTelphoneApi().then(res=>{
                this.setData({
                    contactName: res.data.content.contractName,
                    contactPhone:res.data.content.contractPhone
                })
            })
        },

        // 什么是精品岗位
        showAccurate(e) {
            modalAccurate = this.selectComponent("#modal_accurate");
            modalAccurate.openModal()
            this.setData({
                type:e.currentTarget.dataset.type
            })
        },
        selectPublish(e) {
            this.triggerEvent("select", e.currentTarget.dataset.type)
            // wx.navigateTo({
            //     url: '/pages/publishJobType/index?type=' + e.currentTarget.dataset.type
            // })
        },

        closeModal() {
            this.setData({
                hideFlag: true
            })
            this.triggerEvent("close")
        },
        closeModalOne(){
            this.setData({
                hideFlag: true
            })
        }
    }
})