let app = getApp();
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        message: Object,
        typeName: {
            type: String,
            value: 'message'
        },
        text:String,


    },

    /**
     * 组件的初始数据
     */
    data: {
        type:0,
        hasLink:false,
        complaint:false,
    },

    lifetimes: {
        ready: function() {
            if (this.data.typeName == 'message' && this.data.message.content && this.data.message.content.content) {
                this.setData({
                    text: this.data.message.content.content
                })
            }
            if (this.data.message.content.extra && this.data.message.content.extra.length > 69) {
                let content = JSON.parse(this.data.message.content.extra.substr(69))
                content.content = JSON.parse(content.content)
                this.data.type = content.content.notice_type
            }
            if(this.data.type==14 || this.data.type==15 ||this.data.type==36){
                this.setData({hasLink:true})
            }else {
                this.setData({hasLink:false})
            }
            if(this.data.type==19){//职位被投诉
                this.setData({complaint:true})
            }else {
                this.setData({complaint:false})
            }
        }
    },

    /**
     * 组件的方法列表
     */
    methods: {
        gotoPage: function() {
            app.globalData.jobChange = true
            if(this.data.type==14){// 已通过
                wx.switchTab({
                    url: '/pages/index/index?currentTabIndex=1',
                })
            }
            if(this.data.type==15 || this.data.type==36){//未通过或者职位下架
                app.globalData.jobChange = true
                wx.switchTab({
                    url: '/pages/index/index?currentTabIndex=2',
                })
            }
        }
    }
})
