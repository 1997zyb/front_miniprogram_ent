var app = getApp();
let open_id = ''
let code = ''
let oauth_id = ''
import {
    loginApi,
    getPhoneNumberApi,
    bindPhoneApi,
    getUserInfoApi
} from '../../api/index'
import {
    getResume,
    checkAndAddCityInfo
} from '../../utils/util'

import {
    createSession
} from '../../utils/request'

Component({
    options: {},
    //  组件的属性列表
    properties: {
        option: {
            type: Object,
            value: {}
        }

    },
    lifetimes: {
        attached: function () {
            console.log('login')
            wx.login({
                success: (res) => {
                    code = res.code
                },
            })
        },
    },
    //  组件的初始数据
    data: {
        hideFlag: true,
    },

    //  组件的方法列表
    methods: {
        gotoServiceAgreementPage() {
            let url = encodeURIComponent(app.globalData.domain + '/wap/userAgreementJkzp')
            wx.navigateTo({
                url: '/pages/webView/index?url=' + url
            })
        },
        gotoPrivacyAgreementPage() {
            let url = encodeURIComponent(app.globalData.domain + '/wap/privacyPolicyJkzp')
            wx.navigateTo({
                url: '/pages/webView/index?url=' + url
            })
        },
        getPhoneNumber(e) {
            console.log(e)
            if (e.detail.encryptedData) {
                createSession().then(res2 => {
                    loginApi({
                        code: code,
                        login: 0
                    }).then(res => {
                        open_id = res.data.content.open_id
                        oauth_id = res.data.content.oauth_id
                        wx.setStorage({
                            key: "open_id",
                            data: open_id
                        })
                        wx.setStorage({
                            key: "oauth_id",
                            data: oauth_id
                        })
                        let iv = e.detail.iv
                        let param = {
                            open_id: open_id,
                            encrypted_data: e.detail.encryptedData,
                            iv: iv,
                        }
                        getPhoneNumberApi(param).then(res => {
                            let phone = res.data.content.phone_num
                            let param_2 = {
                                username: phone,
                                user_type: 2,
                                dynamic_sms_code: '',
                                oauth_id: oauth_id,
                                iv: iv
                            }
                            bindPhoneApi(param_2).then(res2 => {
                                wx.setStorage({
                                    key: "is_login",
                                    data: true
                                });
                                getUserInfoApi().then(res => {
                                    checkAndAddCityInfo(() => {
                                        if ((!res.data.content.trueName || !res.data.content.bean)) {
                                            wx.navigateTo({
                                                url: '/pages/personalInformation/index'
                                            })
                                            return
                                        }
                                        if (res.data.content.trueName && res.data.content.bean.industryId && res.data.content.bean.cityId) {
                                        } else {
                                            wx.navigateTo({
                                                url: '/pages/personalInformation/index'
                                            })
                                        }
                                    })


                                })
                                let pages = getCurrentPages()
                                let currentPage = pages[pages.length - 1]
                                if (currentPage.getInfo) {
                                    getResume().then(res => {
                                        checkAndAddCityInfo(() => {
                                            currentPage.getInfo()
                                        })
                                    })
                                } else {
                                    getResume()
                                }
                                this.closeModal()
                                currentPage.setData({
                                    showLogin: false
                                })
                                app.globalData.jobChange = true
                            })
                        });
                    })
                })
            } else {
                wx.showToast({
                    title: '授权失败,请重新授权',
                    icon: 'none'

                })
            }
        },
        cancelLogin() {
            this.triggerEvent('cancelLogin', false)
        },
        openModal() {
            this.setData({
                hideFlag: false
            })
        },
        closeModal() {
            this.setData({
                hideFlag: true
            })
        },
    }
})