import {
    getProvinceListApi
} from "../../api/index";

let app = getApp()
// let searchList = []
Component({
    options: {},
    //  组件的属性列表
    properties: {
        title: {
            type: String,
            value: '选择城市'
        },
        columnsNum: { // '2':省市 '3':省市区
            type: String,
            value: '3'
        },
        show: {
            type: Boolean,
            value: false
        },
        canSelectAll:{ // 显示不限
            type: Boolean,
            value: false
        }
    },
    //  组件的初始数据
    data: {
        areaList: [],
        loading:true,
        searchList:[],
    },
    lifetimes: {
        attached: function () {
            this.getAreaList()
        },
    },
    //  组件的方法
    methods: {
        getAreaList() {
            let searchList = []
            getProvinceListApi().then(res => {
                let list = res.data.content.provinces
                let provinceList = {}
                let cityList = {}
                let countyList = {}

                console.log(searchList)
                list.forEach((province, index) => {
                    let num1 = 11 + index + ''
                    let key1 = num1 + '0000'
                    provinceList[key1] = province.provinceName
                    searchList.push({name:province.provinceName,id:key1,child:[]})
                    province.cities.forEach((city, index2) => {
                        let num2 = 11 + index2 + ''
                        let key2 = num1 + num2 + '00'
                        cityList[key2] = city.name
                        searchList[index].child.push({id:city.id,name:city.name,child:[]})
                        city.childArea.forEach((area, index3) => {
                            let num3 = 11 + index3 + ''
                            let key3 = num1 + num2 + num3
                            countyList[key3] = area.name
                            searchList[index].child[index2].child.push({id:area.id,name:area.name})

                        })
                        countyList[num1 + num2 + '01'] = '不限'
                        searchList[index].child[index2].child.unshift({ id: '', name: '不限' })
                    })
                })
                let areaList = {province_list: provinceList, city_list: cityList, county_list: countyList}
                this.setData({areaList: areaList,loading:false})
                console.log(areaList)
                this.searchList = searchList
            })
        },
        confirm(e){
            console.log(e)
            let result = e.detail.index
            let province = ''
            let city = {}
            let area = {}
            if(result.length>=2){
                province = this.searchList[result[0]]
                city = province.child[result[1]]
            }
            if(result.length==3){
                area = city.child[result[2]]
            }
            console.log(city)
            let data = {province:province,city:city,area:area}
            this.triggerEvent("confirm", data)
            this.setData({show:false})
        },
        cancel() {
            this.triggerEvent("close")
            this.setData({show:false})
        },
        change(e){
            this.triggerEvent("change", e.detail)
        },
        onClose(){
            this.triggerEvent("close")
            this.setData({show:false})
        },
        // 触发外部联系
        openPopup(e) {
            this.triggerEvent("showPopup", e.currentTarget.dataset.talent)
        },

    }
})
