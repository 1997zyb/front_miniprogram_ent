var app = getApp();
let open_id = ''
let code = ''
let oauth_id = ''
import {
    loginApi,
    getPhoneNumberApi,
    bindPhoneApi,
    getUserInfoApi,
    getClaimUserTelphoneApi,
    getVipInfoApi,
    getIndexInfoApi
} from '../../api/index'
import {
    getResume,
    checkLogin
} from '../../utils/util'

import {
    createSession
} from '../../utils/request'

Component({
    options: {},
    //  组件的属性列表
    properties: {
        option: {
            type: Object,
            value: {}
        },
        showType:{
            type:String,
            value:''
        },
        rankType:{
            type: Number,
            value: 0
        },
        resumeOrJob:{
            type: String,
            value: 'resume'
        }
    },
    lifetimes: {
        attached: function() {
            console.log('login')
            wx.login({
                success: (res) => {
                    code = res.code
                },
            })
        },
    },
    //  组件的初始数据
    data: {
        hideFlag: true,
      content:'文案文案文案文案文案文案文案文案文案文案文案文案文案文案文案'
    },

    //  组件的方法列表
    methods: {
        openModal() {
            // getVipInfoApi({ city_id: wx.getStorageSync('current_city').id, package_id:10}).then(res=>{
            //     console.log(res)
            // })
            getIndexInfoApi({ city_id: wx.getStorageSync('current_city').id, package_id: 10 }).then(res=>{
                this.setData({
                    jobPrice:parseFloat(res.data.content.accurate_job_num_single_price).toFixed(2),
                    resumePrice: parseFloat(res.data.content.accurate_resume_num_single_price).toFixed(2),
                    recruitPrice: parseFloat(res.data.content.recruit_job_num_single_price).toFixed(2) 
                })
            })
            this.setData({
                hideFlag: false,
            })
        },
        confirm() {
            if (this.data.platform == 'ios') {
                getClaimUserTelphoneApi().then(res => {
                    let value = res.data.content.contractPhone
                    wx.makePhoneCall({
                        phoneNumber: value,
                        success: function() {
                            console.log("拨打电话成功！")
                        },
                        fail: function() {
                            console.log("拨打电话失败！")
                        }
                    })
                })
            } else {
                wx.navigateTo({
                    url: '/pages/buyVip/index'
                })
            }
        },


        closeModal() {
            this.setData({
                hideFlag: true
            })
            this.triggerEvent("close")
        },

        // 前往Vip购买界面
        buy(){
            this.closeModal()
            wx.navigateTo({
                url: '/pages/buyVip/index',
            })
        }
    }
})