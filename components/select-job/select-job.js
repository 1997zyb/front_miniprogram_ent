// components/select-job/select-job.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        type: {
            type: Number,
            value: 0
        },
        selectJobInit: {
            type: Array,
            value: [],
        },
        selectJobListInit: {
            type: Array,
            value: [],
        },
        selectJobNameListInit: {
            type: Array,
            value: [],
        },
        jobIndex: {
            type: Number,
            value: -1,
        },
        jobIndexs: {
            type: Number,
            value: -1,
        },
        selectIndex:{
            type: Number,
            value: -1,
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        selectJob: [{
                name: '工作类型1',
                jobId: 'one',
                id: 0,
                array: [
                    '工作1', '工作2', '工作3', '工作4', '工作5', '工作6', '工作7', '工作8', '工作9', '工作10',
                ]
            }, {
                name: '工作类型2',
                id: 1,
                jobId: 'tow',
                array: [
                    '工作1', '工作2', '工作3', '工作4', '工作5', '工作6', '工作7',
                ]
            }, {
                name: '工作类型3',
                id: 2,
                jobId: 'three',
                array: [
                    '工作1', '工作2', '工作3', '工作4', '工作5', '工作6', '工作7', '工作8', '工作9', '工作10', '工作11', '工作12', '工作13', '工作14'
                ]
            },
            {
                name: '工作类型4',
                id: 3,
                jobId: 'four',
                array: [
                    '工作1', '工作2', '工作3', '工作4', '工作5', '工作6', '工作7', '工作8', '工作9', '工作10', '工作11', '工作12'
                ]
            }, {
                name: '工作类型5',
                id: 4,
                jobId: 'five',
                array: [
                    '工作1', '工作2', '工作3', '工作4', '工作5', '工作6', '工作7', '工作8', '工作9', '工作10', '工作11', '工作12', '工作13'
                ]
            },
        ],
        selectIndex: 0,
        selectJobList: [],
        selectJobNameList: [],
        scrollHeight:0,
    },
    ready() {
        if(this.data.type==0){
            this.setData({
                selectIndex:0,
                jobIndex: 0,
                jobIndexs: 0,
            })
        }
    },
    observers: {
        'selectJobInit': function(selectJobInit) {
            selectJobInit.forEach((item,index)=>{
                item.id='one'+index
            })
            this.setData({
                selectJob: selectJobInit,
            })
            if (this.data.jobIndex && selectJobInit[this.data.jobIndex]){
                this.setData({
                    intoView: selectJobInit[this.data.jobIndex].id,
                    selectIndex: this.data.jobIndex,
                    scrollFlag: false,
                })
            }
            this.setHeight()
        },
        'selectJobListInit': function (selectJobListInit) {
            this.setData({
                selectJobList: selectJobListInit
            })
        },
        'selectJobNameListInit': function (selectJobNameListInit) {
            this.setData({
                selectJobNameList: selectJobNameListInit
            })
        },
    },
    /**
     * 组件的方法列表
     */
    methods: {
        selectView(e) {
            this.setData({
                intoView: e.currentTarget.dataset.name,
                selectIndex: e.currentTarget.dataset.index,
                scrollFlag:false,
            })
            if (this.data.selectJob[this.data.selectIndex].first_level_job_classify_name =='职位不限'){

                this.setData({
                    jobIndex: 0,
                    jobIndexs: 0,
                })
                const myEventDetail ={
                    name: '职位不限',
                    job: '',
                    jobIndex: 0,
                    jobIndexs: 0,
                }
                this.triggerEvent("selectJob", myEventDetail)
            }
        },
        // 设置跳转高度
        setHeight(){
            let heightArray=[]
            let proportion = wx.getSystemInfoSync().screenWidth/750
            let selectJob = this.data.selectJob
            let height = 0
            selectJob.forEach(item=>{
                if (item.first_level_job_classify_name=='职位不限')
                    height = height + 84 * proportion
                else if (item.second_level_job_classify_list.length>0)
                    height = height + 66 * proportion + 84 * proportion * (parseInt(item.second_level_job_classify_list.length % 3 == 0 ? (item.second_level_job_classify_list.length) / 3 : (item.second_level_job_classify_list.length/3 +1)))
                heightArray.push(height)
            })
            this.setData({
                heightArray
            })
        },
        // 选择工作
        selectJob(e) {
            let index = e.currentTarget.dataset.index
            let indexs = e.currentTarget.dataset.indexs
            let selectJobList = this.data.selectJobList
            let selectJobNameList = this.data.selectJobNameList
            let selectJob = this.data.selectJob
            let item = this.data.selectJob[index].second_level_job_classify_list[indexs]
            if (this.data.type == 0) {
                this.setData({
                    jobIndex: index,
                    jobIndexs: indexs,
                })
                const myEventDetail = {
                    name: item.job_classify_name,
                    job: item.id,
                    jobIndex: index,
                    jobIndexs: indexs,
                } // detail对象，提供给事件监听函数
                this.triggerEvent("selectJob", myEventDetail)
                let e={
                    currentTarget:{
                        dataset:{
                            name: '',
                            index: index,
                        }
                    }
                }
                this.selectView(e)
            } else
                this.setTypeContentOne(e)

        },
        // type为1时的操作
        setTypeContentOne(e) {
            let index = e.currentTarget.dataset.index
            let indexs = e.currentTarget.dataset.indexs
            let selectJobList = this.data.selectJobList
            let selectJobNameList = this.data.selectJobNameList
            let selectJob = this.data.selectJob
            let item = this.data.selectJob[index].second_level_job_classify_list[indexs]
            if (selectJobList.length >= 5 && !item.check) {
                wx.showToast({
                    title: '最多可选5个',
                    icon: 'none',
                })
                return
            }
            if (item.check) {
                item.check = false
                selectJob[index].second_level_job_classify_list[indexs] = item
                let indexOf = selectJobNameList.indexOf(item.job_classify_name)
                selectJobNameList.splice(indexOf, 1)
                selectJobList.splice(indexOf, 1)
                this.setData({
                    selectJobNameList,
                    selectJobList,
                    selectJob
                })
            } else {
                item.check = true
                selectJob[index].second_level_job_classify_list[indexs] = item
                selectJobList.push({
                    jobIndex: index,
                    jobIndexs: indexs,
                })
                selectJobNameList.push(item.job_classify_name)
            }
            const myEventDetail = {
                selectJobNameList,
                selectJobList,
                selectJob
            } // detail对象，提供给事件监听函数
            this.triggerEvent("selectJob", myEventDetail)
        },


        // 滚动触发
        bindscroll(e) {
            let scrollHeight = this.data.scrollHeight - e.detail.deltaY
            let selectIndex=this.data.selectIndex
            if (scrollHeight>this.data.heightArray[selectIndex]){
                while (this.data.selectJob[selectIndex + 1].second_level_job_classify_list.length==0){
                    selectIndex++
                }
                this.setData({
                    // intoView: this.data.selectJob[selectIndex+1].id,
                    selectIndex:selectIndex+1,
                    scrollHeight
                })
            } else if(scrollHeight < this.data.heightArray[selectIndex-1]){
                while (this.data.selectJob[selectIndex - 1].second_level_job_classify_list.length == 0) {
                    selectIndex--
                }
                if(this.data.scrollFlag){
                    selectIndex = selectIndex-1
                }
                this.setData({
                    // intoView: this.data.selectJob[selectIndex].id,
                    selectIndex,
                    scrollFlag:true,
                    scrollHeight
                })
            }else{
                this.setData({
                    scrollHeight
                })
            }
        },
    },


})