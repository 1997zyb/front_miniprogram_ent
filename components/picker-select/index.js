import {
    getProvinceListApi
} from "../../api/index";

let app = getApp()
let searchList = []
Component({
    options: {},
    //  组件的属性列表
    properties: {
        title: {
            type: String,
            value: '请选择'
        },
        show:{
            type:Boolean,
            value:true
        },
        columns: {
            type: Array,
            value: []
        },
        type:{
            type:String,
            value:''
        },
        defaultId:{
            type: Number,
            value: 0,
        }
    },
    //  组件的初始数据
    data: {
        loading:true
    },
    lifetimes: {
        attached: function () {
            // this.setColumns()
        },
    },
    observers: {
        'columns': function (columns) {
            var columnsBase = []
            if (columns.length > 0) {
                columns.forEach(res => {
                    columnsBase.push(res.name)
                })
            }
            this.setData({
                columnsBase
            })
        },
        'defaultId': function (defaultId){
            this.setData({
                id: defaultId
            })
            console.log(this.data.id)
        }
    },
    //  组件的方法
    methods: {
        confirm(e){
            console.log(e)
            let result = e.detail.index
            let province = ''
            let city = {}
            let area = {}
            if(result.length>=2){
                province = searchList[result[0]]
                city = province.child[result[1]]
            }
            if(result.length==3){
                area = city.child[result[2]]
            }
            let data = {province:province,city:city,area:area}
            this.triggerEvent("confirm", data)
            this.setData({show:false})
        },
        cancel(){
            this.setData({show:false})
        },
        change(e){
            this.triggerEvent("change", e.detail)
        },
        onClose(){
            this.setData({show:false})
            this.triggerEvent("close")
        },
        // 触发外部联系
        openPopup(e) {
            this.triggerEvent("showPopup", e.currentTarget.dataset.talent)
        },
        bindChange(e){
            console.log(e)
        },
        bindConfirm(e){
            this.triggerEvent("confirm", this.data.columns[this.data.columnsBase.indexOf(e.detail.value)])
        }
    }
})
