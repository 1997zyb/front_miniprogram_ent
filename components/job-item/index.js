Component({
    options: {

    },
    //  组件的属性列表
    properties: {
        job: {
            type: Object,
            value: {}
        },
        itemType:{
            type:String,
            value:'list'
        }
    },

    //  组件的初始数据
    data: {
        flag: true,
        experienceType: ['', '1-3年', '3-5年', '5-10年','10年以上'],
        certificateType: ['高中', '大专', '本科', '硕士研究生', '博士研究生', '其他', '初中及以下','中专']
    },
    ready(){
        let job=this.data.job
        this.setData({
            jobSee: job
        })
    },

    observers:{
        'job': function (job) {
            job.distance = job.distance? (job.distance > 1000 ? (parseInt(job.distance / 100) / 10 + '千米') : (job.distance + '米')):null
            job.dead_time_start_end_str=job.dead_time_start_end_str.replace(/-/, "至")
            let jobClassflyType = job.job_classify_type || job.job_type
            if (jobClassflyType == 3 && job.academic_certificate && job.work_experience)
                jobClassflyType=3
            else if (jobClassflyType == 2 || (jobClassflyType == 3&&!job.academic_certificate && !job.work_experience))
                jobClassflyType = 2
            // console.log(job.job_title+':'+jobClassflyType)
            if (job.salary.value >= 10000) {
                job.salary.value = parseInt(job.salary.value / 1000) / 10+ '万'
            }
            if (job.salary.value_max >= 10000) {
                job.salary.value_max = parseInt(job.salary.value_max / 1000) / 10 + '万'
            }
            this.setData({
                jobSee: job,
                jobClassflyType
            })
        }
    },

    //  组件的方法列表
    methods: {
        // gotoJobDetailPage(){
        //     wx.navigateTo({
        //       url: '/pages/jobDetail/jobDetail?job_uuid='+this.properties.job.job_uuid
        //     })
        // }
    }
})