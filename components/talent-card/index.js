import {
  cancelCollectTalentApi,
  collectTalentApi,
  getInfoByAccountIdApi,
  getTalentContactApi,
  getClaimUserTelphoneApi,
  entConfirmStuBreakPromiseApi,
  entConfirmWorkCompleteApi,
  getResumeDetailApi,
  subAccountContactResumeTipApi 
} from "../../api/index";
let app = getApp()
Component({
  options: {},
  //  组件的属性列表
  properties: {
    talent: {
      type: Object,
      value: {}
    },
    index: {
      type: Number,
      value: 0
    },
    type: {
      type: String,
      value: '1'
    },
    tabIndex: {
      type: Number,
      value: 0
    },
    pageType: {
      type: Number,
      value: 1
    } //1、人才列表，2、粉丝入口，3、投递入口，4、群组入口，5、IM消息入口
  },
  //  组件的初始数据
  data: {
    flag: true,
    show: false,
    complainShow: false,
    telephone: '',
    currentTalent: {},
    platform: app.globalData.platform,
  },
  //  组件的方法列表
  methods: {
    attached: function() {
      console.log(this.data.talent)
    },
    // 跳转简历详情页面
    gotoResumePage() {
      let id = this.data.talent.resume_id
      wx.navigateTo({
        url: '/pages/resumeInfo/index?resume_id=' + id + '&readResumePage=1'
      })
    },
    // 打开联系弹窗
    showPopup(e) {
      this.parentJudge(()=>{
        this.getResumeDetail(() => {
          this.getContact(e)
        })
      },e)
    },
    // 子母账号判断
    parentJudge(callback,e){
      this.data.currentTalent = e.currentTarget.dataset.talent
      var that = this
      subAccountContactResumeTipApi({
        resume_id: this.data.currentTalent.resume_id
      }).then(res => {
        if (res.data.content.popup == 0) {
          callback()
        } else {
          wx.showModal({
            content: res.data.content.message,
            cancelColor: '#b1b1b1',
            confirmColor: '#00BCD4',
            success(res) {
              if (res.confirm) {
                that.getContact()
              }
            }
          })
        }
      })
    },
    // 获取联系方式
    getContact(e) {
      this.data.currentTalent = e.currentTarget.dataset.talent
      getTalentContactApi({
        resume_id: this.data.currentTalent.resume_id
      }).then(res => {
        if (res.data.errCode == 0) {
          this.data.telephone = res.data.content.telephone
          // this.setData({
          //   show: true
          // });
          this.contactPhone()
        } else if (res.data.errCode == 10) {
          wx.showModal({
            content: '当前剩余' + res.data.content.ent_left_resume_num + '份简历数，获取联系方式将消耗您一份简历数',
            confirmText: '马上获取',
            cancelText: '再考虑下',
            cancelColor: '#b1b1b1',
            confirmColor: '#00BCD4',
            success(res) {
              if (res.confirm) {
                callback()
              }
            }
          })
        } else {
          let contentText = this.data.platform == 'ios' ? '请联系招聘顾问' : '请开通VIP会员进行充值'
          let btnText = this.data.platform == 'ios' ? '立即联系' : '立即开通'
          wx.showModal({
            title: '',
            content: '您的简历数余额不足\n' +
              '\n' +
              contentText,
            confirmText: btnText,
            cancelText: '再考虑下',
            cancelColor: '#b1b1b1',
            confirmColor: '#00BCD4',
            success: (res) => {
              if (res.confirm) {

                if (this.data.platform == 'ios') {
                  getClaimUserTelphoneApi().then(res => {
                    let value = res.data.content.contractPhone;
                    wx.makePhoneCall({
                      phoneNumber: value,
                      success: function() {
                        console.log("拨打电话成功！")
                      },
                      fail: function() {
                        console.log("拨打电话失败！")
                      }
                    })
                  })

                } else {
                  wx.navigateTo({
                    url: '/pages/buyVip/index'
                  })
                }

              }
            }
          })
        }
      })
    },

    //查询当前用户可用简历数量
    getResumeDetail(callback) {

      getResumeDetailApi({
        resume_id: this.data.currentTalent.resume_id,
        read_resume_page: this.data.pageType,
        is_support_free_resume_num: 1
      }).then(res => {
        if (res.data.content.ent_contact_status == 1 || res.data.content.ent_left_resume_num == 0) {
          callback()
        } else {
          wx.showModal({
            content: '当前剩余' + res.data.content.ent_left_resume_num + '份简历数，获取联系方式将消耗您一份简历数',
            confirmText: '马上获取',
            cancelText: '再考虑下',
            cancelColor: '#b1b1b1',
            confirmColor: '#00BCD4',
            success(res) {
              if (res.confirm) {
                callback()
              }
            }
          })
        }

      })
    },
    // 触发外部联系
    openPopup(e) {
      this.triggerEvent("showPopup", e.currentTarget.dataset.talent)
    },
    //  拨打电话
    contactPhone() {
      let value = this.data.telephone;
      wx.makePhoneCall({
        phoneNumber: value,
      })
    },
    // 跳转至在线聊天页面
    chatOnline() {
      let params = {
        accountId: this.data.talent.account_id
      }
      getInfoByAccountIdApi(params).then(res => {
        let url = '/pages/conversation/chat?type=1&targetId=' + res.data.content.friend.uuid + '&title=' + res.data.content.friend.nickname + '&accountId=' + this.data.talent.account_id
        wx.navigateTo({
          url: url,
        })
        this.onClose()
      })
    },
    // 关闭联系弹窗
    onClose() {
      this.setData({
        show: false
      });
    },
    // 收藏
    collect(e) {
      collectTalentApi({
        stu_account_id: this.data.talent.account_id
      }).then(res => {
        if (res.data.errCode == 0) {
          wx.showToast({
            title: '收藏成功',
          })
          this.data.talent.is_collect = 1
          this.setData({
            talent: this.data.talent
          })
          this.triggerEvent("changTalent", this.data.index)
        } else {
          wx.showToast({
            title: res.data.errMsg,
            icon: 'none'
          })
        }

      })
    },
    // 合适
    employ(e) {
      this.triggerEvent("employ", e.currentTarget.dataset.id)
    },
    refuse(e) {
      this.triggerEvent("refuse", e.currentTarget.dataset.id)
    },
    //  取消收藏
    calcelCollect() {
      cancelCollectTalentApi({
        stu_account_id: this.data.talent.account_id
      }).then(res => {
        if (res.data.errCode == 0) {
          wx.showToast({
            title: '取消成功',
          })
          this.data.talent.is_collect = 0
          this.setData({
            talent: this.data.talent
          })
          this.triggerEvent("changTalent", this.data.index)
        } else {
          wx.showToast({
            title: res.data.errMsg,
            icon: 'none'

          })
        }

      })
    },

    //  打开举报弹窗
    openComplainModal(e) {
      this.setData({
        complainShow: true,
        applyJobId: e.currentTarget.dataset.id
      })
    },
    //  关闭举报弹窗
    closeComplainModal() {
      this.setData({
        complainShow: false
      })
    },

    //  提交举报理由
    submitFeedback(e) {
      let param = {
        apply_job_id: this.data.applyJobId,
      }
      entConfirmStuBreakPromiseApi(param).then(res => {
        if (res.data.errCode !== 1) {
          wx.showToast({
            title: '投诉成功',
            success: () => {
              setTimeout(() => {
                let page = getCurrentPages()[getCurrentPages().length - 1]
                page.onShow()
                this.closeComplainModal()
              }, 1000)
            }
          })
        } else {
          wx.showToast({
            title: '当前兼客状态无法举报',
            icon: 'none',
          })
        }

      })
    },

    // 到岗未到岗
    reportDuty(e) {
      entConfirmWorkCompleteApi({
        apply_job_id: e.currentTarget.dataset.id
      }).then(res => {
        if (res.data.errCode !== 1) {
          let talent = this.data.talent
          talent.trade_loop_finish_type = 3
          this.setData({
            talent
          })
        } else {
          wx.showToast({
            title: '当前兼客状态无法确认到岗',
            icon: 'none',
          })
        }
      })
    }
  }
})