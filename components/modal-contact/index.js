var app = getApp();
let open_id = ''
let code = ''
let oauth_id = ''
import {
    loginApi,
    getPhoneNumberApi,
    bindPhoneApi,
    getUserInfoApi,
    getClaimUserTelphoneApi
} from '../../api/index'
import {
    getResume,
    checkLogin
} from '../../utils/util'

import {
    createSession
} from '../../utils/request'

Component({
    options: {},
    //  组件的属性列表
    properties: {
        option: {
            type: Object,
            value: {}
        },
        showType:{
            type:String,
            value:''
        }
    },
    lifetimes: {
        attached: function() {
            console.log('login')
            wx.login({
                success: (res) => {
                    code = res.code
                },
            })
        },
    },
    //  组件的初始数据
    data: {
        hideFlag: true,
      content:'文案文案文案文案文案文案文案文案文案文案文案文案文案文案文案'
    },

    //  组件的方法列表
    methods: {
        openModal() {
            this.setData({
                hideFlag: false,
            })
            getClaimUserTelphoneApi().then(res=>{
                this.setData({
                    contactName: res.data.content.contractName.substr(0,1)+'经理',
                    contactPhone:res.data.content.contractPhone
                })
            })
        },
        // 联系他
        confirm() {
            wx.makePhoneCall({
                phoneNumber: this.data.contactPhone,
                success: function () {
                    console.log("拨打电话成功！")
                },
                fail: function () {
                    console.log("拨打电话失败！")
                }
            })
            // if (this.data.platform == 'ios') {
            //     getClaimUserTelphoneApi().then(res => {
            //         let value = res.data.content.contractPhone
            //         wx.makePhoneCall({
            //             phoneNumber: value,
            //             success: function() {
            //                 console.log("拨打电话成功！")
            //             },
            //             fail: function() {
            //                 console.log("拨打电话失败！")
            //             }
            //         })
            //     })
            // } else {
            //     wx.navigateTo({
            //         url: '/pages/buyVip/index'
            //     })
            // }
        },


        closeModal() {
            this.setData({
                hideFlag: true
            })
        },
    }
})