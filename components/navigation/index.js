const App = getApp();

Component({
	options: {
		addGlobalClass: true,
	},
	/**
	 * 组件的属性列表
	 */
	properties: {
		pageName: {
			type: String,
			value: ''
		},
		showNav: {
			type: Boolean,
			value: true
		},
		showLocation: {
			type: Boolean,
			value: false
		},
		showHome: {
			type: Boolean,
			value: true
		},
		solid: {
			type: Boolean,
			value: true
		},
		white: {
			type: Boolean,
			value: false
		},
		cityName: {
			type: String,
			value: '选择城市'
		},
		type: {
			type: String,
			value: 1,
		},
		btColor: {
			type: String,
			value: '#fff'
		},
		btBorder: {
			type: String,
			value: '#e8e8e8'
		},
		currenlibraryarr:{
			type:Array,
		},
		oldlibraryarr:{
			type:Array
		},
		post_id:{
			type:String,
			value:""
		}
	},

	/**
	 * 组件的初始数据
	 */
	data: {
		// pageRoute: "",
		// currenlibraryarr:[],
		// oldlibraryarr:[],
	},
	lifetimes: {
		attached: function() {
			var that = this
			wx.getSystemInfo({
				success: function(res) {
					console.log(res)
					var top = res.statusBarHeight + ((res.system[0] == 'i' ? 44 : 48) - 32) / 2
					that.setData({
						top
					})
				},
			})
			// console.log('navH:' + App.globalData.navHeight)
			// console.log('top:' + this.data.top)
			this.setData({
				navH: App.globalData.navHeight
			})
		}
	},
	/**
	 * 组件的方法列表
	 */
	methods: {
		goBack() {
			console.log(this.data.currenlibraryarr)
			console.log(this.data.oldlibraryarr)
			if (this.isTip()) {
				wx.showModal({
					title: '您还未保存，确定退出吗？',
					confirmColor: '#19C7EA',
					cancelColor: '#c2c2c2',
					content: '',
					confirmText: '取消',
					cancelText: '确定',
					success: function(tip) {
						if (tip.cancel) {
							wx.navigateBack({
								delta: 1
							})
						} else {
							return
						}
					},
				})
			} 
			else if (this.isCopy()) {
				wx.showModal({
					title: '提示',
					content: '请确认是否添加雇主联系方式？',
					cancelText: '已复制',
					confirmText: '复制添加',
					success: function(tip) {
						if (tip.confirm) {
							let pages = getCurrentPages()
							let currentPage = pages[pages.length - 1]
							currentPage.copy()
						}
						wx.navigateBack({
							delta: 2
						})
					},
				})
			} 
			else if (this.isHasEdit()) {
				wx.showToast({
					title: '号码库已更改，请点击保存',
					icon: 'none',
					duration: 2000
				})
			} 
			else {
				wx.navigateBack({
					delta: 1
				})
			}

		},
		isTip() {
			console.log(this.data.currenlibraryarr)
			let pages = getCurrentPages() // 获取当前的页面栈
			console.log(pages)
			let currentPage = pages[pages.length - 1]
			console.log(currentPage)
			let routeList = ['pages/numLibrary/index', 'pages/personalInformation/index', 'pages/credentials/index',
				'pages/jobIntention/index', 'pages/selfAssessment/index', 'pages/workExperience/index'
			]
			let flag = false
			// 如果当前页面是某一个号码库页面
			if (currentPage.route == "pages/numLibrary/index") {
				console.log(this.dataset)
				let currenlibraryarr = this.dataset.currenlibraryarr;
				let oldlibraryarr = this.dataset.oldlibraryarr;
				this.data.currenlibraryarr = this.dataset.currenlibraryarr;
				this.data.oldlibraryarr = this.dataset.oldlibraryarr;
				this.setData({
					currenlibraryarr:this.data.currenlibraryarr,
					oldlibraryarr:this.data.oldlibraryarr
				})
				console.log(this.data.currenlibraryarr);
				console.log(this.data.oldlibraryarr);
				if (currenlibraryarr.length !== oldlibraryarr.length) {
					flag = true;
				} else {
					for (let i = 0; i < currenlibraryarr.length; i++) {
						if (currenlibraryarr[i] !== oldlibraryarr[i]) {
							flag = true;
						}
					}
				}
			} else {
				routeList.forEach(item => {
					if (currentPage.route == item) {
						flag = true
					}
				})
			}
			return flag
		},
		isHasEdit() {
			console.log(this.data.currenlibraryarr)
			let pages = getCurrentPages() // 获取当前的页面栈
			console.log(pages)
			let currentPage = pages[pages.length - 1]
			console.log(currentPage)
			if (currentPage.route == "pages/numLibrary/index") {
				// let pageRoute = "pages/numLibrary/index"
				this.setData({
					pageRoute: "pages/numLibrary/index"
				})
			}
			// if(pageRoute == "pages/numLibrary/index" && currentPage.route == "pages/publishJob/index"){
				
			// }
			let flag = false;
			if (currentPage.route == "pages/publishJob/index" && this.data.post_id !== "") { // 判断出他是编辑岗位
				let currenlibraryarr = this.data.currenlibraryarr;
				let oldlibraryarr = this.data.oldlibraryarr;
				console.log(this.data.currenlibraryarr);
				console.log(this.data.oldlibraryarr);
				if (currenlibraryarr.length !== 0 && oldlibraryarr.length !== 0) {
					if (currenlibraryarr.length != oldlibraryarr.length) {
						flag = true;
					} else {
						for (let i = 0; i < currenlibraryarr.length; i++) {
							if (currenlibraryarr[i] != oldlibraryarr[i]) {
								flag = true;
							}
						}
					}
					return flag 
				}
			}
			return flag
		},
		isCopy() {
			let pages = getCurrentPages()
			let currentPage = pages[pages.length - 1]
			let flag = false
			if (currentPage.route == 'pages/applyJobSuccess/applyJobSuccess') {
				flag = true
			}
			return flag
		},
		goHome() {
			wx.switchTab({
				url: '/pages/index/index'
			});
		},
		gotoSelectCityPage() {
			wx.navigateTo({
				url: '/pages/citySelect/index'
			})
		},
	}
})
