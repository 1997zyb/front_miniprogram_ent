
import {openAdDetail} from '../../utils/util'
Component({
    options: {},
    //  组件的属性列表
    properties: {
        adInfo: {type: Object, value:{}}

    },
    lifetimes: {
        attached: function () {
        },
    },
    //  组件的初始数据
    data: {
        hideFlag: true,
    },

    //  组件的方法列表
    methods: {
       gotoWebViewPage(){
           openAdDetail(this.properties.adInfo)
       },
        openModal() {
            var timestamp = Date.parse(new Date());
            var date = new Date(timestamp);
            var Y = date.getFullYear();
            //获取月份  
            var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1);
            //获取当日日期 
            var D = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
            date = Y + M + D
            if (!wx.getStorageSync('popup_limit') || wx.getStorageSync('popup_limit').date != date) {
                let popup_limit = {
                    date: date,
                    count: this.properties.adInfo.popup_limit
                }
                wx.setStorageSync('popup_limit', popup_limit)
            }
            if (wx.getStorageSync('popup_limit').count > 0) {
                let popup_limit = wx.getStorageSync('popup_limit')
                popup_limit.count--
                wx.setStorageSync('popup_limit', popup_limit)
                this.setData({
                    hideFlag: false
                })
            }
        },
        closeModal() {
            this.setData({hideFlag: true})
        },
    }
})
