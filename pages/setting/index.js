import {logoutApi,getAccountThirdPlatBindInfo,unBandingWechatUserApi} from "../../api/index";

let app = getApp()

Page({
    data: {
        imageUrl:'',
        isLogin:true,
        title:'设置',
		contactStock:0,
    },
    onLoad(option) {
		console.log(app.globalData.userInfo)
        this.getAccountThirdPlatBindInfo()
		this.setData({
			contactStock:app.globalData.userInfo.contactStock
		})
    },

    // 获取推送绑定信息
    getAccountThirdPlatBindInfo() {
        getAccountThirdPlatBindInfo().then(res=>{
            this.setData({
                is_bind_wechat_public:res.data.content.is_bind_wechat_public
            })
        })
    },
    
    onShow(){
        this.setData({isLogin:wx.getStorageSync('is_login')})
    },
    onPullDownRefresh() {
        wx.stopPullDownRefresh();
    },
    onShareAppMessage() {
        return {
            title: '城市服务行业一站式招聘平台',
            path: '/pages/index/index',
            imageUrl: 'https://wodan-idc.oss-cn-hangzhou.aliyuncs.com/shijianke-wechat/jiankezhaopin/images/shareMain.png',
            success: (res) => {
                wx.showToast({
                    title: '转发成功',
                })
            },
            fail: () => {
                wx.showToast({
                    title: '转发失败',
                })
            }
        }
    },
    //  退出登录
    loginOut() {
        wx.showModal({
            title: '',
            content: '确定要退出登录吗？',
            cancelColor: '#b1b1b1',
            confirmColor: '#00BCD4',
            success: (res) => {
                if (res.confirm) {
                    logoutApi().then(res => {
                        if (res.data.errCode == 0) {
                            this.setData({isLogin:false})
                            wx.removeStorageSync('user_globalData')
                            wx.setStorageSync('is_login', false)
                            wx.setStorageSync('im_connect', false)
                        }
                    })
                } else {
                }
            }
        })
    },
    gotoAboutUsPage(){
        wx.navigateTo({
          url: '/pages/aboutUs/index'
        })
    },
    gotoSendMessagePage(){
        wx.navigateTo({
          url: '/pages/sendMessage/index'
        })
    },
	// 去编辑号码库
	gotoEditNumLibrary(){
		wx.navigateTo({
		  url: '/pages/numLibraryEdit/index'
		})
	},

    // 跳转至微信通知服务
    gotoWxNotice() {
        if(!this.data.is_bind_wechat_public){
            wx.navigateTo({
                url: '../qrcode/index'
            })
        }else {
            wx.showModal({
                title: '确定关闭微信通知？',
                content: '关闭微信通知服务，将不能继续通过微信公众号接收来自兼客招聘的消息提醒，确定关闭吗？',
                cancelText: '确定关闭',
                confirmText:'再想想',
                success(res) {
                  if (res.cancel) {
                    unBandingWechatUserApi().then(res=>{
                        console.log(res)
                    })
                  }
                }
              })
        }
    }
})

