import {getClaimUserTelphoneApi} from '../../api/index'
let app = getApp()
let modalLogin = {}
Page({
    data: {
        showLogin: false,
        packageList: [
            {title: '闪电招', subTitle: '报名量计费，至尊展位', type: '1', content: '一键发布+至尊展位+急速直达+专人服务'},
            {title: '急速招', subTitle: '报名量计费，最佳性价比', type: '1', content: '一键发布全国+全国曝光+至尊展位+专人服务'},
            {title: 'VIP会员', subTitle: '平台推荐，高效急速招聘', type: '1', content: '精准匹配+刷新置顶+海量简历+专人服务'},
            {title: '广告位', subTitle: '精准投放，平台推荐', type: '1', content: '黄金展位+覆盖全国用户+持续曝光+高效转化'},
            {title: '全国特岗', subTitle: '时间计价，品效合一', type: '1', content: '一键发布+至尊展位+海量曝光+专人服务'},
        ],
        navH:''
    },
    onLoad(option) {
        this.setData({
            navH: app.globalData.navHeight
        })
    },
    onShow() {

        if (!wx.getStorageSync('is_login')) {
            this.openLogin()
        }else if (this.data.showLogin) {
            this.setData({showLogin:false})
        }

    },
    //   分享触发事件
    onShareAppMessage() {
        return {
            title: '兼客招聘商家版',
            path: '/pages/index/index',
            imageUrl:'../../images/share_img.png',
            success: (res) => {
                wx.showToast({
                    title: '转发成功',
                })
            },
            fail: () => {
                wx.showToast({
                    title: '转发失败',
                })
            }
        }
    },
    //  打开登录弹窗
    openLogin() {
        this.setData({showLogin: true})
        modalLogin = this.selectComponent("#modal_login");
        modalLogin.openModal();
    },
    //  关闭登录弹窗
    closeLogin() {

        modalLogin = this.selectComponent("#modal_login");
        modalLogin.closeModal();
        this.setData({showLogin: false})
    },
    onPullDownRefresh() {
        wx.stopPullDownRefresh();
    },
    contact() {
        getClaimUserTelphoneApi().then(res => {
            let value = res.data.content.contractPhone;
            wx.showModal({
                title: '',
                content: '联系销售顾问(电话:'+value+')'+'立即开通套餐 ',
                confirmText: '立即联系',
                cancelColor:'#b1b1b1',
                confirmColor: '#00BCD4',
                cancelText: '取消',
                success: (res) => {
                    if (res.confirm) {
                        wx.makePhoneCall({
                            phoneNumber: value,
                            success: function () {
                                console.log("拨打电话成功！")
                            },
                            fail: function () {
                                console.log("拨打电话失败！")
                            }
                        })
                    } else {
                    }
                }
            })
        })
    },


})

