let app = getApp()

Page({
    data: {

        jobFirstGrade: '',
        title: '更多要求',
        limitAge: '年龄不限',
        limitSex: '性别不限',
        limitHeight: '身高不限',
        limitWokeDate: '不限',
        limitIdentity: '身份不限',
        relNameVerify: false, // 实名认证
        healthCardVerify: false, // 健康证认证
        studentCardVerify: false, // 学生证认证
        lifePhotoVerify: false, // 生活照
        licenseVerify: false, // 驾照
        ageList: [{
            id: 0,
            name: '年龄不限'
        }, {
            id: 2,
            name: '18-24岁'
        }, {
            id: 3,
            name: '25-29岁'
        }, {
            id: 4,
            name: '30-39岁'
        }, {
            id: 5,
            name: '40-49岁'
        }, {
            id: 6,
            name: '50岁以上'
        }],

        sexList: [{
            id: '',
            name: '性别不限'
        }, {
            id: 0,
            name: '仅女生'
        }, {
            id: 1,
            name: '仅男生'
        }],
        heightList: [{
            id: '',
            name: '身高不限'
        }, {
            id: 1,
            name: '160cm以上'
        }, {
            id: 2,
            name: '165cm以上'
        }, {
            id: 3,
            name: '168cm以上'
        }, {
            id: 4,
            name: '170cm以上'
        }, {
            id: 5,
            name: '175cm以上'
        }, {
            id: 6,
            name: '180cm以上'
        }],
        wokeDateList: [{
            id: '',
            name: '不限制'
        }, {
            id: 0,
            name: '全部到岗'
        }, {
            id: 2,
            name: '2天以上'
        }, {
            id: 3,
            name: '3天以上'
        }, {
            id: 5,
            name: '5天以上'
        }],
        identityList: [{
            id: '',
            name: '身份不限'
        }, {
            id: 1,
            name: '仅学生'
        }, {
            id: 2,
            name: '仅社会人士'
        }]
    },
    onLoad(option) {
        this.setAgeList()
        this.setData({
            jobFirstGrade: app.globalData.publishInfo.jobRankType
        })
    },
    onShow() {
        this.initData()
    },
    // 设置年龄列表
    setAgeList() {
        let ageList = [{
            values: ['年龄不限'],
            className: 'min',
            defaultIndex: 0,
        }, {
            values: [],
            className: 'max',
            defaultIndex: 0
        }, ]
        let temp = []
        let tempString = ['']
        for (let i = 16; i <= 60; i++)
            temp.push(i)
        ageList[0].values = ageList[0].values.concat(temp)
        ageList[1].values = tempString.concat(ageList[1].values.concat(temp))
        this.setData({
            ageList: ageList,
            temp: temp
        })
    },
    initData() {
        let date
        switch (app.globalData.publishInfo.limitWokeDate) {
            case 0:
                date = 1
                break
            case 2:
                date = 2
                break
            case 3:
                date = 3
                break
            case 5:
                date = 4
                break
            default:
                date = ''
        }
      app.globalData.publishInfo.age_unLimited = app.globalData.publishInfo.age_low == 0 ? 1 : app.globalData.publishInfo.age_unLimited
        this.setData({
            limitAge: app.globalData.publishInfo.limitAge ? this.data.ageList[app.globalData.publishInfo.limitAge].name : this.data.ageList[0].name,
          limitAge: !app.globalData.publishInfo.age_low || app.globalData.publishInfo.age_low == 0 || app.globalData.publishInfo.age_unLimited == 1 ? '年龄不限' : (app.globalData.publishInfo.age_low+'岁-' + app.globalData.publishInfo.age_high+'岁'),
            limitSex: app.globalData.publishInfo.limitSex || app.globalData.publishInfo.limitSex == 0 ? this.data.sexList[app.globalData.publishInfo.limitSex + 1].name : this.data.sexList[0].name,
            limitHeight: app.globalData.publishInfo.limitHeight ? this.data.heightList[app.globalData.publishInfo.limitHeight].name : this.data.heightList[0].name,
            limitIdentity: app.globalData.publishInfo.limitIdentity ? this.data.identityList[app.globalData.publishInfo.limitIdentity].name : this.data.identityList[0].name,
            limitWokeDate: date ? this.data.wokeDateList[date].name : '不限',
            relNameVerify: app.globalData.publishInfo.relNameVerify ? true : false,
            healthCardVerify: app.globalData.publishInfo.healthCardVerify ? true : false,
            studentCardVerify: app.globalData.publishInfo.studentCardVerify ? true : false,
            lifePhotoVerify: app.globalData.publishInfo.lifePhotoVerify ? true : false,
            licenseVerify: app.globalData.publishInfo.licenseVerify ? true : false,
        })
    },
    onPullDownRefresh() {
        wx.stopPullDownRefresh();
    },
    //  性别选择
    bindSexChange(e) {
        this.setData({
            limitSex: this.data.sexList[e.detail.value].name
        })
        app.globalData.publishInfo.limitSex = this.data.sexList[e.detail.value].id
    },
    //  年龄选择
    // bindAgeChange(e) {
    //     debugger
    //     this.setData({
    //         limitAge: this.data.ageList[e.detail.value].name
    //     })
    //     app.globalData.publishInfo.limitAge = this.data.ageList[e.detail.value].id
    // },
    //  身高选择
    bindHeightChange(e) {
        this.setData({
            limitHeight: this.data.heightList[e.detail.value].name
        })
        app.globalData.publishInfo.limitHeight = this.data.heightList[e.detail.value].id
    },
    //  身份选择
    bindIdentityChange(e) {
        this.setData({
            limitIdentity: this.data.identityList[e.detail.value].name
        })
        app.globalData.publishInfo.limitIdentity = this.data.identityList[e.detail.value].id
    },
    //  到岗天数选择
    bindWokeDateChange(e) {
        this.setData({
            limitWokeDate: this.data.wokeDateList[e.detail.value].name
        })
        app.globalData.publishInfo.limitWokeDate = this.data.wokeDateList[e.detail.value].id
    },
    //  实名认证选择
    relNameVerifyChange() {
        this.setData({
            relNameVerify: !this.data.relNameVerify
        })
        app.globalData.publishInfo.relNameVerify = this.data.relNameVerify ? 1 : ''
    },
    //  生活照选择
    lifePhotoVerifyChange() {
        this.setData({
            lifePhotoVerify: !this.data.lifePhotoVerify
        })
        app.globalData.publishInfo.lifePhotoVerify = this.data.lifePhotoVerify ? 1 : ''
    },
    //  健康证选择
    healthCardVerifyChange() {
        this.setData({
            healthCardVerify: !this.data.healthCardVerify
        })
        app.globalData.publishInfo.healthCardVerify = this.data.healthCardVerify ? 1 : ''
    },
    //  学生证认证选择
    studentCardVerifyChange() {
        this.setData({
            studentCardVerify: !this.data.studentCardVerify
        })
        app.globalData.publishInfo.studentCardVerify = this.data.studentCardVerify ? 1 : ''
    },
    //  驾驶证认证选择
    licenseVerifyChange() {
        this.setData({
            licenseVerify: !this.data.licenseVerify
        })
        app.globalData.publishInfo.licenseVerify = this.data.licenseVerify ? 1 : ''
    },
    //  确定按钮
    save() {
        wx.navigateBack()
    },
    // 性别要求
    showSex() {
        this.setData({
            showSex: true,
        })
    },
    // 性别要求
    bindSexChange(e) {
        app.globalData.publishInfo.limitSex = e.detail.id
        this.setData({
            limitSex: e.detail.name,
            showSex: false,
        })
    },
    // 年龄要求
    showAge() {
        this.setData({
            showAge: true,
        })
    },
    // 年龄要求
    bindAgeChange(e) {
        let val = e.detail.value
        if(val[0]=='年龄不限'){
            app.globalData.publishInfo.age_unLimited = 1
            app.globalData.publishInfo.age_low = ''
            app.globalData.publishInfo.age_high = ''
            this.setData({limitAge:'年龄不限'})
        }else {
            app.globalData.publishInfo.age_unLimited = 0
            app.globalData.publishInfo.age_low = val[0]
            app.globalData.publishInfo.age_high = val[1]
            this.setData({limitAge:val[0]+'岁-'+val[1]+'岁'})
        }
        console.log(e.detail.value)
        this.setData({
            showAge: false,
        })
    },

    // 身份要求
    showLimitIdentity() {
        this.setData({
            showLimitIdentity: true,
        })
    },
    // 身份要求
    bindLimitIdentityChange(e) {
        app.globalData.publishInfo.limitIdentity = e.detail.id
        this.setData({
            limitIdentity: e.detail.name,
            showLimitIdentity: false,
        })
    },

    // 身高要求
    showHeight() {
        this.setData({
            showHeight: true,
        })
    },
    // 身高要求
    bindHeightChange(e) {
        app.globalData.publishInfo.limitHeight = e.detail.id
        this.setData({
            limitHeight: e.detail.name,
            showHeight: false,
        })
    },

    // 最少上岗天数要求
    showWokeDate() {
        this.setData({
            showWokeDate: true,
        })
    },
    // 性别要求
    bindWokeDateChange(e) {
        app.globalData.publishInfo.limitWokeDate = e.detail.id
        this.setData({
            limitWokeDate: e.detail.name,
            showWokeDate: false,
        })
    },
    // 年龄要求列选择
    bindAgeColumnChange(e) {
        const item = e.detail.value
        const index = e.detail.index
        const ageList = this.data.ageList
        const temp = this.data.temp
        const tempString = ['']
        // if (item[0] == '年龄不限') {
        //   ageList[1].values = tempString.concat(temp.slice(0, 60 - 15))
        // } else {
        //     ageList[1].values = temp.slice(item[0]-16,60-15)
        // }
        // console.log(e)
        ageList[0].defaultIndex = ageList[0].values.indexOf(item[0])
        ageList[1].defaultIndex = ageList[1].values.indexOf(item[1])
        if (item[0] == '年龄不限' && index==0){
            ageList[1].defaultIndex=0
            this.setData({
                ageList
            })
        } else if (item[0] == '年龄不限' && index == 1){
          ageList[0].defaultIndex = 1
          this.setData({
            ageList
          })
        }
        if ((ageList[1].defaultIndex <= ageList[0].defaultIndex) && e.detail.index == 0)
            ageList[1].defaultIndex = ageList[0].defaultIndex
        if ((ageList[1].defaultIndex <= ageList[0].defaultIndex) && e.detail.index == 1)
            ageList[0].defaultIndex = ageList[1].defaultIndex
        this.setData({
            ageList
        })
    },
    // 关闭弹窗
    onClose() {
        this.setData({
            showAge: false
        })
    }
})