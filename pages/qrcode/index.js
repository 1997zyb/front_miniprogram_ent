// pages/qrcode/index.js
import drawQrcode from '../../utils/weapp.qrcode.esm.js'
import {getBusinessBindWechatPublicQrCodeApi} from '../../api/index'
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  onLoad: function (options) {
    this.getUrl()
    
  },
  onReady: function () {},
  onShow: function () {},
  // 获取URL 
  getUrl() {
    getBusinessBindWechatPublicQrCodeApi().then(res=>{
      console.log(res)
      drawQrcode({
        width: 80,
        height: 80,
        canvasId: 'myQrcode',
        // ctx: wx.createCanvasContext('myQrcode'),
        text: res.data.content.QrCodeUrl,
        // v1.0.0+版本支持在二维码上绘制图片
      })
    })
  },
  // 保存二维码
  clickSave() {
    let that = this
    wx.canvasToTempFilePath({
      x: 0,
      y: 0,
      canvasId: 'myQrcode',
      success: function (res) {
        let shareImg = res.tempFilePath;
        that.checkSetting(()=>{
          wx.saveImageToPhotosAlbum({
            filePath: shareImg,
            success() {
              wx.showToast({
                title: '保存成功'
              })
            },
            fail() {
              wx.showToast({
                title: '保存失败',
                icon: 'none'
              })
            }
          })
        })
         
      },
      fail: function (res) {}
    })
  },

  // 关闭授权
  cancleSet() {
    this.setData({
      openSet: false
    })
  },
  // 校验保存图片权限
  checkSetting(callback) {
    let that =this
    wx.getSetting({
      success(res) {
        // 如果没有则获取授权
        if (!res.authSetting['scope.writePhotosAlbum']) {
          wx.authorize({
            scope: 'scope.writePhotosAlbum',
            success() {
              callback&&callback()
            },
            fail() {
            // 如果用户拒绝过或没有授权，则再次打开授权窗口
            //（ps：微信api又改了现在只能通过button才能打开授权设置，以前通过openSet就可打开，下面有打开授权的button弹窗代码）
              that.setData({
                openSet: true
              })
            }
          })
        } else {
          callback&&callback()
        }
      }
    })
  }
})