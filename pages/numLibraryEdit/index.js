import {findNumberLibraryApi} from '../../api/index2.js'

let app = getApp();
Page({
	data: {
		version: "",
		numlibrarys: [{
			librarysName: "微信号码库",
			total: null,
			id: 6
		}, {
			librarysName: "QQ号码库",
			total: null,
			id: 7
		}, {
			librarysName: "QQ群号码库",
			total: null,
			id: 8
		}]
	},
	onShow: function() {
		console.log("打印出onShow")
		console.log(app);
		this.showNumLibraryTotal()
	},
	onReady: function() {
		console.log("打印出onReady")
	},
	onLoad: function(options) {
		console.log("打印出onLoad")
		this.setData({
			version: app.globalData.version
		})
	},
	showNumLibraryTotal() {
		//  随机微信号个数
		findNumberLibraryApi({number_type: 1}).then(res => {
			this.data.numlibrarys[0].total = res.data.content.number_library_list.length;
			this.setData({
				numlibrarys:this.data.numlibrarys
			})
		})
		//  随机QQ号个数
		findNumberLibraryApi({number_type: 2}).then(res => {
			this.data.numlibrarys[1].total = res.data.content.number_library_list.length;
			this.setData({
				numlibrarys:this.data.numlibrarys
			})
		})
		//  随机QQ群号个数
		findNumberLibraryApi({number_type: 3}).then(res => {
			this.data.numlibrarys[2].total = res.data.content.number_library_list.length;
			this.setData({
				numlibrarys:this.data.numlibrarys
			})
		})
		console.log(this.data.numlibrarys)
	},
	editNumLibrary(e) {
		console.log(e)
		console.log(e.currentTarget.dataset.id)
		let id = e.currentTarget.dataset.id
		console.log("去编辑")
		wx.navigateTo({
			url: '/pages/numLibrary/index' + '?id=' + id,
		})
	}
})
