
import {request} from '../../utils/request.js'

Page({
    data: {
        inputValue:'',
        paramValue:'',
    },
    onLoad: function (option) {
    },
    onPullDownRefresh: function () {
    },
    test(){
        console.log(this.data.paramValue)
        console.log(typeof(this.data.paramValue))
        
        request({
            url: this.data.inputValue,
            method: 'get',
          params: this.data.paramValue?JSON.parse(this.data.paramValue):''
        }).then(res=>{console.log(res)})
    },
    bindKeyInput(e) {
        this.setData({
            inputValue: e.detail.value
        })
    },
    bindParamInput(e) {
        this.setData({
            paramValue: e.detail.value
        })
    },


})