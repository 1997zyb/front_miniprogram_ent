let app = getApp()

Page({
    data: {
        title: '导航标题'
    },
    onLoad(option) {
    },
    onShow() {
    },
    onUnload() {
    },
    onPullDownRefresh() {
        wx.stopPullDownRefresh();
    },
    setObjProp() {
        // 小程序单独设置data中对象的具体属性值
        this.setData({['obj.id']: 2})
        // 小程序单独设置data中数组中的某个属性
        this.setData({['obj[2].id']: 2})
        this.setData({['obj[$(2)].id']: 2})
    }
}
)

