// pages/selectedUp/index.js
let modalAccurate = {}
Page({
    data: {
        title:'选择上架方式'
    },
    // 什么是精品岗位
    showAccurate() {
        modalAccurate = this.selectComponent("#modal_accurate");
        modalAccurate.openModal()
    },
    selectPublish(e){
        wx.navigateTo({
            url: '/pages/publishJobType/index?type='+e.currentTarget.dataset.type
        })
    }
})