
let app = getApp()

Page({
    data: {
        sendMessage:true
    },
    onLoad(option) {},
    onShow(){},
    onUnload() {},
    onPullDownRefresh() {
        wx.stopPullDownRefresh();
    },
    sendMessageChange(){
        this.setData({sendMessage: !this.data.sendMessage})
    },
})

