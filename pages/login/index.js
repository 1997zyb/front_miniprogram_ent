// pages/login/login.js
import { loginApi, getSmsAuthenticationCodeApi, loginByphoneApi} from '../../api/index'
import { createSession } from '../../utils/request'
import {getResume} from "../../utils/util";
var app = getApp();
var common = require('../../utils/common.js')
var md5 = require('../../utils/md5.js')
var warnImgUrl = '../../images/warn.png';
var oauth_id = '';
var submitEvent = {
  check: {
    checkPhone: function (inputValue) {
      var isCheckPhone = new RegExp(/(?:^1\d{10}$)|(?:^0\d{2,3}-?\d{7,9}$)/).test(inputValue);
      if (!isCheckPhone) {
        wx.showToast({
          title: '请输入11位有效手机号',
          icon:'none',
          duration: 2000
        });
        return false;
      }
      return true;
    },
    CheckCode: function (inputValue) {
      var isCheckCode = /^\S{6}$/.test(inputValue);
      if (!isCheckCode) {
        wx.showToast({
          title: '请输入动态密码',
          icon:'none',
          duration: 2000
        });
        return false;
      }
      return true;
    },
    CheckPwd: function (inputValue) {
      var isCheckPwd = /^\S{6,20}$/.test(inputValue);
      if (!isCheckPwd) {
        wx.showToast({
          title: '请输入6-20位密码',
          icon:'none',
          duration: 2000
        });
        return false;
      }
      return true;
    },
    checkAll: function (that,loginType) {
      var self = this,
        inputPhone = that.data.inputPhone,
        inputCode = that.data.inputCode,
        inputPwd = that.data.inputPwd,
        isCheckd = false;
      if (loginType == 1){
        var CheckPwd = self.CheckPwd(inputPwd);
        var checkPhone = self.checkPhone(inputPhone);

        isCheckd = (CheckPwd && checkPhone);
      }
      if (loginType == 2) {
        var CheckCode = self.CheckCode(inputCode);
        var checkPhone = self.checkPhone(inputPhone);

        isCheckd = (CheckCode && checkPhone);
      }
      return isCheckd
    }
  },
  countDown: {
    _resetCountDown: function (that) {
      var self = this;
      self._isGettingVCode = false;
      if (self._countDownInterval) {
        clearInterval(self._countDownInterval);
      }
      that.setData({
        getVCodeText: '获取验证码'
      })
      return self;
    },
    _countDown: function (that) {
      var self = this;
      self._isGettingVCode = true;
      var seconds = common.COUNT_DOWN_TIME;
      if (self._countDownInterval) {
        clearInterval(self._countDownInterval);
      }
      self._countDownInterval = setInterval(function () {
        if (seconds == 0) {
          self._resetCountDown(that);
          return;
        }
        that.setData({
          getVCodeText: [seconds--, '秒后重发'].join('')
        })
      }, 1000);
      return self;
    },
    _isGettingVCode: false
  }
}
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isMessageLogin: false,
    inputPhone: '',
    inputCode: '',
    inputPwd: '',
    getVCodeText: '获取验证码',
    loginType: '动态密码登录',
    challenge: '',
    loginCallbackUrl: ''
  },
  onLoad: function (option) {
    var that = this;
    var challenge = wx.getStorageSync('challenge');
    var loginCallbackUrl = wx.getStorageSync('loginCallbackUrl');
    that.setData({
      challenge: challenge,
      loginCallbackUrl: loginCallbackUrl
    });
  },
  /**
   * 重新进入登录页面，重新请求oauth_id
   */
  onShow: function () {
    createSession().then(()=>{
      wx.login({
        success:res=>{
          loginApi( {
            code: res.code,
            login:1
          }).then(res=>{
            oauth_id = res.data.content.oauth_id
          })
        },
        fail:function(){
          wx.reLaunch({
            url: '../index/index',
          })
          console.log('调取登录凭证接口失败')
        }
      })
    })

  },
  /**
   * 切换短信验证登录
   */
  goMessageLogin: function () {
    var isMessageLogin = this.data.isMessageLogin;
    if (isMessageLogin) {
      this.setData({
        isMessageLogin: false,
        loginType: '账号密码登录',
        inputCode: ''
      })
    } else {
      this.setData({
        isMessageLogin: true,
        loginType: '动态密码登录',
        inputPwd: ''
      })
    }
  },
  /**
   * 输入框输入事件
   */
  inputEvent: function (e) {
    var inputType = e.currentTarget.dataset.type;
    var inputValue = e.detail.value;
    switch (inputType) {
      case 'phoneNumber':
        this.setData({
          inputPhone: inputValue
        });
        break;
      case 'code':
        this.setData({
          inputCode: inputValue
        });
        break;
      case 'password':
        this.setData({
          inputPwd: inputValue
        });
        break;
    }
  },
  /**
   * 输入框失焦事件
   */
  blurEvent: function (e) {
    var inputType = e.currentTarget.dataset.type;
    var inputValue = e.detail.value;
    switch (inputType) {
      case 'phoneNumber':
        submitEvent.check.checkPhone(inputValue);
        break;
      case 'code':
        submitEvent.check.CheckCode(inputValue);
        break;
      case 'password':
        submitEvent.check.CheckPwd(inputValue);
        break;
    }
  },
  /**
   * 获取验证码
   */
  getVCode: function (e) {

    var that = this,
        inputPhone = that.data.inputPhone;
    if (submitEvent.check.checkPhone(inputPhone)) {
      var isGettingVCode = submitEvent.countDown._isGettingVCode;
      if (isGettingVCode) {
        return;
      }
      getSmsAuthenticationCodeApi({
        port: 'weChat',
        phone_num: inputPhone,
        opt_type: 7,
        user_type: 2
      }).then(res=>{
        if (res.data.errCode != 0) {
          submitEvent.countDown._resetCountDown(that);
          wx.showToast({
            title: res.data.errMsg,
            icon:'none',
            duration: 2000
          });
        }
      })
      submitEvent.countDown._countDown(this);
    }
  },
  goRegister: function () {
    wx.navigateTo({
      url: '../register/register',
    })
  },
  /**
   * 登录
   */
  toSubmit: function () {
    var username = this.data.inputPhone,
        pwd = this.data.inputPwd,
        code = this.data.inputCode,
        challenge = this.data.challenge
    if (pwd) {
      var password = md5.hexMD5(pwd + challenge);
    }
    var loginType = this.data.isMessageLogin ? 2 : 1;
    var isChecked = submitEvent.check.checkAll(this, loginType);
    var data = {};
    if (password) {
      data = {
        username: username,
        password: password ? password : null,
        user_type: 2,
        oauth_id: oauth_id ? oauth_id : null
      }
    } else {
      data = {
        username: username,
        dynamic_sms_code: code ? code : null,
        user_type: 2,
        oauth_id: oauth_id ? oauth_id : null
      }
    }
    if (isChecked) {
      loginByphoneApi(data).then(res=>{
        if (res.data.errCode != 0) {
          wx.showToast({
            title: res.data.errMsg,
            icon:'none',
            duration: 2000
          });
        } else {
          wx.setStorage({
            key: "is_login",
            data: true
          });
          app.globalData.jobChange=true
          // wx.reLaunch({
          //   url: '../index/index'
          // })
          getResume()
        }
      })
    }
  },
})
