import {findNumberLibraryApi,editNumberLibraryApi} from "../../api/index2.js";

let app = getApp();
Page({
	data: {
		version: "",
		numList: [],
		currentLibraryList: [], // 当前输入框值
		oldLibraryList: [], // 旧的输入框值
		title: "",
		options: {},
	},
	// onLoad监听页面加载，一个页面只会被调用一次
	// 可以在onLoad中获取当前页面所调用的query函数
	onLoad: function(options) {
		this.data.options = options;
		if (this.data.options.id == 6) {
			this.setData({
				title: "微信号码库"
			})
		} else if (this.data.options.id == 7) {
			this.setData({
				title: "QQ号码库"
			})
		} else if (this.data.options.id == 8) {
			this.setData({
				title: "QQ群号码库"
			})
		}
	},
	// 监听页面显示，每次打开页面都会调用一次，
	onShow: function() {
		let params = {
		        number_type: this.data.options.id - 5,
		    };
		findNumberLibraryApi(params).then(res=>{		
			if(res.data.errCode == 0){
				this.setData({
					numList:res.data.content.number_library_list
				})
				this.data.numList.forEach(item => {
					if (item.numberStr !== "")
						this.data.oldLibraryList.push(item.numberStr);
						this.data.currentLibraryList.push(item.numberStr);
				})
				this.setData({
					version: app.globalData.version,
					oldLibraryList: this.data.oldLibraryList,
					currentLibraryList:this.data.currentLibraryList,
				})
			}
			// 判断输入框是否有3个，至少为3个
			if (this.data.numList.length >= 3) {
				return this.setData({
					numList: this.data.numList
				})
			} else {
				let less = 3 - this.data.numList.length
				for (let i = 0; i < less; i++) {
					this.data.numList.push({
						numberStr: ''
					})
				}
				return this.setData({
					numList: this.data.numList
				})
			}	
		})	
	},
	// 监听页面初次渲染完成，一个页面只会调用一次
	// 表示页面已经准备完成，可以和视图层进行交互
	onReady() {
	},
	// 添加号码
	addNumInput() {
		console.log(this.data.numList)
		if (this.data.numList.length >= 50) {
			wx.showToast({
				title: '最多添加50个号码',
				icon: 'none',
				duration: 2000
			})
			return false
		} else {
			this.data.numList.push({
				numberStr: ""
			})
			this.setData({
				numList: this.data.numList
			})
		}
	},
	inputBlur(e) {
		this.data.currentLibraryList = [];
		let index = e.currentTarget.dataset.index //当前点击了哪一个输入框
		this.data.numList[index].numberStr = e.detail.value
		this.data.numList.forEach(item => {
			if (item.numberStr !== "") {
				this.data.currentLibraryList.push(item.numberStr)
			}
		})
		this.setData({
			numList: this.data.numList,
			currentLibraryList: this.data.currentLibraryList
		})
	},
	// 删除号码
	delInput(e) {
		this.data.currentLibraryList = [];
		let index = e.currentTarget.dataset.index
		this.data.numList.splice(index, 1)
		this.data.numList.forEach(item => {
			if (item.numberStr !== "") {
				this.data.currentLibraryList.push(item.numberStr)
			}
		})
		this.setData({
			numList: this.data.numList,
			currentLibraryList: this.data.currentLibraryList
		})
	},
	// 清空号码
	clearBoth() {
		this.data.numList.forEach(item => {
			item.numberStr = "";
		})
		this.setData({
			numList: this.data.numList,
			currentLibraryList:[]
		})
	},
	// 保存号码
	formSubmit(e) {
		let resNumList = [];
		this.data.numList.forEach(item => {
			if (item.numberStr !== "") {
				resNumList.push(item.numberStr)
			}
		})
		console.log(resNumList)
		this.data.currentLibraryList = resNumList;
		this.setData({
			currentLibraryList: this.data.currentLibraryList,
		})
		console.log(this.data.currentLibraryList)
		let pat = /[\u4e00-\u9fa5]/g // 有汉字
		const isHasChinese = resNumList.some((item) => {
			return pat.test(item)
		})
		if (isHasChinese) {
			return wx.showToast({
				title: '号码中不能有汉字',
				icon: 'none',
				duration: 2000
			})
		}
		if (this.data.options.id == 7 || this.data.options.id == 8) {
			const isQQ = resNumList.every((item) => {
				let qqPat = /^[1-9][0-9]{4,10}$/g // QQ号规范
				return qqPat.test(item)
			})
			console.log(isQQ)
			if (!isQQ) {
				return wx.showToast({
					title: '请输入规范QQ号',
					icon: 'none',
					duration: 2000
				})
			}
		}
		if (this.data.options.id == 6) {
			const isWX = resNumList.every((item) => {
				let wxPat = /^[a-zA-Z0-9][-_a-zA-Z0-9]{5,19}$/g; // 微信号规范
				return wxPat.test(item);
			});
			if (!isWX) {
				return wx.showToast({
					title: '请输入规范微信号',
					icon: 'none',
					duration: 2000
				})
			}
		}
		if (!isHasChinese && resNumList.length !== 0) {
			let resStr = resNumList.toString().replace(/\s*/g, '')
			let params = {
			        number_type: this.data.options.id - 5,
			        number_strings: resStr,
			    };
			editNumberLibraryApi(params).then(res=>{
				let publish = app.globalData.publishInfo
				// let post = app.globalData.postInfo
				publish.contact = {}
				console.log(publish)
				// publish.contact.contact_type = null
				// post.contact = {}
				// post.contact.contact_type = null
				console.log(res)
				if(res.data.errCode == 0){
					// let eventChannel = this.getOpenerEventChannel()
					wx.showToast({
						title: '保存成功',
						icon: 'success',
						duration: 2000
					});
					let pages = getCurrentPages() // 获取当前的页面栈
					console.log(pages)
					let currentPage = pages[pages.length - 1] //当前页面
					let prevPage = pages[pages.length-2] // 上个页面
					if(currentPage.route == "pages/numLibrary/index" && prevPage.route == "pages/publishJob/index"){
						prevPage.setData({
							dataFromNumLibrary:[this.data.currentLibraryList,this.data.oldLibraryList]
						})
					}
					if(this.data.options.id == 6){
							publish.contact.contact_num = '随机微信号：' + resNumList.length + '个';
							publish.contact.contact_type = 6
							// post.contact.contact_num = '随机微信号：' + resNumList.length + '个';
							// post.contact.contact_type = 6
					}
					else if(this.data.options.id == 7){
							publish.contact.contact_num = '随机QQ号：' + resNumList.length + '个';
							publish.contact.contact_type = 7
							// post.contact.contact_num = '随机QQ号：' + resNumList.length + '个';
							// post.contact.contact_type = 7
					}
					else if(this.data.options.id == 8){
							publish.contact.contact_num = '随机QQ群号：' + resNumList.length + '个';
							publish.contact.contact_type = 8
							// post.contact.contact_num = '随机QQ群号：' + resNumList.length + '个';
							// post.contact.contact_type = 8
					}
					console.log(publish)
					setTimeout(() => {
						wx.navigateBack({
							delta:1,
							success(){
								prevPage.numChange()
							}
						})
					}, 1000)
				}	
			})
		} else {
			wx.showToast({
				title: '至少添加一个号码',
				icon: 'none',
				duration: 2000
			})
		}
	}
})
