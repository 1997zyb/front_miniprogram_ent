import {getResumeDetailApi} from '../../api/index'
import {parseTime} from '../../utils/util'

let common = require('../../utils/common.js');
let warnImgUrl = '../../images/warn.png';
let submitEvent = {
    check: {
        checkPhone: function (inputValue) {
            let isCheckPhone = common.REG.TELPHONE.test(inputValue);
            if (!isCheckPhone) {
                wx.showToast({
                    title: '请绑定手机号码',
                    image: warnImgUrl,
                    duration: 2000
                });
                return false;
            }
            return true;
        },
        checkName: function (inputValue) {
            let isCheckNAME = common.REG.NAME.test(inputValue);
            if (!isCheckNAME) {
                wx.showToast({
                    title: '请输入真实姓名',
                    image: warnImgUrl,
                    duration: 2000
                });
                return false;
            }
            return true;
        },
        checkSex: function (inputValue) {
            let isCheckCode = inputValue.length > 0 ? true : false;
            if (!isCheckCode) {
                wx.showToast({
                    title: '请选择性别',
                    image: warnImgUrl,
                    duration: 2000
                });
                return false;
            }
            return true;
        },
        checkDate: function (inputValue) {
            let isCheckDate = inputValue.length > 0 ? true : false;
            if (!isCheckDate) {
                wx.showToast({
                    title: '请选择性别出生日期',
                    image: warnImgUrl,
                    duration: 2000
                });
                return false;
            }
            return true;
        },
        checkAll: function (that) {
            let self = this,
                inputPhone = that.data.inputPhone,
                inputName = that.data.inputName,
                inputSex = that.data.inputSex,
                inputDate = that.data.inputDate,
                isCheckd = false;
            if (!self.checkPhone(inputPhone) || !self.checkName(inputName) || !self.checkSex(inputSex) || !self.checkDate(inputDate)) {
                return isCheckd
            } else {
                isCheckd = true;
                return isCheckd;
            }
        }
    }
}
let app = getApp()
let photoModal = {}
let modalLogin = {}
Page({
    data: {
        navHeight:app.globalData.navHeight,
        isFromTalentPage:false,
        picArrowRight: '../../images/common/arrow/arrow-right.png',//图片
        picPen: '../../images/common/pen.png',//图片
        imageCard: ['https://wodan-idc.oss-cn-hangzhou.aliyuncs.com/shijianke-wechat/jiankezhaopin/images/studentCard.png',
            'https://wodan-idc.oss-cn-hangzhou.aliyuncs.com/shijianke-wechat/jiankezhaopin/images/healthy_card.png',
            'https://wodan-idc.oss-cn-hangzhou.aliyuncs.com/shijianke-wechat/jiankezhaopin/images/drive_card.png',
            'https://wodan-idc.oss-cn-hangzhou.aliyuncs.com/shijianke-wechat/jiankezhaopin/images/level_card.png',],
        inputPhone: '',
        workYears: 0,
        inputName: '',
        inputSex: '',
        inputDate: '',
        inputType: '',
        sex: '',
        date: '',
        type: '',
        showModal: {
            type: '',
            isModal: false,
            btnData: []
        },
        isBind: false,
        id_card_verify_status: '',//实名认证状态（ 1:未认证  2：认证中 3：认证通过  4：认证驳回）
        stu_id_card_no: '',//学生证号
        stu_id_card_url: '',// 学生证图片地址
        health_cer_no: '',//健康证号
        health_cer_url: '',//健康证图片地址
        isApply: false,
        isPublic: false,
        resume: {},
        isUpLoad: false,
        showLogin: false,
        jobStatusArray: ['暂时不考虑', '随时到岗', '在职考虑机会'],
      cardTypeArray: ['学生证', '健康证', '驾驶证', '其它'],
        jobTypeArray: ['全部都可以', '兼职', '全职',  '在线工作'],
        jobTimeArray: ['不限', '周末', '工作日', '寒暑假', '节假日'],
        userTypeDesc: ['上班族', '学生党'],
        dailyPost: ['0', '100', '200', '300', '400', '500', '600', '700', '800', '900', '1000', '1500', '2000', '2500', '3000', '3500', '4000', '4500', '5000', '5500', '6000', '6500', '7000', '7500', '8000', '8500', '9000', '9500', '10000'],
    daily: [
      ['0', '100', '200', '300', '400', '500', '600', '700', '800', '900', '1000', '1500', '2000', '2500', '3000', '3500', '4000', '4500', '5000', '5500', '6000', '6500', '7000', '7500', '8000', '8500', '9000', '9500'],
      ['100', '200', '300', '400', '500', '600', '700', '800', '900', '1000', '1500', '2000', '2500', '3000', '3500', '4000', '4500', '5000', '5500', '6000', '6500', '7000', '7500', '8000', '8500', '9000','9500', '10000']
    ],
    monthlyPost: ['0', '1000', '2000', '3000', '4000', '5000', '6000', '7000', '8000', '9000', '10000', '11000', '12000', '13000', '14000', '15000', '16000', '17000', '18000', '19000', '20000', '21000', '22000', '23000', '24000', '25000', '26000', '27000', 28000, '29000', '30000', '31000'],
        monthly: [['0', '1000', '2000', '3000', '4000', '5000', '6000', '7000', '8000', '9000', '10000', '11000', '12000', '13000', '14000', '15000', '16000', '17000', '18000', '19000', '20000', '21000', '22000', '23000', '24000', '25000', '26000', '27000', '28000', '29000', '30000'], ['1000', '2000', '3000', '4000', '5000', '6000', '7000', '8000', '9000', '10000', '11000', '12000', '13000', '14000', '15000', '16000', '17000', '18000', '19000', '20000', '21000', '22000', '23000', '24000', '25000', '26000', '27000', '28000', '29000', '30000', '30000以上'],],
        educationList: [{id: 0, name: ''}, {id: 1, name: '初中及以下'}, {id: 2, name: '中专'}, {id: 3, name: '高中'}, {
            id: 4,
            name: '专科'
        }, {id: 5, name: '本科'}, {id: 6, name: '硕士'}, {id: 7, name: '博士'}],
        imglistCertificate:[],
        imglistLivePhoto:[]
    },
    onLoad(option) {
        this.getResume(option)
    },
    onShow() {

    },
    getInfo() {
    },
    //  获取简历信息
    getResume(option) {
        if(option.readResumePage==1){
            this.setData({isFromTalentPage:true})
        }

        let param = {
            resume_id: option.resume_id,
            read_resume_page: option.readResumePage?option.readResumePage:3,
            apply_job_id: option.apply_job_id?option.apply_job_id:'',
            is_support_free_resume_num: 1
        }
        getResumeDetailApi(param).then(res => {
            if (res) {
                if (res.data.content.resume_experience_list) {
                    let experienceList = res.data.content.resume_experience_list
                    experienceList.forEach(item => {
                        item.job_end_time = parseTime(item.job_end_time, '{y}-{m}-{d}')
                        item.job_begin_time = parseTime(item.job_begin_time, '{y}-{m}-{d}')
                    })
                }
                // let isUpLoad = false
                // if (res.data.content.profile_url.indexOf("Upload") != -1) {
                //     isUpLoad = true
                // }
                this.setData({resume: res.data.content, 
                    // isUpLoad: isUpLoad
                })
                // this.initPage(res.data.content)
                this.setOldData(res.data.content)
                app.globalData.userInfo = res.data.content;
                // wx.setStorageSync('user_globalData', res.data.content);
                let monthlyPost = this.data.monthlyPost
                let monthly = this.data.monthly
                res.data.content.resume_certificate_list.forEach(item=>{
                    this.data.imglistCertificate.push(item.photo_url)
                })
                res.data.content.life_photo.forEach(item=>{
                    this.data.imglistLivePhoto.push(item.life_photo)
                })
                let salaryType = res.data.content.expect_job_type == 1 ? 'daily' : 'monthly'
        let salaryTypePost = this.data[salaryType + 'Post']
        let salaryTypeList = this.data[salaryType]
        this.setData({
          expect_salary_min: (res.data.content.expect_salary_min || res.data.content.expect_salary_min == 0) ? salaryTypeList[0][salaryTypePost.indexOf(res.data.content.expect_salary_min.toString())] : '',
          expect_salary_max: (res.data.content.expect_salary_max || res.data.content.expect_salary_max == 0) ? salaryTypeList[1][salaryTypePost.indexOf(res.data.content.expect_salary_max.toString()) - 1] : '',
          workTime: this.data.jobTimeArray[res.data.content.expect_work_time_type == -1 ? 0 : res.data.content.expect_work_time_type],
                    workTime: this.data.jobTimeArray[res.data.content.expect_work_time_type == -1 ? 0 : res.data.content.expect_work_time_type],
                    // workYears: res.data.content.start_work_time?this.setWorkYears(res.data.content.start_work_time):'无工作经验',
                    workYears: res.data.content.work_year ? res.data.content.work_year  : '无工作经验',
                    jobType: this.data.jobTypeArray[res.data.content.expect_job_type == -1 ? 0 : res.data.content.expect_job_type],
                    imglistCertificate: this.data.imglistCertificate,
                    imglistLivePhoto: this.data.imglistLivePhoto,
                })
            }
        })
    },
    // 计算工龄
    setWorkYears(startTime) {
        if (!startTime) {
            return false
        }
        let year = parseInt(parseTime((new Date()).valueOf()).substr(0, 4))
        let month = parseInt(parseTime((new Date()).valueOf()).substr(5, 2))
        let startYear = parseInt(startTime.substr(0, 4))
        let startMonth = parseInt(startTime.substr(5, 2))
        return parseInt((((year - startYear) * 12 + month - startMonth) / 12) < 1 ? 1 : (((year - startYear) * 12 + month - startMonth) / 12))
    },
    //  跳转意向职位选择页面
    gotoJobIntentionPage() {
        wx.getStorage({
            key: 'is_login',
            success: res => {
                if (res.data == true) {
                    wx.navigateTo({
                        url: '../jobIntentionInfo/index'
                    })
                } else {
                    this.setData({loginShow: true})
                }
            },
            fail: err => {
                this.setData({loginShow: true})

            }
        })

    },
    // 打电话
    callPhone() {
        wx.makePhoneCall({
            phoneNumber: this.data.resume.telphone,
        })
    },
    // 复制微信号
    copyWx() {
        wx.setClipboardData({
            data: this.data.resume.wechat_number,
        })
    },
    gotoEducationExperiencePageModify(e) {
        let index = e.currentTarget.dataset.num
        wx.navigateTo({
            url: '/pages/educationExperience/index?index=' + index
        })
    },
    // 跳转至证件页面
    gotoCardPage(e) {
        let index = ''
        if (e.currentTarget.dataset.num || e.currentTarget.dataset.num == 0)
            index = '?index=' + e.currentTarget.dataset.num
        wx.navigateTo({
            url: '/pages/cardInfo/index' + index
        })
    },
    //  初始化数据
    initPage(data) {
        let inputSex = "";
        if (data.sex == 1) {
            inputSex = "男";
        } else if (data.sex == 0) {
            inputSex = "女";
        }
        let inputType = "";
        if (data.user_type == 1) {
            inputType = "学生";
        } else if (data.user_type == 0) {
            inputType = "社会人士";
        }
        let length = data.account_telphone ? data.account_telphone.length : 0;
        let isBind = length ? false : true;
        this.setData({
            inputPhone: data.account_telphone ? data.account_telphone : '',
            inputName: data.true_name ? data.true_name : '',
            inputSex: inputSex,
            inputDate: data.birthday ? data.birthday : '',
            inputType: inputType,
            sex: data.sex ? data.sex : '',
            date: data.birthday ? data.birthday : '',
            type: data.user_type ? data.user_type : '',
            isBind: isBind,
            id_card_verify_status: data.id_card_verify_status ? data.id_card_verify_status : '',
            stu_id_card_no: data.stu_id_card_no ? data.stu_id_card_no : '',
            stu_id_card_url: data.stu_id_card_url ? data.stu_id_card_url : '',
            health_cer_no: data.health_cer_no ? data.health_cer_no : '',
            health_cer_url: data.health_cer_url ? data.health_cer_url : '',
        });
    },
    // 兼容旧数据
    setOldData() {
        let workExperienceList = this.data.resume.resume_experience_list
        workExperienceList.forEach(item => {
            if (!item.corp_name) {
                item.job_begin_year_month = parseTime(item.job_begin_time).substr(0, 7)
                item.job_end_year_month = parseTime(item.job_end_time).substr(0, 7)
                item.corp_name = '',
                    item.salary = ''
            }
        })

    },
    //  图片预览
    previewImageCertificate(e){
        var current = e.currentTarget.dataset.src;
        wx.previewImage({
            current: current, // 当前显示图片的http链接
            urls: this.data.imglistCertificate // 需要预览的图片http链接列表
        })
    },
    previewImageLivePhoto(e){
        var current = e.currentTarget.dataset.src;
        wx.previewImage({
            current: current, // 当前显示图片的http链接
            urls: this.data.imglistLivePhoto // 需要预览的图片http链接列表
        })
    }


})
