
let app = getApp()
import {getJobClassifyFirstSecondListApi} from '../../api/index'

Page({
    data: {
        title:'发布职位',
        jobType:3,
        firstGradeId:'',
        secondGradeId:'',
        JobClassifyList:[],
        isEdit:false,
        postId:'',
        intoView:''
    },
    onLoad(option) {
        if(option.postId){
            this.setData({isEdit:true,title:'编辑职位'})
            this.setData({jobType:app.globalData.postInfo.job_classify_type,postId:option.postId})
        }else {
            app.globalData.publishInfo.jobRankType = 3
        }
        // this.setData({selectJobType:option.type})
        this.init()
    },
    onShow(){},
    onUnload() {

    },
    init(){
        getJobClassifyFirstSecondListApi().then(res=>{
            let newList = [[],[],[]]
            res.data.content.first_level_job_classify_list.forEach((item,index)=>{
                item.viewId = 'view'+index
                if(item.second_level_job_classify_list.length>0){
                    if(item.first_level_job_classify_type==1){
                        newList[0].push(item)
                    }
                    if(item.first_level_job_classify_type==2){
                        newList[1].push(item)
                    }
                    if(item.first_level_job_classify_type==3){
                        newList[2].push(item)
                    }
                }
            })
            console.log(newList)
            this.setData({JobClassifyList:newList})
            if(this.data.isEdit){
                newList.forEach((item)=>{
                    item.forEach((item2,index2)=>{

                        if(item2.second_level_job_classify_list.length>0){
                            item2.second_level_job_classify_list.forEach((item3)=>{
                                if(item3.id==app.globalData.postInfo.job_type_id){
                                    this.setData({firstGradeId:item2.first_level_job_classify_id,secondGradeId:item3.id})
                                    app.globalData.publishInfo.fistGradeId = item2.first_level_job_classify_id
                                    app.globalData.publishInfo.secondGradeId = item3.id
                                }
                            })
                        }
                    })
                })

            }else {
                app.globalData.publishInfo.fistGradeId = this.data.JobClassifyList[this.data.jobType-1][0].first_level_job_classify_id
                app.globalData.publishInfo.secondGradeId = this.data.JobClassifyList[this.data.jobType-1][0].second_level_job_classify_list[0].id
                this.setData({firstGradeId:app.globalData.publishInfo.fistGradeId,secondGradeId:app.globalData.publishInfo.secondGradeId})
            }
        })
    },
    onPullDownRefresh() {
        wx.stopPullDownRefresh();
    },
    jobTypeChange (e){
        this.setData({jobType:e.currentTarget.dataset.index})
        this.setData({
            firstGradeId:this.data.JobClassifyList[this.data.jobType-1][0].first_level_job_classify_id,
            secondGradeId:this.data.JobClassifyList[this.data.jobType-1][0].second_level_job_classify_list[0].id
        })
        app.globalData.publishInfo.jobRankType = e.currentTarget.dataset.index
        app.globalData.publishInfo.fistGradeId = this.data.JobClassifyList[this.data.jobType-1][0].first_level_job_classify_id
        app.globalData.publishInfo.secondGradeId = this.data.JobClassifyList[this.data.jobType-1][0].second_level_job_classify_list[0].id
    },
    firstGradeChange (e){
        this.setData({
            intoView: e.currentTarget.dataset.view,
            firstGradeId:e.currentTarget.dataset.id,
            secondGradeId:e.currentTarget.dataset.item.second_level_job_classify_list[0].id
        })
        console.log(this.data.intoView)
        app.globalData.publishInfo.fistGradeId = e.currentTarget.dataset.id
        app.globalData.publishInfo.secondGradeId = e.currentTarget.dataset.item.second_level_job_classify_list[0].id
    },
    secondGradeChange (e){
        this.setData({secondGradeId:e.currentTarget.dataset.id})
        app.globalData.publishInfo.secondGradeId = e.currentTarget.dataset.id
    },
    verify(){

    },
    next(){
        if(!app.globalData.publishInfo.secondGradeId){
            wx.showToast({
                title: '请选择要发布的职位分类',
                icon: 'none'
            })
            return false
        }else {
            console.log(app.globalData.publishInfo)
            wx.navigateTo({
                url: '/pages/publishJob/index?postId=' + this.data.postId 
                // + '&type=' + this.data.selectJobType
            })
        }

    }
})

