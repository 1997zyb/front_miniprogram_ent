// pages/vipRecord/index.js
import {
    entQueryVipUseageDetailApi

} from '../../api/index'
const app = getApp()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        title: '使用明细（精准简历）',
        index: 0,
        noData: false,
        noMoreData: false,
        pageNum: 1,
        pageSize: 20,
        list: [],
        queryType: 1,
    },
    onLoad: function(options) {
        var that=this
        wx.getSystemInfo({
            success: function (res) {
                var top = res.statusBarHeight + ((res.system[0] == 'i' ? 44 : 48) - 32) / 2
                that.setData({
                    maxHeight: res.screenHeight - app.globalData.navHeight,
                    height: app.globalData.navHeight
                })
            },
        })
        this.getInfo()
    },
    getInfo() {
        entQueryVipUseageDetailApi({
            page_num: this.data.pageNum,
            page_size: this.data.pageSize,
            query_type: this.data.queryType==0?1:2
        }).then(res => {
            let list=[]
            // list.push(res.data.content.accurate_job_use_detail_list)
            // list.push(res.data.content.accurate_resume_use_detail_list)
            this.setData({
                list: res.data.content.accurate_resume_use_detail_list,
                noData: res.data.content.accurate_resume_use_detail_list == undefined || res.data.content.accurate_resume_use_detail_list.length==0?true:false
            })
        })
    },
    selectIndex(e) {
        this.setData({
            index: e.currentTarget.dataset.index
        })
    },
    // // 上拉加载
    // onReachBottom() {
    //     if (this.data.noMoreData || this.data.collectionList.length == 0)
    //         return
    //     this.setData({
    //         pageNum: this.data.pageNum + 1
    //     })
    //     this.getInfo()
    // },

    // // 下拉刷新
    // onPullDownRefresh() {
    //     this.setData({
    //         number: 1,
    //         collectionList: [],
    //         noMoreData: false,
    //         noData: false,
    //     })
    //     wx.stopPullDownRefresh()
    //     this.getInfo()
    // },
})