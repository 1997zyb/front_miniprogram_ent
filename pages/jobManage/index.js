import {getQueryApplyJobListApi, refuseOrEmployApplicantApi,getInfoByAccountIdApi,getTalentContactApi} from '../../api/index'
import Toast from '../../components/van-weapp/toast/toast';

Page({
    data: {
        optionParam: '',
        jobUuid: '',
        jobTitle: '',
        refuseReasonShow: false,
        applyJobId: '',
        selectApply:null,
        tabList: [{data: [], title: '待处理', index: 0,nodataTitle:'暂时没有待处理兼客',noMoreData:false},
            {data: [], title: '合适', index: 1,nodataTitle:'暂时没有合适兼客',noMoreData:false},
            {data: [], title: '不合适', index: 2,nodataTitle:'暂时没有不合适兼客',noMoreData:false
        }],
    },
    onLoad(option) {
        this.setData({jobTitle: option.job_title, jobUuid: option.job_uuid})

    },
    onShow(){
        this.getInfo()
    },
    onPullDownRefresh() {
        wx.stopPullDownRefresh();
    },
    //  获取页面信息
    getInfo() {
        this.getQueryApplyJobList(0)
        this.getQueryApplyJobList(1)
        this.getQueryApplyJobList(2)
    },
    //  拨打电话
    contactPhone(e) {
        if(this.data.selectApply.account_telphone){
            let value = this.data.selectApply.account_telphone;
            wx.makePhoneCall({
                phoneNumber: value,
                success: function () {
                    console.log("拨打电话成功！")
                },
                fail: function () {
                    console.log("拨打电话失败！")
                }
            })
        }else {
            getTalentContactApi({resume_id: this.data.selectApply.resume_id}).then(res => {
                let value = res.data.content.telephone
                wx.makePhoneCall({
                    phoneNumber: value,
                    success: function () {
                        console.log("拨打电话成功！")
                    },
                    fail: function () {
                        console.log("拨打电话失败！")
                    }
                })
            })
        }

    },
    //  跳转职位详情
    gotoJobDetailPage() {
        wx.navigateTo({
            url: '/pages/jobDetail/index?job_uuid=' + this.data.jobUuid
        })
    },
    //  获取职位申请人列表
    getQueryApplyJobList(index) {
        let param = {
            list_type: index + 1,
            job_uuid: this.data.jobUuid,
            query_param: JSON.stringify({page_size: 10, page_num: 1, timestamp: Date.parse(new Date())})
        }
        getQueryApplyJobListApi(param).then(res => {
            this.data.tabList[index].noMoreData = false
            this.data.tabList[index].data = res.data.content.apply_job_resume_list;
            this.setData({tabList: this.data.tabList})
            if(res.data.content.apply_job_resume_list && res.data.content.apply_job_resume_list.length<10){
                this.data.tabList[index].noMoreData = true
            }
            this.setData({tabList:this.data.tabList})
        })

    },
    //  跳转申请者简历页面
    gotoResumePage(e) {
        console.log(e.currentTarget.dataset)
        let id = e.currentTarget.dataset.id
        let applyJobId = e.currentTarget.dataset.applyjobid
        wx.navigateTo({
          url: '/pages/resumeInfo/index?resume_id=' + id + '&apply_job_id=' + applyJobId
        })
    },
    //  雇佣
    employ(e) {
        console.log(e)
        this.setData({applyJobId:e.detail})
        let param = {
            apply_job_id_list: this.data.applyJobId,
            employ_status: 1,
            employ_memo: ''
        }
        refuseOrEmployApplicantApi(param).then(res => {
            Toast('操作成功');
            this.getInfo()
        })
    },
    //  打开拒绝理由弹窗
    openRefuseModal(e) {
        this.setData({applyJobId: e.detail, refuseReasonShow: true})
    },
    //  关闭拒绝理由弹窗
    closeRefuseModal() {
        this.setData({refuseReasonShow: false})

    },
    //  拒绝
    refuse(e) {
        let param = {
            apply_job_id_list: this.data.applyJobId,
            employ_status: 2,
            employ_memo: e.currentTarget.dataset.desc
        }
        refuseOrEmployApplicantApi(param).then(res => {
            this.closeRefuseModal()
            Toast('操作成功');
            this.getInfo()

        })
    },
    // 打开联系弹窗
    showPopup(e) {
        console.log(e)
        let data = e.detail
        this.setData({ selectApply: data});
        this.contactPhone()
    },
    // 跳转至在线聊天页面
    chatOnline() {
        let params = {accountId: this.data.selectApply.account_id}
        getInfoByAccountIdApi(params).then(res => {
          let url = '/pages/conversation/chat?type=1&targetId=' + res.data.content.friend.uuid + '&title=' + res.data.content.friend.nickname + '&accountId=' + this.data.selectApply.account_id
            wx.navigateTo({
                url: url,
            })
            this.onClose()
        })
    },
    // 关闭联系弹窗
    onClose() {
        this.setData({show: false});
    },
})

