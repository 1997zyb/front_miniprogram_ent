import {
    getUserInfoApi,
    getCityList,
    getPublishedJobListApi,
    pauseJobApi,
    closeJobApi,
    queryJobNumApi,
    getClaimUserTelphoneApi,
    getIndexInfoApi,
    getEmployerAuthorityApi,
    vipOpreteApi,
    getEnumApi,
    updateAccurateJobApi,
    cancelAccurateJobApi,
    getVipUseInfoApi,
    queryAccurateResumeNumApi

} from '../../api/index'
import {
    $getMapInfo
} from '../../utils/mapInfo'
import {
    $ifLogin
} from '../../utils/login'
import {
    getCityId,
    parseTime,
    checkAndAddCityInfo
} from '../../utils/util'
import Toast from '../../components/van-weapp/toast/toast';

let app = getApp()
let modalLogin = {}
let modalActivity = {}
let modalAccurate = {}
let modalAccurateResume = {}
let isFirstTime = true
const pageSize = 10
Page({
    data: {
        isLogin: true,
        published: true,
        showLogin: false,
        navHeight: app.globalData.navHeight,
        tabList: [{
                data: [],
                title: '待审核',
                index: 0,
                nodataTitle: '没有正在审核的职位',
                nodata: false
            },
            {
                data: [],
                title: '招聘中',
                index: 1,
                nodataTitle: '没有正在招聘中的职位',
                nodata: false
            },
            {
                data: [],
                title: '已结束',
                index: 2,
                nodataTitle: '没有已结束的职位',
                nodata: false
            }
        ],
        checkingList: [], // 审核中
        currentCity: {
            name: '定位中',
            id: 211,
        },
        recruitingList: [], // 招聘中
        endList: [], // 已结束
        currentTabIndex: 1,
        currentPage: [{
            page: 1,
            noMoreData: false
        }, {
            page: 1,
            noMoreData: false
        }, {
            page: 1,
            noMoreData: false
        }],
        popAd: [],
        navH: '',
        operaAuthorize: null,
        editPost: false,
        currentPost: null,
        canEdit: true,
        platform: app.globalData.platform,
        jobTypeList: ['全部岗位', '精品岗位', '普通岗位'],
        jobTypeIndex: 0,
    },
    onLoad(option) {
        if (option.currentTabIndex) {
            this.setData({
                currentTabIndex: option.currentTabIndex
            })
        }
        
        //  设置位置信息
        $getMapInfo().then(res => {
            res = res.address_component
            wx.getStorage({
                key: 'city_list',
                success: (list) => {
                    this.setLocationInfo(res, list.data)
                },
                fail: () => {
                    getCityList().then(list => {
                        let cityList = list.data.content.cities
                        wx.setStorageSync('city_list', cityList);
                        this.setLocationInfo(res, cityList)
                    })
                }
            });
        })
        //  判断登录
        $ifLogin()
        this.setData({
            navH: app.globalData.navHeight
        })

        // modalAccurate = this.selectComponent("#modal_accurate_buy_job");
        // modalAccurate.openModal()
    },
    onShow() {
        if (app.globalData.jobChange) {
            this.init()

            this.getInfo()
            // this.setData({
            //     onShow:true
            // })
            app.globalData.jobChange = false
            if (this.data.showLogin) {
                this.closeLogin()
            }
        }
        wx.getStorage({
            key: 'is_login',
            success: res => {
                if (!res.data) {
                    this.setData({
                        isLogin: false
                    })
                }
            },
            fail: err => {
                this.setData({
                    isLogin: false
                })
            }
        });
        wx.getStorage({
            key: 'current_city',
            success: res => {
                this.setData({
                    currentCity: res.data
                });

            }
        });

    },
    //  分享触发事件
    onShareAppMessage() {
        return {
            title: '城市服务行业一站式招聘平台',
            path: '/pages/index/index',
            imageUrl: 'https://wodan-idc.oss-cn-hangzhou.aliyuncs.com/shijianke-wechat/jiankezhaopin/images/shareMain.png',
            success: (res) => {
                wx.showToast({
                    title: '转发成功',
                })
            },
            fail: () => {
                wx.showToast({
                    title: '转发失败',
                })
            }
        }
    },
    //  获取页面信息
    getInfo() {
        // if (this.data.onShow)
        // return
        wx.getStorage({
            key: 'is_login',
            success: res => {
                if (res.data) {
                    this.setData({
                        isLogin: true
                    })
                    this.isPublished()
                } else {
                    this.setData({
                        isLogin: false
                    })
                }
            },
            fail: res => {
                this.setData({
                    isLogin: false
                })
            }
        });
        getIndexInfoApi({
            city_id: this.data.currentCity.id ? this.data.currentCity.id : 211
        }).then(res => {
            this.setData({
                popAd: res.data.content.ent_pop_up_ad_list.length > 0 ? res.data.content.ent_pop_up_ad_list[0] : ''
            })
            
            if (isFirstTime && this.data.popAd) {
                modalActivity = this.selectComponent("#modal_activity");
                modalActivity.openModal()

                isFirstTime = false
            }
        }).catch(e => {})
        this.getTabInfo()

        getEmployerAuthorityApi().then(res => {
            this.setData({
                operaAuthorize: res.data.content
            })
            app.globalData.operaAuthorize = res.data.content
        })
    },
    //  联系客服
    contact() {
        getClaimUserTelphoneApi().then(res => {
            let value = res.data.content.contractPhone;
            let contentText = this.data.platform == 'ios' ? '请联系招聘顾问' : '请开通VIP会员进行充值'
            let btnText = this.data.platform == 'ios' ? '立即联系' : '立即充值'
            let vip = this.selectComponent("#vip");
            vip.openModal('您当前可发职位不足\n' + contentText, btnText, this.data.platform);
            // wx.showModal({
            //     title: '',
            //     content: '您当前可发职位不足\n' +contentText,
            //     confirmText: btnText,
            //     cancelColor: '#b1b1b1',
            //     confirmColor: '#00BCD4',
            //     success: (res) => {
            //         if (res.confirm) {
            //             if(this.data.platform=='ios'){
            //                 wx.makePhoneCall({
            //                     phoneNumber: value,
            //                     success: function () {
            //                         console.log("拨打电话成功！")
            //                     },
            //                     fail: function () {
            //                         console.log("拨打电话失败！")
            //                     }
            //                 })
            //             }
            //             else {
            //                 wx.navigateTo({
            //                     url: '/pages/buyVip/index'
            //                 })
            //             }
            //         } else {
            //         }
            //     }
            // })
        })
    },
    //  设置位置信息
    setLocationInfo(location, cityList) {
        let cityInfo = {}
        if (location && getCityId(location.city, cityList)) {
            cityInfo = {
                name: location.city.replace('市', ''),
                id: getCityId(location.city, cityList)
            }
            app.globalData.serviceCity = cityInfo
        } else {
            cityInfo = {
                name: '选择城市',
                id: ''
            }
            app.globalData.serviceCity = {
                name: '请选择城市',
                id: '-1'
            }
        }
        wx.setStorageSync('locate_city', cityInfo);
        wx.setStorageSync('current_city', cityInfo);
        this.setData({
            currentCity: cityInfo
        });
        checkAndAddCityInfo()
    },
    //  tab切换事件
    onTabChange(e) {
        this.setData({
            currentTabIndex: e.detail.index
        })
        this.init()
        this.getTabInfo()

    },
    //  页面初始化
    init() {
        this.setData({
            editPost: false,
            currentPost: null,
            canEdit: true,
        })
        this.data.currentPage = [{
            page: 1,
            noMoreData: false
        }, {
            page: 1,
            noMoreData: false
        }, {
            page: 1,
            noMoreData: false
        }]
        this.data.tabList = [{
                    data: [],
                    title: '待审核',
                    index: 0,
                    nodataTitle: '没有正在审核的职位',
                    nodata: false
                },
                {
                    data: [],
                    title: '招聘中',
                    index: 1,
                    nodataTitle: '没有正在招聘中的职位',
                    nodata: false
                },
                {
                    data: [],
                    title: '已结束',
                    index: 2,
                    nodataTitle: '没有已结束的职位',
                    nodata: false
                }
            ],
            this.setData({
                currentPage: this.data.currentPage,
                tabList: this.data.tabList
            })
    },
    //  跳转发布职位
    gotoPublishPage() {
        if (!app.globalData.userInfo.complete) {
            wx.showModal({
                title: '提示',
                content: '请先完善雇主信息',
                confirmText: '去完善',
                success(res) {
                    if (res.confirm) {
                        wx.navigateTo({
                            url: '/pages/personalInformation/index'
                        })
                        return
                    } else if (res.cancel) {
                        wx.navigateTo({
                            url: '/pages/personalInformation/index'
                        })
                        return
                    }
                }
            })
        } else {
            wx.navigateTo({
                url: '/pages/publishJobType/index'
            })
            // let param = {city_id: this.data.currentCity.id}
            // app.globalData.publishInfo = {}
            // app.globalData.postInfo = {}
            // queryJobNumApi(param).then(res => {
            //     if (res.data.errCode == 0) {
            //         if (res.data.content.all_recruit_job_num - res.data.content.userd_recruit_job_num > 0) {
            //             wx.navigateTo({
            //                 url: '/pages/publishJobType/index'
            //             })
            //         } else {
            //             this.contact()
            //         }
            //     } else if (res.data.errCode == 1){
            //         wx.showToast({
            //             title: '请选择发布城市',
            //             icon: 'none'
            //         })
            //     }  else {
            //         wx.showToast({
            //             title: res.data.errMsg,
            //             icon: 'none'
            //         })
            //     }
            // })
        }
    },
    //  跳转编辑职位
    gotoEditPostPage(e) {
        console.log(this.data.currentPost)
        this.closeEdit()
        app.globalData.publishInfo = this.data.currentPost // 要发布的职位
        app.globalData.postInfo = this.data.currentPost // 拿到的职位原始数据
        if (e.currentTarget.dataset.type == '1') {
            app.globalData.publishInfo.isRepublic = true
        } else {
            app.globalData.publishInfo.isRepublic = false
        }
        app.globalData.publishInfo.jobRankType = app.globalData.postInfo.job_classify_type
        console.log(app.globalData.postInfo)
        if (this.data.canEdit) {
            if (app.globalData.postInfo.status == 1) {
                wx.navigateTo({
                    url: '/pages/publishJobType/index?postId=' + this.data.currentPost.job_uuid + '&type=' + (app.globalData.postInfo == 1 ? 0 : 1)
                })
            } else {
                wx.navigateTo({
                    url: '/pages/publishJob/index?postId=' + this.data.currentPost.job_uuid + '&type=' + (app.globalData.postInfo == 1 ? 0 : 1)
                })
            }
        } else {
            wx.navigateTo({
                url: '/pages/publishJob/index?postId=' + this.data.currentPost.job_uuid + '&type=' + (app.globalData.postInfo == 1 ? 0 : 1)
            })
        }

    },
    //  跳转编辑职位(重新发布)
    //  未审核跳转职位详情
    gotoJobDetailPageFull(e) {
        if (this.data.currentTabIndex == 0) {
            this.gotoJobDetailPage(e)
        }
    },
    //  跳转职位详情页面
    gotoJobDetailPage(e) {
        wx.navigateTo({
            url: '/pages/jobDetail/index?job_uuid=' + e.currentTarget.dataset.id
        })
    },
    //  打开职位详细
    jobClick(e) {
        let index = e.currentTarget.dataset.index
        if (index === 0) {} else {
            wx.navigateTo({
                url: '/pages/jobManage/index?job_uuid=' + e.currentTarget.dataset.id + "&job_title=" + e.currentTarget.dataset.title
            })
        }
    },
    //  暂停职位
    pause() {
        this.closeEdit()
        let param = {
            job_id: this.data.currentPost.job_id,
            operate: this.data.currentPost.display_status == 1 ? 0 : 1
        }
        pauseJobApi(param).then(res => {
            this.init()
            this.getTabInfo()
        })
    },
    //  关闭职位
    close(e) {
        this.closeEdit()
        wx.showModal({
            title: '',
            content: '确定关闭该职位吗？',
            confirmText: '再考虑下',
            cancelText: '确定关闭',
            cancelColor: '#b1b1b1',
            confirmColor: '#00BCD4',
            success: (res) => {
                if (!res.confirm) {
                    let param = {
                        job_id: this.data.currentPost.job_id
                    }
                    closeJobApi(param).then(res => {
                        this.init()
                        this.getTabInfo()
                    })
                }
            }
        })

    },
    //  获取职位数据
    getTabInfo() {
        let queryType
        if (this.data.currentTabIndex == 0) {
            queryType = 9
        } else if (this.data.currentTabIndex == 1) {
            queryType = 4
        } else if (this.data.currentTabIndex == 2) {
            queryType = 2
        }
        let index = this.data.jobTypeIndex == 1 ? 2 : (this.data.jobTypeIndex == 2 ? 1 : 0)
        let param = {
            query_type: queryType,
            page_size: pageSize,
            page_num: this.data.currentPage[this.data.currentTabIndex].page,
            timestamp: Date.parse(new Date()),
            accurate_job_query_type: this.data.currentTabIndex == 1 ? index : 0
        }
        if (!this.data.currentPage[this.data.currentTabIndex].noMoreData) {
            getPublishedJobListApi(param).then(res => {
                this.data.currentPage[this.data.currentTabIndex].page++
                    if (res.data.content) {
                        let list = res.data.content.job_list
                        if (list.length < pageSize) {
                            this.data.currentPage[this.data.currentTabIndex].noMoreData = true
                        }

                        list.forEach(item => {
                            if (item.working_time_start_date) {
                                item.working_time_start_date = parseTime(item.working_time_start_date, '{y}-{m}-{d}')
                            }
                            if (item.working_time_end_date) {
                                item.working_time_end_date = parseTime(item.working_time_end_date, '{y}-{m}-{d}')
                            }
                        })
                        this.data.tabList[this.data.currentTabIndex].data = this.data.tabList[this.data.currentTabIndex].data.concat(list)
                        this.data.tabList[this.data.currentTabIndex].nodata = (this.data.tabList[this.data.currentTabIndex].data.length == 0)
                        this.setData({
                            tabList: this.data.tabList,
                            currentPage: this.data.currentPage,
                            normalJobCount: res.data.content.normal_self_job_list_count,
                            accurateJobCount: res.data.content.accurate_self_job_list_count,
                            totalJobCount: res.data.content.total_self_job_list_count
                        })
                    }
                // this.setData({
                //     onShow:false
                // })
            }).catch(err => {
                console.log(err) // 这里catch到错误timeout
            })
        }


    },
    //  是否发布过职位
    isPublished() {
        let param = {
            query_type: 6,
            page_size: pageSize,
            page_num: 1,
            timestamp: Date.parse(new Date())
        }
        getPublishedJobListApi(param).then(res => {
            if (res.data.content.job_list.length > 0) {
                this.setData({
                    published: true
                })
            } else {
                this.setData({
                    published: false
                })
            }
        })
    },
    //  打开登录弹窗
    openLogin() {
        this.setData({
            showLogin: true
        })
        modalLogin = this.selectComponent("#modal_login");
        modalLogin.openModal();
    },
    //  关闭登录弹窗
    closeLogin() {
        modalLogin = this.selectComponent("#modal_login");
        modalLogin.closeModal();
        this.setData({
            showLogin: false
        })
    },
    //  点击事件开始
    handleTouchStart(e) {
        this.setData({
            clickStartTime: e.timeStamp
        })
    },
    //  记录点击结束时间
    handleTouchEnd(e) {
        this.setData({
            clickEndTime: e.timeStamp
        })
    },
    //  长按事件
    longPress() {
        if (this.data.clickEndTime - this.data.clickStartTime > 5000) {
            wx.showToast({
                title: 'v3.4.1',
                success: () => {
                    wx.navigateTo({
                        url: '/pages/pageList/index'
                    })
                }
            })
        }
    },
    //  打开职位编辑弹窗
    openEdit(e) {
        let post = e.currentTarget.dataset.post
        this.setData({
            editPost: true,
            currentPost: e.currentTarget.dataset.post,
            operaAuthorize:{canPause:post.is_suspend_power,canClose:post.is_close_power}
        })
        if (e.currentTarget.dataset.post.job_classify_type == 1) {
            this.setData({
                canEdit: false
            })
        } else {
            this.setData({
                canEdit: true
            })
        }
    },
    //  关闭职位编辑弹窗
    closeEdit() {
        this.setData({
            editPost: false
        })
    },
    //  vip操作
    vipOprete(e) {
        let num = e.currentTarget.dataset.type == 3 ? 100 : 1
        let param = {
            job_id: e.currentTarget.dataset.item.job_id,
            vip_spread_type: e.currentTarget.dataset.type,
            vip_spread_num: num
        }
        vipOpreteApi(param).then(res => {
            if (res.data.errCode == 0) {
                Toast.success('操作成功');
            } else if (res.data.errCode == 1) {
                let textTip = e.currentTarget.dataset.type == 1 ? '置顶天数' : e.currentTarget.dataset.type == 2 ? '刷新次数' : '推送人数'
                let contentText = this.data.platform == 'ios' ? '请联系招聘顾问' : '请开通VIP会员进行充值'
                let btnText = this.data.platform == 'ios' ? '立即联系' : '立即充值'
                let vip = this.selectComponent("#vip");
                vip.openModal('您的职位' + textTip + '不足\n' + contentText, btnText, this.data.platform);
                // wx.showModal({
                //     title: '',
                //     content: '您的职位' + textTip + '不足\n' +
                //         contentText,
                //     confirmText: btnText,
                //     cancelText: '再考虑下',
                //     cancelColor: '#b1b1b1',
                //     confirmColor: '#00BCD4',
                //     success: (res) => {
                //         if (res.confirm) {
                //             if(this.data.platform=='ios'){
                //                 getClaimUserTelphoneApi().then(res => {
                //                     let value = res.data.content.contractPhone
                //                     wx.makePhoneCall({
                //                         phoneNumber: value,
                //                         success: function () {
                //                             console.log("拨打电话成功！")
                //                         },
                //                         fail: function () {
                //                             console.log("拨打电话失败！")
                //                         }
                //                     })
                //                 })
                //             }else {
                //                 wx.navigateTo({
                //                     url: '/pages/buyVip/index'
                //                 })
                //             }

                //         } else {
                //         }
                //     }
                // })
            }

        }).catch(err => {
            console.log(err)
        })
    },
    onPullDownRefresh() {
        wx.stopPullDownRefresh();
    },
    // 选择岗位类型
    selectJob(e) {
        
        this.setData({
            jobTypeIndex: e.currentTarget.dataset.index,

        })
        this.init()
        this.getTabInfo()
    },

    // 修改精准简历
    editExcellentResume() {
        this.closeEdit()
        app.globalData.publishInfo = this.data.currentPost
        queryAccurateResumeNumApi({ city_id: wx.getStorageSync('current_city').id || ''}).then(res => {
            console.log(res)
            if (res.data.errCode==99) {
                modalAccurateResume = this.selectComponent("#modal_accurate_buy");
                modalAccurateResume.openModal()
            } else if (res.data.errCode == 100){
                let vip = this.selectComponent("#vip");
                vip.openModal('套餐内精准简历数不足，您可以联系招聘顾问升级套餐', '立即联系','ios');
            }else {
                wx.setStorageSync('selectedJobList', this.data.currentPost.job_classify_id_list == 0 ? this.data.currentPost.job_type_id.toString() : this.data.currentPost.job_classify_id_list)
                wx.setStorageSync('sendJobId', this.data.currentPost.job_id)
                wx.navigateTo({
                    url: '/pages/jobExperience/index?type=setJobOne',
                })
            }
        })
    },

    // 取消精品展示
    closeDispaly() {
        this.closeEdit()
        let param = {
            job_id: this.data.currentPost.job_id
        }
        cancelAccurateJobApi(param).then(res => {
            if (res.data.errCode == 102) {
                let vip = this.selectComponent("#vip");
                vip.openModal('您的普通岗位数不足，购买更多普通岗位即可立即发布', '立即购买');
            } else {
                var that = this
                wx.showToast({
                    title: '取消精品展示成功',
                    icon: 'success',
                    success() {
                        that.init()
                        that.getTabInfo()
                    }
                })
            }
        })
    },

    // 设置精品展示
    setDispaly() {
        this.closeEdit()
        let param = {
            job_id: this.data.currentPost.job_id
        }
        updateAccurateJobApi(param).then(res => {
            if (res.data.errCode == 99) {
                modalAccurate = this.selectComponent("#modal_accurate_buy_job");
                modalAccurate.openModal()
            } else if (res.data.errCode == 103) {
                let vip = this.selectComponent("#vip");
                vip.openModal('套餐内精品岗位数不足，你可以联系招聘顾问升级套餐', '立即联系', 'ios');
            } else {
                var that = this
                wx.showToast({
                    title: '设置精品岗位展示成功',
                    icon: 'success',
                    success() {
                        that.init()
                        that.getTabInfo()
                    }
                })
            }
        })
    }
})