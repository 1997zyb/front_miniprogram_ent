let app = getApp()
import {getQueryString,urlDelParam,putParameterToUrl} from '../../utils/util'
Page({
    data: {
        url: '',
        isLoad: false,
        options:null
    },
    onLoad: function (option) {

        this.options = option
        this.data.url = option.url
        let url = decodeURIComponent(option.url)

        if(option.title){
            wx.setNavigationBarTitle({
                title: option.title
            });
        }
        //  在路径上加sessionId用于自动登录
        wx.getStorage({
            key: 'sessionid',
            success:(res)=> {
                url = urlDelParam(url,'app_user_token')
                url = putParameterToUrl({app_user_token:res.data},url)
                if (url.indexOf('?')!='-1'){
                    url = url + '&sessionId=' + res.data
                }
                else {
                    url = url + '?sessionId=' + res.data
                }
            },
            complete:()=>{
                console.log(url)
                this.setData({
                    url: url,
                    isLoad: true
                })
            }
        })

    },
    onPullDownRefresh: function () {
    },
    onShareAppMessage() {
        let url = decodeURIComponent(this.options.url)
        let params = url.split('?')[1]
        let shareTitle =  getQueryString(params,'shareTitle')
        let shareImg =  getQueryString(params,'shareImg')
        url = urlDelParam(url,'inviteCode')
        url = urlDelParam(url,'sessionId')
        url = urlDelParam(url,'app_user_token')
        url = putParameterToUrl({inviteCode:app.globalData.userInfo.accountId},url)
        let shareUrl = '/pages/webView/index?url='+encodeURIComponent(url)
        let shareParam  = {path:shareUrl}
        shareTitle?shareParam.title=shareTitle:''
        shareImg?shareImg.title=shareImg:''
        if(shareTitle && shareImg){
            return {
                title: shareTitle,
                path: shareUrl,
                imageUrl:shareImg,
            }
        }
        return shareParam
    },

})

// https://m.jianke.cc/m/inviteEmp/index?shareTitle=就差你了，快来帮我点一下！&shareImg=https://wodan-idc.oss-cn-hangzhou.aliyuncs.com/shijianke-wechat/youpin/images/h5_share_employerInvite.png
