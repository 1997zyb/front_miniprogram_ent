// pages/vipInfo/index.js
import {
    getVipUseInfoApi

} from '../../api/index'
var modalContact = {}
var modalLogin={}
Page({

    /**
     * 页面的初始数据
     */
    data: {
        title: 'VIP会员',
        list: ['普通岗位数', '精品岗位数', '精准简历数', '刷新次数', '置顶(天)', '推送(人)', '简历数(份)'],
        contact: false,
        companyName: '',
        flag:true,
        intoIndex:0,
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        this.getVipInfo()
        console.log(wx.getStorageSync('user_globalData').bean)
        this.setData({
            companyName: wx.getStorageSync('user_globalData').bean.enterpriseName || wx.getStorageSync('user_globalData').trueName

        })
    },
    //   获取vip内容
    getVipInfo() {
        getVipUseInfoApi().then(res => {
            console.log(res)
            this.setData({
                vipInfo: res.data.content.city_vip_info,
                vipInfoContent:res.data.content
            })
        })
    },

    // 前往使用明细页面
    goToRecord() {
        wx.navigateTo({
            url: '/pages/vipRecord/index',
        })
    },

    //立即升级
    upVip() {
        //   this.setData({
        //       contact: !this.data.contact,
        //   })
        modalContact = this.selectComponent("#modal_contact");
        modalContact.openModal()
    },

    //   前往Vip使用规则
    goToVipUser() {
        wx.navigateTo({
            url: '/pages/vipUserRule/index?type=' + this.data.vipInfo[this.data.intoIndex].vip_city_name,
        })
    },
    // 滚动事件
    bindscroll(e){
        console.log(e.detail.deltaX)
        if (!this.data.flag)
        return
        if (e.detail.deltaX < 0 && (this.data.intoId != 'card2')){
            this.setData({
                intoIndex: this.data.intoIndex + 1,
                intoId: 'card' + (this.data.intoIndex + 1),
                flag:false
            })
            setTimeout(() => {
                this.setData({
                    flag: true
                })
            }, 1000)
        } else if (e.detail.deltaX > 0 && this.data.intoId != 'card0'){
            this.setData({
                intoIndex:this.data.intoIndex-1,
                intoId: 'card' + (this.data.intoIndex - 1),
                flag: false
            })
            setTimeout(() => {
                this.setData({
                    flag: true
                })
            }, 500)
        }
    },
    // 滑动时间
    swiperScroll(e){
        this.setData({
            intoIndex:e.detail.current
        })
    },
    //  打开登录弹窗
    openLogin() {
        this.setData({ showLogin: true })
        modalLogin = this.selectComponent("#modal_login");
        modalLogin.openModal();
    },
    //  关闭登录弹窗
    closeLogin() {
        modalLogin = this.selectComponent("#modal_login");
        modalLogin.closeModal();
        this.setData({ showLogin: false })
    },
})