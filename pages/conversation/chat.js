import { Service, reconnect} from '../../utils/im'
const utils = require('../../utils/utils.js');
const { adapterHeight } = utils.getAdapterheight();
const {Message,File} = Service
const { globalData } = getApp();
// const { Service: { Status, Message, File } } = globalData;
const RongEmoji = require('../../lib/RongIMEmoji-2.2.6.js');
const { getJobDetailApi, getInfoByAccountIdApi, initMessageBody} = require('../../api/index.js');
RongEmoji.init();

const softKeyboardHeight = 210;


const sendJobDetails = (context) => {
  console.log(context)
  let { type, targetId, messageList, jobDetails } = context.data;
  let fromUser = context.data.fromUser
  let sendContent = {
    message: '职位卡片',
    headUrl: fromUser.avatar,
    name: fromUser.name,
    jobDetails: JSON.stringify(jobDetails)
  }
  setExtra('职位卡片', context, sendContent, 20006, (extra) => {
    Message.sendText({
      type,
      targetId,
      content: fromUser.name + ':【职位卡片】',
      extra
    }).then(message => {
      message = setMessageUser(message)
      messageList.push(message);
      context.setData({
        messageList,
        toView: message.uId
      });
    });
  })
  // Message.sendJobDetails({
  //   type,
  //   targetId,
  //   jobDetails: JSON.stringify(jobDetails)
  // }).then(message => {
  //   messageList.push(message);
  //   context.setData({
  //     messageList,
  //     toView: message.uId
  //   });
  // });
};

const getToView = (context) => {
  let { messageList } = context.data;
  let index = messageList.length - 1;
  let message = messageList[index] || {};
  return message.uId || '';
};

const setKeyboardPos = (context, keyboardHeight, adapterHeight) => {
  keyboardHeight = keyboardHeight || 0;
  let data;
  let isScroll = (keyboardHeight > 0);
  if (isScroll) {
    data = {
      bottom: adapterHeight + keyboardHeight,
      isShowEmojiSent: false,
      toView: getToView(context)
    };
  } else {
    data = {
      bottom: adapterHeight + keyboardHeight,
      isShowEmojiSent: false
    };
  }
  context.setData(data);
};

const showSoftKeyboard = (context, display) => {
  context.setData({
    display: display,
    bottom: softKeyboardHeight,
    isShowKeyboard: false,
    toView: getToView(context)
  });
};
const hideSoftKeyboard = (context) => {
  context.setData({
    display: {
      emoji: 'none',
      more: 'none'
    }
  });
};

const hideKeyboard = (context) => {
  let keyboardHeight = 0;
  let { adapterHeight } = context.data;
  setKeyboardPos(context, keyboardHeight, adapterHeight);
  hideSoftKeyboard(context);
};

const formatEmojis = () => {
  let list = RongEmoji.list;
  return utils.sliceArray(list, { size: 24 });
};


// 设置消息json的字符串
const setMessageUser = (message) => {
  if (message.content.extra && message.content.extra.length > 69) {
    // console.log(message)
    let content = JSON.parse(message.content.extra.substr(69))
    content.content = JSON.parse(content.content)
    if (content.bizType == 20001 || content.bizType == 20007)
      return setMessageText(message, content.content)
    else if (content.bizType == 20002)
      return setMessageImage(message, content.content)
    else if (content.bizType == 20006)
      return setMessageJobDetails(message, content.content)
  }
}

// 设置职位卡片内容
const setMessageJobDetails = (message, content) => {
  console.log(content)
  message.content.content = content.message
  message.content = {
    messageName: 'RichContentMessage',
    content: content.jobDetails,
    user: {
      neme: content.name,
      avatar: content.headUrl,
    }
  }
  message.objectName = 'RC:ImgTextMsg',
    message.messageType = 'RichContentMessage',
    message.name = 'RichContentMessage',
    message.sender = {
      neme: content.name,
      avatar: content.headUrl,
    }
  return message
}

//设置图片内容
const setMessageImage = (message, content) => {
  message.content.content = content.message
  message.content = {
    messageName: 'ImageMessage',
    extra: {
      width: content.width || 50,
      height: content.height || 50,
    },
    imageUri: content.url,
    user: {
      neme: content.name,
      avatar: content.headUrl,
    }
  }
  message.objectName = 'RC:ImgMsg',
    message.messageType = 'ImageMessage',
    message.name = 'ImageMessage',
    message.sender = {
      neme: content.name,
      avatar: content.headUrl,
    }
  return message
}

// 设置文本内容
const setMessageText = (message, content) => {
  message.content.content = content.message
  message.content.user = {
    neme: content.name,
    avatar: content.headUrl,
  }
  message.sender = {
    neme: content.name,
    avatar: content.headUrl,
  }
  return message
}

const getMessageList = (context, params, callback) => {
  let { position } = params;
  return Message.getList(params).then((result) => {
    let messages = result.messageList;
    let hasMore = result.hasMore;
    let { messageList, playingVoice, playingMusicComponent } = context.data;
    messageList = messages.concat(messageList);
    let toView = '';
    if (params.position == 0) {
      let index = messageList.length - 1;
      let message = messageList[index] || {};
      toView = message.uId || '';
    }
    let isFirst = (position == 0);
    if (!hasMore && !isFirst) {
      // 灰条提示
      toView = 'message-notify-without';
      context.setData({
        hasMore: hasMore
      });
    }

    messageList.forEach(message => {
      message = setMessageUser(message)
    })

    if (isFirst) {
      context.setData({
        messageList: messageList,
        isAllowScroll: true,
        toView: toView
      });
    } else {
      context.setData({
        messageList: messageList,
        isAllowScroll: true
      });
    }

    callback()
  });
};

const updatePlayStatus = (context, { newMusicComponent, isPlaying }, callback) => {
  let { data: { messageList, playingMusicComponent } } = context;
  callback = callback || utils.noop;
  messageList.map((message) => {
    callback(message);
    return message;
  });
  if (playingMusicComponent) {
    playingMusicComponent.setData({
      isPlaying
    });
  }
  if (newMusicComponent) {
    context.setData({
      playingMusicComponent: newMusicComponent,
      messageList
    });
  } else {
    context.setData({
      messageList
    });
  }

};

const stopPlayMusic = (context) => {
  let newMusicComponent = null, isPlaying = false;
  updatePlayStatus(context, { newMusicComponent, isPlaying }, (message) => {
    utils.extend(message, { isPlaying });
  });
};

const getImageUrls = (context) => {
  let { messageList } = context.data;
  return messageList.filter(message => {
    return message.name == 'ImageMessage';
  }).map(message => {
    return message.content.imageUri;
  });
};

const onLoad = (context, query) => {
  let { title, type, targetId, selectApply } = query;
  wx.setNavigationBarTitle({
    title
  });
  context.setData({
    adapterHeight: adapterHeight,
    type,
    targetId,
    title,
  });
  let keyboardHeight = 0;
  setKeyboardPos(context, keyboardHeight, adapterHeight);

  let position = 0;
  let count = 15;
  getMessageList(context, { type, targetId, position, count }, () => {
    if (selectApply) {
      let jobDetails = JSON.parse(selectApply)
      let param = {
        job_uuid: jobDetails.job_uuid,
        job_id: jobDetails.job_id,
      }
      getJobDetailApi(param).then(res => {
        context.setData({
          jobDetails: res.data.content.parttime_job,
          content: '您好，我对这个职位很感兴趣，可以聊一聊吗？'
        })
        sendJobDetails(context)
        sendText(context)
      })
    }
  });
  Message.watch((message) => {
    if (message.senderUserId != context.data.targetId && message.conversationType == 1)
      return
    let { messageList } = context.data;
    message = setMessageUser(message)
    messageList.push(message);
    context.setData({
      messageList,
      toView: message.uId
    });
  });

};


const onUnload = (context) => {
  let { playingVoice, playingMusicComponent } = context.data;
  if (playingVoice) {
    playingMusicComponent.stop();
  }
  if (playingMusicComponent) {
    playingMusicComponent.stop();
  }
};

const showVoice = (context) => {
  let { adapterHeight } = context.data;
  context.setData({
    isShowKeyboard: false
  });
  hideKeyboard(context);
};

const showKeyboard = (context) => {
  context.setData({
    isShowKeyboard: true
  });
  hideKeyboard(context);
};

const recorderManager = wx.getRecorderManager()

const startRecording = (context) => {
  context.setData({
    isRecording: true
  });
  let record = () => {
    recorderManager.start({
      format: 'mp3'
    });
  };
  wx.getSetting({
    success(res) {
      if (!res.authSetting['scope.record']) {
        wx.authorize({
          scope: 'scope.record',
          success: record
        })
      } else {
        record();
      }
    }
  })
};

const stopRecording = (context) => {
  context.setData({
    isRecording: false
  });
  recorderManager.onStop((res) => {
    console.log('recorder stop', res)
    const { tempFilePath, duration } = res
    File.upload({
      path: tempFilePath
    }).then(file => {
      console.log(file)
      let content = {
        content: file.downloadUrl,
        duration: Math.ceil(duration / 1000)
      };
      let { type, targetId, messageList } = context.data;
      Message.sendVoice({
        type,
        targetId,
        content
      }).then(message => {
        messageList.push(message);
        context.setData({
          messageList,
          toView: message.uId
        });
      });
    });
  })
  recorderManager.stop();
};

const showEmojis = (context) => {
  showSoftKeyboard(context, {
    emoji: 'block',
    more: 'none'
  });
};

const showMore = (context) => {
  showSoftKeyboard(context, {
    emoji: 'none',
    more: 'block'
  });
};

const selectEmoji = (context, event) => {
  var content = context.data.content;
  var { emoji } = event.target.dataset;
  content = content + emoji;
  context.setData({
    content: content,
    isShowEmojiSent: true
  });
};

// 初始化信息
const setExtra = (res, context, sendContent, bizType, callback) => {
  let fromUser = context.data.fromUser
  let toUser = context.data.toUser

  let param = {
    fromUser: fromUser.accountId,
    fromType: 2,
    fromUuid: fromUser.id,
    headUrl: fromUser.avatar,
    toUser: toUser.accountId,
    toType: 1,
    toUuid: toUser.id,
    notifyContent: fromUser.name + ':' + res,
    content: JSON.stringify(sendContent),
    bizType: bizType,
    msgType: 2,
  }
  initMessageBody(param).then(res => {
    callback(res.data.content.msgBody)
  })
}

// 发送文本消息
const sendText = (context) => {
  let { content, type, targetId, messageList } = context.data;
  context.setData({
    content: '',
    isShowEmojiSent: false
  });
  if (content.length == 0) {
    return;
  }
  let fromUser = context.data.fromUser
  let sendContent = {
    message: content,
    headUrl: fromUser.avatar,
    name: fromUser.name,
  }
  setExtra(content, context, sendContent, 20001, (extra) => {

    console.log(extra)
    Message.sendText({
      type,
      targetId,
      content,
      extra
    }).then(message => {
      messageList.push(message);
      context.setData({
        messageList,
        toView: message.uId
      });
    });
  })
};

const getMoreMessages = (context) => {
  let { type, targetId, hasMore } = context.data;
  let position = null;
  let count = 5;
  if (hasMore) {
    context.setData({
      isAllowScroll: false
    });
    getMessageList(context, { type, targetId, position, count }, () => { });
  }
};

const sendImage = (context, type) => {
  wx.chooseImage({
    count: 1,
    sizeType: ['compressed'],
    sourceType: [type],
    success: (res) => {
      let { tempFilePaths } = res;
      let tempFilePath = tempFilePaths[0];
      wx.getImageInfo({
        src: tempFilePath,
        success: (res) => {
          let extra = utils.compress(res);
          let { type, targetId, messageList } = context.data;
          let name = 'ImageMessage';
          let content = {
            imageUri: tempFilePath,
            extra
          };
          let message = Message.create({
            type,
            targetId,
            name,
            content
          });
          File.upload({
            path: tempFilePath
          }).then(result => {
            let fromUser = context.data.fromUser
            let sendContent = {
              snedType: '1',
              message: '图片',
              photoName: (new Date()).valueOf().toString(),
              url: result.downloadUrl,
              headUrl: fromUser.avatar,
              name: fromUser.name,
              height: extra.height,
              width: extra.height,
            }
            setExtra('【图片】', context, sendContent, 20002, (extra) => {
              Message.sendText({
                type,
                targetId,
                content: fromUser.name + ':【图片】',
                extra
              }).then(message => {
                message = setMessageUser(message)
                messageList.push(message);
                context.setData({
                  messageList,
                  toView: message.uId
                });
              });
            })
          });
        }
      })
    }
  })
};

const sendMusic = (context) => {
  let { content, type, targetId, messageList } = context.data;
  Message.sendMusic({
    type,
    targetId
  }).then(message => {
    messageList.push(message);
    context.setData({
      messageList,
      toView: message.uId
    });
  });
};

const playVoice = (context, event) => {
  let voiceComponent = event.detail;
  let { playingVoice } = context.data;
  if (playingVoice) {
    let playingId = playingVoice.__wxExparserNodeId__;
    let voiceId = voiceComponent.__wxExparserNodeId__;
    // 两次播放为同个音频，状态保持不变
    if (playingId == voiceId) {
      return;
    }
    let { innerAudioContext } = playingVoice.data;
    playingVoice.setData({
      isPlaying: false
    });
    innerAudioContext.stop();
  }
  context.setData({
    playingVoice: voiceComponent
  });
};

const playMusic = (context, event) => {
  let newMusicComponent = event.detail;
  let { playingMusicComponent, messageList } = context.data;
  let { properties: { message: { messageUId: newPlayId } } } = newMusicComponent
  let playingId = '';

  // 连续点击播放不同音乐
  if (playingMusicComponent) {
    let { properties: { message } } = playingMusicComponent;
    playingId = message.messageUId;
    //先停止上一个，再播放
    let isDiffMusic = (playingId != newPlayId);
    if (isDiffMusic) {
      let { innerAudioContext } = playingMusicComponent.data;
      playingMusicComponent.setData({
        isPlaying: false
      });
      innerAudioContext.stop();
    }
  }
  let isPlaying = false;
  updatePlayStatus(context, { newMusicComponent, isPlaying }, (message) => {
    let { messageUId } = message;
    // 默认为未播放状态
    isPlaying = false;
    if (messageUId == newPlayId) {
      isPlaying = true;
    }
    utils.extend(message, { isPlaying });
  });
};

const previewImage = (context, event) => {
  let currentImageUrl = event.detail;
  let urls = getImageUrls(context);
  if (utils.isEmpty(urls)) {
    urls.push(currentImageUrl);
  }
  wx.previewImage({
    current: currentImageUrl,
    urls: urls
  })
};

const stopMusic = (context, event) => {
  let musicComponent = event.detail;
  let { properties: { message: { messageUId } } } = musicComponent;

  let { messageList, playingMusicComponent } = context.data;
  if (playingMusicComponent) {
    let { data: { innerAudioContext } } = playingMusicComponent;
    innerAudioContext.stop();
  }
  musicComponent.setData({
    isPlaying: false
  });
  stopPlayMusic(context);
};

// 设置交流对象信息
const setObjectInfo = (query, context, callback) => {
  let params = {
    accountId: query.accountId
  }
  getInfoByAccountIdApi(params).then(res => {
    let returnData = res.data.content.friend
    let toUser = {
      name: returnData.userName,
      type: 1,
      avatar: returnData.userPortrait,
      id: returnData.uuid,
      accountId: returnData.accountId
    }
    let userList = wx.getStorageSync('user_list')
    let fromUser = userList[userList.length - 1]
    context.setData({
      toUser,
      fromUser
    })
    callback()
  })
}

Page({
  data: {
    navHeight: globalData.navHeight,
    content: '',
    messageList: [],
    bottom: 0,
    adapterHeight: 0,
    display: {
      emoji: 'none',
      more: 'none'
    },
    emojis: formatEmojis(),
    isShowEmojiSent: false,
    isRecording: false,
    isShowKeyboard: false,
    hasMore: true,
    toView: '',
    playingVoice: null,
    playingMusicComponent: null,
    isAllowScroll: true,
    scrollTop: 0,
    // name:'默认名字',
  },
  hideKeyboard: function () {
    hideKeyboard(this);
  },
  selectEmoji: function (event) {
    selectEmoji(this, event);
  },
  sendText: function () {
    sendText(this);
  },
  sendJobDetails: function () {//点击图文按钮之后，找到此方法
    sendJobDetails(this);
  },
  getMoreMessages: function (event) {
    getMoreMessages(this);
  },
  sendImage: function (e) {
    sendImage(this, e.currentTarget.dataset.type);
  },
  // sendCamera:function(){
  //     sendCamera(this)
  // },
  sendMusic: function () {
    sendMusic(this);
  },
  showVoice: function () {
    showVoice(this);
  },
  showKeyboard: function () {
    showKeyboard(this);
  },
  startRecording: function () {
    startRecording(this);
  },
  stopRecording: function () {
    stopRecording(this);
  },
  showEmojis: function () {
    showEmojis(this);
  },
  showMore: function () {
    showMore(this);
  },
  // 以下是事件
  onShow:function(){
    reconnect({})
  },
  onLoad: function (query) {
    if (parseInt(query.accountId) >0) {
      setObjectInfo(query, this, () => {
        onLoad(this, query)
      })
    } else {
      this.setData({
        chatType: 3
      })
      onLoad(this, query)
    }

  },
  onUnload: function () {
    onUnload(this);
  },
  onInput: function (event) {
    this.setData({
      content: event.detail.value
    });
  },
  onFocus: function (event) {
    let { height } = event.detail;
    let adapterHeight = 0;
    setKeyboardPos(this, height, adapterHeight);
    hideSoftKeyboard(this);
  },
  onPlayVoice: function (event) {
    playVoice(this, event);
  },
  onPlayMusic: function (event) {
    playMusic(this, event);
  },
  onMusicStop: function (event) {
    stopMusic(this, event);
  },
  onPreviewImage: function (event) {
    previewImage(this, event);
  },
  onHide: function () {
    hideKeyboard(this);
    stopPlayMusic(this);
  },
  //  打开登录弹窗
  openLogin() {
    this.setData({ showLogin: true })
    modalLogin = this.selectComponent("#modal_login");
    modalLogin.openModal();
  },
  //  关闭登录弹窗
  closeLogin() {
    this.setData({ showLogin: false })
    modalLogin = this.selectComponent("#modal_login");
    modalLogin.closeModal();
  },
})
