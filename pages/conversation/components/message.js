// pages/conversation/components/message.js
Component({
  options: {
    multipleSlots: true
  },
  /**
   * 组件的属性列表
   */
  properties: {
    message: Object,
    chatType:String,
  },
  relations: {
    './message/image': {
      type: 'child'
    },
    './message/text': {
      type: 'child'
    },
    './message/voice': {
      type: 'child'
    },
    './message/jobDetails': {//新增图文
      type: 'child'
    }

  },
  /**
   * 组件的初始数据
   */
  data: {
  },
  lifetimes: {
    ready: function() {
      console.log(this.data.message)
      console.log(this.data.chatType)
      if (this.data.typeName == 'message' && this.data.message.content && this.data.message.content.content) {
        this.setData({
          text: this.data.message.content.content
        })
      }
      if (this.data.message.content.extra && this.data.message.content.extra.length > 69) {
        let content = JSON.parse(this.data.message.content.extra.substr(69))
        content.content = JSON.parse(content.content)
        this.data.type = content.content.notice_type
        console.log(content)
      }
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {
    onPlayVoice: function (event) {
      this.triggerEvent('onplay', event.detail)
    },
    onPlayMusic: function(event){
      this.triggerEvent('onplaymusic', event.detail)
    },
    onMusicStop: function (event){
      this.triggerEvent('onmusicstop', event.detail)
    },
    onPreviewImage: function(event){
      let {detail} = event;
      this.triggerEvent('onpreviewimage', detail);
    },
    onSkip: function (event) {
      let { detail } = event;
      this.triggerEvent('onskip', detail);
    },
  }
})
