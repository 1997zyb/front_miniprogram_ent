Component({
  /**
   * 组件的属性列表
   */
  properties: {
    message: Object
  },

  /**
   * 组件的初始数据
   */
  data: {
    title:'我是标题，很长很长的标题，标题标题标题标题标题标题标题标题',
    type:'工作',
    time:'9/13发布',
    times:'999次阅读',
    price:'220元/天',
    number:'12 人',
    workTiem:'9/12 至 9/17',
    workSection:'9:30 ~ 11:30，14:00 ~ 16:00',
    address:'南江滨大道432号南江滨大道432号南江滨大道432号南江滨大道432号南江滨大道',
    url:'/pages/jobDetail/jobDetail?job_uuid=9466a645-0990-4725-a5b2-2e79323b2462'
  },

  lifetimes: {
    ready: function () {
      if (this.data.message.content.content){
        this.setData({
          content: JSON.parse(this.data.message.content.content)
        })
      }
    }
  },

  /**
   * 组件的方法列表
   */
  methods: {
    skip: function (e) {
      var url = e.currentTarget.dataset.url;
      this.triggerEvent('onskip', url);
      wx.navigateTo({
        url: '/pages/jobDetail/index?job_uuid=' + this.data.content.job_uuid,
      })
    }
  }
})
