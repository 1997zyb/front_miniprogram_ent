import {getResume} from "../../utils/util";
import {getInfoByUserIdApi} from "../../api/index.js";
import {Service, getToken, reconnect, connect, Conversation, Status, setTabBarBadge} from '../../utils/im'

const utils = require('../../utils/utils');
const app = getApp();
const {
    globalData
} = getApp();
// const RongIMLib = require('../../lib/RongIMLib.miniprogram-1.0.8.js');
// const RongIMClient = RongIMLib.RongIMClient;
// const requestUserAuth = () => {
//   return new Promise((resolve, reject) => {
//     wx.getSetting({
//       success: function (res) {
//         console.log(res)
//         resolve(!!res.authSetting['scope.userInfo'])
//       },
//       fail: function (error) {
//         console.log(error);
//         reject(error)
//       }
//     })
//   });
// };

const watchConversation = (context) => {
    Conversation.watch((conversationList) => {
        context.setData({
            conversationList
        });
        context.getInfomation(conversationList)
    });
};

const watchStatus = () => {
    Status.watch((status) => {
        if (status == 3) {
            Status.connect();
        }
    })
}

// const connect = (context) => {
//   watchConversation(context);
//   watchStatus();
//   Status.connect().then(() => {
//     console.log('connect successfully');
//   }, (error) => {
//     wx.showToast({
//       title: error.msg,
//       icon: 'none',
//       duration: 3000
//     })
//   })
// };


let modalLogin = {}

Page({

    /**
     * 页面的初始数据
     */
    data: {
        hasUserAuth: true,
        conversationList: [],
        navH:app.globalData.navHeight
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        if (wx.getStorageSync('is_login')) {
            this.setData({
                hasUserAuth: true
            });
            let userInfo = app.globalData.userInfo
            if (userInfo.accountId) {
                getToken(userInfo.accountId, () => {
                    connect()
                })
            } else {
                getResume().then((userInfo) => {
                        getToken(userInfo.data.content.bean.accountId, () => {
                            connect()
                        })
                    }
                )
            }
        } else {
            this.setData({
                hasUserAuth: false
            });
            this.openLogin()
        }
    },

    onShow() {
        // Status.logout()
        // connect()

        reconnect({})
        this.setData({
            isLogin: wx.getStorageSync('is_login'),
            conversationList: wx.getStorageSync('conversationList'),
            height: wx.getSystemInfoSync().windowHeight - app.globalData.navHeight,
        })
        setTabBarBadge(this.data.conversationList ? this.data.conversationList : [], this)
    },

    //  打开登录弹窗
    openLogin() {
        this.setData({
            showLogin: true
        })
        modalLogin = this.selectComponent("#modal_login");
        modalLogin.openModal();
    },
    //  关闭登录弹窗
    closeLogin() {
        this.setData({
            showLogin: false
        })
        modalLogin = this.selectComponent("#modal_login");
        modalLogin.closeModal();
        this.onShow()
    },
    // onAuthCompleted: function(user){
    //   requestUserAuth().then((hasUserAuth) => {
    //     this.setData({
    //       hasUserAuth
    //     });
    //     if (hasUserAuth) {
    //       connect(this);
    //     }
    //   });
    // },


    gotoChat: function (event) {
        let {
            currentTarget: {
                dataset: {
                    item
                }
            }
        } = event;
        let {
            conversationType: type,
            targetId,
            target
        } = item;

        let isSame = (conversation, another) => {
            let isSaveType = (conversation.conversationType == another.conversationType);
            let isSaveTarget = (conversation.targetId == another.targetId);
            return (isSaveType && isSaveTarget);
        };


        let url = './chat?type={type}&targetId={targetId}&title={title}&accountId={accountId}';
        url = utils.tplEngine(url, {
            type,
            targetId,
            title: target.name,
            accountId: target.accountId
        });
        wx.navigateTo({
            url: url,
        });

        let {
            conversationList
        } = this.data;
        utils.map(conversationList, (conversation) => {
            if (isSame(conversation, item)) {
                conversation.unReadCount = 0;
            }
            return conversation
        });
        Conversation.clearUnreadCount(item);

        this.setData({
            conversationList
        });

    }
})
