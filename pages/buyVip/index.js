let app = getApp()
import {
  getVipUseInfoApi,
  getVipInfoApi,
  getClaimUserTelphoneApi,
  buyVipApi,
  getPayOrderApi
} from '../../api/index'

Page({
  data: {
    title: 'VIP会员',
    isService: false,
    vipInfoList: null,
    isLoad: false,
    cityName: '',
    cityId: '',
    currentTabIndex: 0,
    platform: app.globalData.platform,
    showCity: false
  },
  onLoad(option) {
    // console.log(this.data.platform)
    this.setData({
      cityName: wx.getStorageSync('current_city').name,
      cityId: wx.getStorageSync('current_city').id
    })
  },
  onShow() {
    this.getVipInfo()
  },
  onPullDownRefresh() {
    wx.stopPullDownRefresh();
  },
  changePackeg(e) {
    this.setData({
      currentTabIndex: e.currentTarget.dataset.index
    })
  },
  onTabChange(e) {
    this.setData({
      currentTabIndex: e.detail.index
    })
  },
  // 打开城市选择控件
  gotoCitySelectPage() {
    this.setData({
      showCity: true
    })

    // wx.navigateTo({
    //     url: '/pages/citySelect/index'
    // })
  },
  // 联系销售
  contact() {
    getClaimUserTelphoneApi().then(res => {
      let value = res.data.content.contractPhone;
      wx.makePhoneCall({
        phoneNumber: value,
        success: function() {
          console.log("拨打电话成功！")
        },
        fail: function() {
          console.log("拨打电话失败！")
        }
      })
    })
  },
  // 获取所在城市VIP信息
  getVipInfo() {
    getVipInfoApi({
      city_id: this.data.cityId
    }).then(res => {
      if (res.data.errCode === 0) {
        this.setData({
          vipInfoList: res.data.content.vip_package_entry_list
        }, {
          isLoad: true
        }, {
          isService: true
        }, )
      } else {
        this.setData({
          vipInfoList: []
        }, {
          isService: false
        }, {
          isLoad: true
        }, )
      }

    })
  },
  // 购买
  buy() {
    let param = {
      total_amount: this.data.vipInfoList[this.data.currentTabIndex].promotion_price,
      vip_package_id: this.data.vipInfoList[this.data.currentTabIndex].package_id,
      vip_city_id: this.data.cityId,
      saleman_id_code: '',
    }

    buyVipApi(param).then(res => {
      if (res.data.errCode === 0) {
        let param2 = {
          recharge_amount: this.data.vipInfoList[this.data.currentTabIndex].promotion_price,
          // recharge_amount: 1,
          pay_channel: 9,
          pay_channel_type: 4,
          client_time_millseconds: Date.now(),
          vip_order_id: res.data.content.vip_order_id,
        }
        getPayOrderApi(param2).then(res => {
          if (res.data.errCode === 0) {
            console.log({
              // timeStamp: res.data.content.timeStamp,
              timeStamp: Date.now().toString(),
              nonceStr: res.data.content.nonce_str,
              package: res.data.content.package,
              signType: 'MD5',
              paySign: res.data.content.pay_sign,
            })
            wx.requestPayment({
              'timeStamp': res.data.content.timeStamp,
              'nonceStr': res.data.content.nonce_str,
              'package': res.data.content.package,
              'signType': 'MD5',
              'paySign': res.data.content.pay_sign,
              success(res) {
                wx.showToast({
                  title: '购买成功'
                })
                wx.switchTab({
                  url: '/pages/center/index'
                });
              },
              fail(err) {
                wx.showToast({
                  title: '购买失败',
                  icon: 'none'
                })
                console.log(err)
              }
            })
          } else {
            
            wx.showToast({
              icon:'none',
              title: res.data.errMsg,
            })
          }
        })

      } else if (res.data.errCode == 88) {
        wx.hideToast()
        wx.showModal({
          title: '',
          content: '当前城市已开通VIP服务，不能重复购买\n' +
            '\n' +
            '是否切换其他城市',
          confirmText: '确定',
          cancelColor: '#b1b1b1',
          confirmColor: '#00BCD4',
          success: (res) => {
            if (res.confirm) {
              this.gotoCitySelectPage()
            } else {}
          }
        })
      }

    })
  },
  // 城市选择
  citySelect(e) {
    this.setData({
      cityName: e.detail.city.name,
      cityId: e.detail.city.id
    })
    this.getVipInfo()
  },

  // 前往使用明细
  userInfo() {
    wx.navigateTo({
      url: '/pages/vipRecord/index',
    })
  }
})