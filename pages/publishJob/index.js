import {
	postJobInfoApi,
	getJobClassifyListApi,
	queryJobNumApi,
	queryAccurateJobNumApi,
	getClaimUserTelphoneApi,
	getJobClassifyFirstSecondListApi,
	editParttimeJobApi,
	getEnumApi,
	getProvinceListApi,
	getJobTagListApi,
	queryAccurateResumeNumApi
} from "../../api/index";
import {
	findNumberLibraryApi,
	showJobContactInfo,
} from "../../api/index2"
import {
	parseTime,
	getArryNameById,
	formatTime
} from "../../utils/util";

let app = getApp() // 获取到小程序全局唯一的app实例
const limitNum = 1000
let modalAccurate = {}
let modalAccurateBuy = {}
let modalSelectUp = {}
Page({
	data: {
		columns: [],
		jobFirstGrade: '', //3全职;2线下兼职;1线上
		title: '发布职位',
		btnText: '发布',
		showIninput: true,
		isEdit: false,
		textHieght: 400,
		num: limitNum,
		jobContent: '',
		jobTitle: '',
		jobTitleLength: 0,
		today: Date.parse(new Date()),
		startTime: '工作开始时间',
		endTime: '工作结束时间',
		deadTime: '报名截止日期',
		classifyList: [], // 职位类别枚举
		classifyName: '', // 职位类别名称
		classifyId: '', // 职位类别ID
		employNum: '', // 招聘人数
		salary: '', // 薪资
		salaryMax: '', //薪资最大值
		isFaceTalk: 0,
		unitName: '元/天', // 单位名称
		unitId: 1, //  单位id
		settlementName: '当天结算', // 结算方式名称
		settlementId: 1, //  结算方式ID
		// payName: '在线支付',// 支付方式名称
		// payId: 1,// 支付方式ID
		limitSex: '', // 性别要求ID
		limitSexName: '性别要求（选填）', // 性别要求名称
		// limitAge: '', //  年龄限制
		age_low: '',
		age_high: '',
		age_unLimited: 0,
		limitHeight: '', //  身高限制
		limitWokeDate: '', // 上岗时间
		licenseVerify: '', // 驾照要求
		relNameVerify: false, // 实名认证
		healthCardVerify: false, // 健康证
		studentCardVerify: false, // 学生证
		lifePhotoVerify: false, // 生活照
		cityName: '', // 一级城市名称
		cityId: '', // 一级城市Id
		subCityName: '', // 二级城市名称
		subCityId: '', // 二级城市Id
		address: '', // 详细地址
		contactName: '', // 联系人
		contactNum: '', // 联系电话
		contactTypeId: 1, // 联系类型
		contactTypeName: '手机', // 联系类型
		phone: '', // 联系电话
		welfareCheckList: [], // 已选择的福利
		workingTimePeriod: [], // 添加的工作时段
		agreement: true,
		showWelfare: false,
		experienceId: '',
		experienceName: '经验不限',
		educationId: '',
		educationName: '学历不限',
		contactTypeList: [],
		experienceList: [],
		educationList: [],
		unitList: [],
		settlementList: [],
		// payIdList: [{id: 1, name: '在线支付'}, {id: 2, name: '现金支付'}],
		sexList: [{
			id: '',
			name: '不限'
		}, {
			id: 1,
			name: '男'
		}, {
			id: 0,
			name: '女'
		}],
		welfareList: [{
			id: 1,
			name: '底薪',
			checked: 0
		}, {
			id: 2,
			name: '培训',
			checked: 0
		}, {
			id: 3,
			name: '绩效',
			checked: 0
		}, {
			id: 4,
			name: '餐补',
			checked: 0
		}, {
			id: 5,
			name: '包住',
			checked: 0
		}],
		timePeriodList: ['01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00',
			'12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00',
			'24:00'
		],
		workTimeList: [
			['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00',
				'12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'
			],
			['01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00',
				'13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00', '24:00'
			],

		], // 工作时段枚举
		jobIntervalArray: [{
			values: ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00',
				'11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00',
				'23:00'
			],
			className: 'jobIntervalColumn1',
			defaultIndex: 0
		}, {
			values: ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00',
				'11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00',
				'23:00'
			],
			className: 'jobIntervalColumn1',
			defaultIndex: 0,
		}],
		jobIntervalIndex: [0, 1],
		operaAuthorize: null,
		postId: '',
		editLimit: false,
		showCity: false,
		publishJobType: '',
		deadTimeStamp: new Date().getTime(),
		minDateDead: new Date().getTime(),
		startTimeStamp: new Date().getTime(),
		formatterStart(type, value) {
			if (type === 'year') {
				return `${value}年`;
			} else if (type === 'month') {
				return `${value}月`;
			} else
				return `${value}日`;
			return value;
		},
		endTimeStamp: new Date().getTime(),
		formatterEnd(type, value) {
			if (type === 'year') {
				return `${value}年`;
			} else if (type === 'month') {
				return `${value}月`;
			} else
				return `${value}日`;
			return value;
		},
		jobList: [],
		jobNameList: [],
		selectJobType: 0,
		numIsDisabled: false, // 是否禁用输入框
		showNumLibraryBtn: false, // 是否显示编辑号码库按钮
		contactStock:0, //
		dataFromNumLibrary:[],
		post_id:""
	},
	onLoad(option) {
		// this.testData()
		console.log(option) 
		this.setData({
			post_id:option.postId
		})
		console.log(app) // 可以在app中获取到个人信息
		app.globalData.recordTime.startTime = new Date()
		this.setData({
			jobFirstGrade: app.globalData.publishInfo.jobRankType,
			contactStock:app.globalData.userInfo.contactStock || 0,
			// selectJobType: option.type
		})
		this.getEmus()
		this.getJobTagList(() => { //获取服务端下发的枚举
			this.setData({
				cityId: wx.getStorageSync('current_city').id,
				cityName: wx.getStorageSync('current_city').name,
				subCityName: '全市'
			})
			if (option && option.postId) { //  如果是编辑职位初始化数据
				this.initData()
				this.setData({
					option
				})
			} else {
				app.globalData.publishInfo.age_high = ''
				app.globalData.publishInfo.age_low = ''
				app.globalData.publishInfo.age_unLimited = 1
				if (this.data.jobFirstGrade == 3) { // 全职设置默认选项
					this.setData({
						unitId: 3,
						unitName: '元/月',
						contactName: app.globalData.userInfo.name ? app.globalData.userInfo.name : '',
						contactNum: app.globalData.userInfo.contactTel ? app.globalData.userInfo.contactTel : '', //获取联系号码
						contactTypeId: '1'
					})
				} else if (this.data.jobFirstGrade == 2) {
					this.setData({
						unitId: 1,
						unitName: '元/日',
						contactName: app.globalData.userInfo.name ? app.globalData.userInfo.name : '',
						contactNum: app.globalData.userInfo.contactTel ? app.globalData.userInfo.contactTel : '',
						contactTypeId: '1'
					})
				} else {
					this.setData({
						unitId: 4,
						unitName: '元/次',
						contactName: app.globalData.userInfo.name ? app.globalData.userInfo.name : '',
						contactNum: app.globalData.userInfo.contactTel ? app.globalData.userInfo.contactTel : '',
						contactTypeId: '1'
					})
				}
			}
		})
	},
	onUnload() {
		app.globalData.jobChange = true
	},
	onShow() {
		console.log(this.data.dataFromNumLibrary)
		console.log(this.data.contactTypeId)
		console.log(app.globalData)
		console.log(app.globalData.postInfo)
		console.log(app.globalData.publishInfo)
		this.checkLimitPage()
		this.setData({
			age_low: app.globalData.publishInfo.age_low || '',
			age_high: app.globalData.publishInfo.age_high || '',
			age_unLimited: app.globalData.publishInfo.age_unLimited || 0,
			limitSex: app.globalData.publishInfo.limitSex || app.globalData.publishInfo.limitSex == 0 ? app.globalData.publishInfo
				.limitSex : '',
			limitHeight: app.globalData.publishInfo.limitHeight ? app.globalData.publishInfo.limitHeight : '',
			limitWokeDate: app.globalData.publishInfo.limitWokeDate ? app.globalData.publishInfo.limitWokeDate : '',
			limitIdentity: app.globalData.publishInfo.limitIdentity ? app.globalData.publishInfo.limitIdentity : '',
			licenseVerify: app.globalData.publishInfo.licenseVerify ? app.globalData.publishInfo.licenseVerify : '',
			relNameVerify: app.globalData.publishInfo.relNameVerify ? app.globalData.publishInfo.relNameVerify : '',
			healthCardVerify: app.globalData.publishInfo.healthCardVerify ? app.globalData.publishInfo.healthCardVerify : '',
			studentCardVerify: app.globalData.publishInfo.studentCardVerify ? app.globalData.publishInfo.studentCardVerify :
				'',
			lifePhotoVerify: app.globalData.publishInfo.lifePhotoVerify ? app.globalData.publishInfo.lifePhotoVerify : '',
			operaAuthorize: app.globalData.operaAuthorize ? app.globalData.operaAuthorize : null,
			// contactNum: app.globalData.publishInfo.contact?
			// app.globalData.publishInfo.contact.contact_num: app.globalData.userInfo.contactTel,
			// contactTypeId: app.globalData.publishInfo.contact ? app.globalData.publishInfo.contact.contact_type : '1',
			// showNumLibraryBtn:app.globalData.publishInfo.contact? (app.globalData.publishInfo.contact.contact_type > 5 ? true : false):false
			showNumLibraryBtn:this.data.contactTypeId > 5?true:false
		})
		console.log(this.data.contactNum)
		//  判断当前选择城市是否还有职位数
		if (this.data.cityId && !this.data.postId && !this.data.isEdit) {
			let param = {
				city_id: this.data.cityId
			}
			queryJobNumApi(param).then(res => {
				if (res.data.content.city_can_use_recruit_job_num > 0 || res.data.content.nation_can_use_recruit_job_num) {} else {
					this.contact()
				}
			})
		}
	},
	numChange(){
		this.setData({
			contactNum: app.globalData.publishInfo.contact.contact_num
		})
	},
	// 检查更多要求是否有填写
	checkLimitPage() {
		let flag = false
		if (app.globalData.publishInfo.age_low && app.globalData.publishInfo.age_unLimited != 1)
			flag = true
		if (app.globalData.publishInfo.limitSex && app.globalData.publishInfo.limitSex != '性别不限')
			flag = true
		if (app.globalData.publishInfo.limitHeight && app.globalData.publishInfo.limitHeight != '身高不限')
			flag = true
		if (app.globalData.publishInfo.limitWokeDate && app.globalData.publishInfo.limitWokeDate != '不限')
			flag = true
		if (app.globalData.publishInfo.limitIdentity && app.globalData.publishInfo.limitIdentity != '身份不限')
			flag = true
		if (app.globalData.publishInfo.relNameVerify)
			flag = true
		if (app.globalData.publishInfo.healthCardVerify)
			flag = true
		if (app.globalData.publishInfo.studentCardVerify)
			flag = true
		if (app.globalData.publishInfo.lifePhotoVerify)
			flag = true
		if (app.globalData.publishInfo.licenseVerify)
			flag = true
		this.setData({
			flag
		})
	},
	// 编辑职位初始化职位数据
	initData() {
		let post = app.globalData.postInfo
		console.log(post)
		if (app.globalData.postInfo.status == 2 || app.globalData.postInfo.status == 4) {
			this.setData({
				editLimit: true
			})
		} else {
			this.setData({
				editLimit: false
			})
		}
		this.welfareInit(post.job_tags)
		this.setJobExerience(post)
		this.setData({
			title: app.globalData.publishInfo.isRepublic ? '发布岗位' : '编辑职位',
			isEdit: true,
			btnText: app.globalData.publishInfo.isRepublic ? '发布' : '保存',
			cityId: post.city_id,
			cityName: post.city_name,
			subCityName: post.address_area_name,
			subCityId: post.address_area_id,
			postId: post.job_uuid,
			jobTitle: post.job_title,
			jobTitleLength: post.job_title.length,
			classifyName: post.job_classfie_name,
			classifyId: post.job_type_id,
			startTime: '工作开始时间',
			endTime: '工作结束时间',
			deadTime: '报名截止日期',
			// startTime: post.working_time_start_date,
			// endTime: post.working_time_end_date,
			// deadTime: parseTime(post.apply_dead_time, '{y}-{m}-{d}'),
			workingTimePeriod: this.reFormatWorkTime(post.working_time_period),
			employNum: post.recruitment_num,
			salary: post.salary.value,
			salaryMax: post.salary.value_max,
			isFaceTalk: post.salary.is_face_talk,
			unitName: post.salary.unit_value,
			unitId: post.salary.unit,
			settlementName: post.salary.settlement_value,
			settlementId: post.salary.settlement,
			// payName: post.salary.pay_type?this.data.payIdList[post.salary.pay_type - 1].name,
			// payId: post.salary.pay_type,
			jobContent: post.job_desc,
			address: post.working_place,
			age_low: post.age_low || '',
			age_high: post.age_high || '',
			age_unLimited: post.age_unLimited || 0,
			limitSex: post.sex || post.sex == 0 ? post.sex : '',
			limitSexName: post.sex ? this.data.sexList[post.sex].name : post.sex == 0 ? this.data.sexList[2].name : '性别要求（选填）',
			welfareList: this.data.welfareList,
			experienceId: post.work_experience,
			experienceName: post.apply_limit_name_arr.workExperience ? post.apply_limit_name_arr.workExperience : '经验不限',
			educationId: post.academic_certificate,
			educationName: post.apply_limit_name_arr.academicCertificate ? post.apply_limit_name_arr.academicCertificate : '学历不限',
			contactName: post.contact.name ? post.contact.name : app.globalData.userInfo.name ? app.globalData.userInfo.name :
				'',
			contactNum: post.contact.contact_num ? post.contact.contact_num : app.globalData.userInfo.contactTel ? app.globalData
				.userInfo.contactTel : '',
			contactTypeId: post.contact.contact_type ? post.contact.contact_type : '1',
			publishJobType: post.shelf_mode

		})
		app.globalData.publishInfo.fistGradeId = app.globalData.publishInfo.fistGradeId ? app.globalData.publishInfo.fistGradeId :
			post.fistGradeId, // 职位一级id
			app.globalData.publishInfo.secondGradeId = app.globalData.publishInfo.secondGradeId ? app.globalData.publishInfo.secondGradeId :
			post.job_type_id
		// app.globalData.publishInfo.limitAge = post.age
		app.globalData.publishInfo.age_unLimited = post.age_unLimited || 0
		app.globalData.publishInfo.age_low = post.age_low || ''
		app.globalData.publishInfo.age_high = post.age_high || ''
		app.globalData.publishInfo.limitSex = post.sex
		app.globalData.publishInfo.limitIdentity = post.age
		app.globalData.publishInfo.limitHeight = post.height
		app.globalData.publishInfo.limitWokeDate = post.apply_job_date
		app.globalData.publishInfo.relNameVerify = post.rel_name_verify
		app.globalData.publishInfo.healthCardVerify = post.health_cer
		app.globalData.publishInfo.studentCardVerify = post.stu_id_card
		app.globalData.publishInfo.lifePhotoVerify = post.life_photo
		app.globalData.publishInfo.licenseVerify = post.drive_license
		app.globalData.publishInfo.contactName = post.contact.name
		app.globalData.publishInfo.contactTel = post.contact.phone_num
		app.globalData.publishInfo.contactType = post.contact.contact_type
		if(post.contact.contact_type > 5){
			this.setData({
				numIsDisabled:true,
				showNumLibraryBtn:true
			})
		}
	},
	// 测试数据
	testData() {
		this.setData({
			jobContent: '职位描述职位描述',
			jobTitle: '职位标题职位标题',
			address: '联系地址联系地址',
			employNum: '10', // 招聘人数
			salary: '12000', // 薪资
			salaryMax: '18000', //薪资最大值
		})
	},
	setJobExerience(post) {
		getJobClassifyFirstSecondListApi({
			classify_type: 3
		}).then((res) => {
			let jobList = res.data.content.first_level_job_classify_list
			let jobListSelect = post.job_classify_id_list == "-1" ? [] : post.job_classify_id_list.split(',').map(Number)
			let selectJobList = []
			let jobNameList = []
			jobList.forEach((item, index) => {
				item.second_level_job_classify_list.forEach((items, indexs) => {
					if (jobListSelect.indexOf(parseInt(items.id)) != -1) {
						selectJobList.push({
							jobIndex: index,
							jobIndexs: indexs,
						})
						jobNameList.push(items.job_classify_name)
						items.check = true
					}
				})
			})
			this.setData({
				jobList: jobListSelect,
				jobNameList,
			})
		})
	},
	// 获取枚举数据
	getEmus() {
		getEnumApi().then(res => {
			console.log(res)
			let emus = res.data.content.enumList
			emus.listWorkExperienceEnum.unshift({
				id: '',
				name: '经验不限'
			})
			emus.listEducationTypeEnum.unshift(emus.listEducationTypeEnum[emus.listEducationTypeEnum.length - 1])
			emus.listEducationTypeEnum.unshift(emus.listEducationTypeEnum[emus.listEducationTypeEnum.length - 2])
			emus.listEducationTypeEnum.splice(emus.listEducationTypeEnum.length - 1, 1)
			emus.listEducationTypeEnum.splice(emus.listEducationTypeEnum.length - 1, 1)
			emus.listEducationTypeEnum.unshift({
				id: '',
				name: '学历不限'
			})
			this.setData({
				// 
				contactTypeList: this.data.contactStock == 1? emus.listContactTypeEnum:emus.listContactTypeEnum.slice(0,5),
				experienceList: emus.listWorkExperienceEnum,
				educationList: emus.listEducationTypeEnum,
				unitList: emus.listSalaryUnitEnum,
				settlementList: emus.listSettlementWayEnum,

			})
		})


	},
	// 获取枚举
	getJobTagList(callBack) {
		getJobTagListApi({
			job_classify_type: this.data.jobFirstGrade
		}).then(res => {
			let welfareList = []
			res.data.content.job_tag_list.forEach(item => {
				welfareList.push({
					id: item.tag_id,
					name: item.tag_title,
					check: 0,
				})
			})
			this.setData({
				welfareList
			})
			callBack()
		})
	},
	// 发布职位不足联系销售
	contact() {
		let vip = this.selectComponent("#vip");
		vip.openModal('您当前可发职位不足，\n' +
			'请联系销售购买相关套餐', '立即联系', app.globalData.platform);
		this.setData({
			showIninput: false,
		})
		// getClaimUserTelphoneApi().then(res => {
		//     let value = res.data.content.contractPhone;
		//     wx.showModal({
		//         title: '',
		//         content: '您当前可发职位不足，\n' +
		//             '\n' +
		//             '请联系销售购买相关套餐',
		//         confirmText: '立即联系',
		//         cancelColor: '#b1b1b1',
		//         confirmColor: '#00BCD4',
		//         success: (res) => {
		//             if (res.confirm) {
		//                 wx.makePhoneCall({
		//                     phoneNumber: value,
		//                 })
		//             } else {
		//                 this.setData({
		//                     cityId: '',
		//                     cityName: '',
		//                 })
		//             }
		//         }
		//     })
		// })
	},
	//  职位分类选择
	bindClassChange(e) {
		this.setData({
			classifyName: this.data.classifyList[e.detail.value].job_classfier_name,
			classifyId: this.data.classifyList[e.detail.value].job_classfier_id
		})
	},
	//  职位名称输入
	jobTitleChange(e) {
		this.setData({
			jobTitle: e.detail.value,
			jobTitleLength: e.detail.value.length
		})
	},
	//  经验选择事件
	bindExperienceChange(e) {
		console.log(e)
		this.setData({
			experienceId: e.detail.id,
			experienceName: e.detail.name,
		})
		this.onClose()
	},
	//  学历选择事件
	bindEducationChange(e) {
		this.setData({
			educationId: e.detail.id,
			educationName: e.detail.name,
		})
		this.onClose()
	},
	//  联系方式选择事件
	// bindContactTypeChange(e) {
	//     this.setData({
	//         contactTypeId: this.data.contactTypeList[e.detail.value].id
	//     })
	// },
	// 报名结束时间展示
	showDeadTime() {
		this.setData({
			showIninput: false,
			showDeadTime: true
		})
	},
	// 选择工作报名结束
	onInputDead(event) {
		this.setData({
			deadTimeStamp: event.detail,
			deadTime: formatTime(event.detail, 'Y-M-D')
		});

		this.onClose()
	},
	// 工作开始时间展示
	showStart() {
		this.setData({
			showIninput: false,
			showStart: true
		})
	},
	// 选择工作开始时间
	onInputStart(event) {
		this.setData({
			startTimeStamp: event.detail,
			startTime: formatTime(event.detail, 'Y-M-D')
		});
		this.onClose()
	},
	// 工作结束时间展示
	showEnd() {
		this.setData({
			showIninput: false,
			showEnd: true
		})
	},
	// 选择工作结束时间
	onInputEnd(event) {
		console.log(event)
		this.setData({
			endTimeStamp: event.detail,
			endTime: formatTime(event.detail, 'Y-M-D')
		});
		this.onClose()
	},
	//  截止时间选择
	bindDeadDateChange(e) {
		this.setData({
			deadTime: e.detail.value
		})
	},
	//  添加工作时段
	addWorkTime(e) {
		let t = {}
		t.startTime = this.data.workTimeList[0][e.detail.value[0]]
		t.endTime = this.data.workTimeList[1][e.detail.value[1] ? e.detail.value[1] : 0]
		this.data.workingTimePeriod.push(t)
		this.setData({
			workingTimePeriod: this.data.workingTimePeriod
		})
	},
	// 工作工作开始时间选择事件
	bindcolumnchange(e) {
		if (e.detail.column === 0) {
			this.data.workTimeList[1] = this.data.timePeriodList.concat()
			this.data.workTimeList[1].splice(0, e.detail.value)
			this.setData({
				workTimeList: this.data.workTimeList
			})
		}
	},
	//  删除工作时段
	delWorkTime(e) {
		this.data.workingTimePeriod.splice(e.currentTarget.dataset.index, 1)
		this.setData({
			workingTimePeriod: this.data.workingTimePeriod
		})
	},
	//  清空工作时段
	cleanWorkTime() {
		this.setData({
			workingTimePeriod: []
		})
	},
	//  招聘人数选择
	employNumChange(e) {
		this.setData({
			employNum: e.detail.value
		})
	},
	//  薪资输入框
	salaryChange(e) {
		this.setData({
			salary: e.detail.value
		})
	},
	//  薪资最大值输入框
	salaryMaxChange(e) {
		this.setData({
			salaryMax: e.detail.value
		})
	},
	//  薪水类别选择
	// bindUnitChange(e) {
	//     this.setData({
	//         unitId: this.data.unitList[e.detail.value].id,
	//         unitName: this.data.unitList[e.detail.value].name
	//     })
	// },
	//  结算方式选择
	// bindSettlementTypeChange(e) {
	//     this.setData({
	//         settlementType: this.data.settlementList[e.detail.value].id,
	//         settlementName: this.data.settlementList[e.detail.value].name
	//     })
	// },
	// //  支付方式选择
	// bindPayTypeChange(e) {
	//     this.setData({
	//         payId: this.data.payIdList[e.detail.value].id,
	//         payName: this.data.payIdList[e.detail.value].name
	//     })
	// },
	//  性别限制
	bindLimitSexChange(e) {
		this.setData({
			limitSex: this.data.sexList[e.detail.value].id,
			limitSexName: this.data.sexList[e.detail.value].name
		})
	},
	salaryTypeChange(e) {
		this.setData({
			isFaceTalk: e.currentTarget.dataset.id,
		})
	},
	//  福利选择
	welfareClick(e) {
		this.data.welfareList.forEach(item => {
			if (item.id === e.currentTarget.dataset.id) {
				if (item.checked == 1) {
					item.checked = 0
					let index = this.data.welfareCheckList.indexOf(item.id)
					this.data.welfareCheckList.splice(index, 1)
				} else {
					item.checked = 1
					this.data.welfareCheckList.push(item.id)
				}
			}
		})
		this.setData({
			welfareList: this.data.welfareList
		})
		this.setData({
			welfareCheckList: this.data.welfareCheckList
		})
	},
	//  工作描述输入
	getValue(e) {
		if (e.detail.value.length > 250) {
			this.setData({
				textHieght: 1000
			})
		}
		if (e.detail.cursor >= limitNum) {
			this.setData({
				jobContent: e.detail.value.substring(0, limitNum)
			})
		} else {
			this.setData({
				jobContent: e.detail.value
			})
		}

	},
	//  详细地址输入框
	addressChange(e) {
		this.setData({
			address: e.detail.value
		})
	},
	//  联系人输入框
	contactChange(e) {
		this.setData({
			contactName: e.detail.value
		})
	},
	//  联系电话输入框
	phoneChange(e) {
		this.setData({
			contactNum: e.detail.value
		})
	},
	//  跳转职位限制页面
	gotoJobLimitPage() {
		wx.navigateTo({
			url: '/pages/limit/index'
		})
	},
	//  跳转城市选择页面
	gotoCitySelectPage() {
		if (this.data.editLimit) return
		this.setData({
			showCity: true
		})

		// wx.navigateTo({
		//     url: '/pages/citySelect/index?isFromPublish=true'
		// })
	},
	//  跳转职位发布协议页面
	gotoWebViewPage() {
		wx.navigateTo({
			url: '/pages/webView/index?url=https://www.jianke.cc/wap/toJobPublishAgreementPage&title=兼客招聘发布职位协议'
		})
	},
	//  勾选同意职位发布协议
	changeAgreementState() {
		this.setData({
			agreement: !this.data.agreement
		})
	},
	//  打开福利弹窗
	onpenModal() {
		this.setData({
			showWelfare: true,
			showIninput: false
		})
	},
	// 初始化福利
	welfareInit(list) {
		console.log(this.data.welfareList)
		if (list) {
			list.forEach(item => {
				this.data.welfareList.forEach(welfare => {
					if (item.tag_id == welfare.id) {
						welfare.checked = 1
						this.data.welfareCheckList.push(welfare.id)
						this.setData({
							welfareCheckList: this.data.welfareCheckList
						})
					}
				})
			})
		}
	},
	//  关闭福利弹窗
	closeModal(e) {
		if (e.currentTarget.dataset.type == 0) {
			this.setData({
				welfareList: [{
						id: 1,
						name: '餐补',
						checked: 0
					}, {
						id: 2,
						name: '培训',
						checked: 0
					},
					{
						id: 3,
						name: '绩效',
						checked: 0
					},
					{
						id: 4,
						name: '底薪',
						checked: 0
					},
					{
						id: 5,
						name: '包住',
						checked: 0
					}
				],
				welfareCheckList: []
			})
		}
		this.setData({
			showWelfare: false,
			showIninput: true
		})
	},
	//  工作时段转换
	formatWorkTime(arr) {
		if (arr && arr.length === 0) {
			return {}
		} else {
			let list = {}
			if (arr.length === 1) {
				list.f_start = new Date().setHours(arr[0].startTime.substring(0, 2), 0, 0, 0)
				list.f_end = new Date().setHours(arr[0].endTime.substring(0, 2), 0, 0, 0)
			} else if (arr.length === 2) {
				list.f_start = new Date().setHours(arr[0].startTime.substring(0, 2), 0, 0, 0)
				list.f_end = new Date().setHours(arr[0].endTime.substring(0, 2), 0, 0, 0)
				list.s_start = new Date().setHours(arr[1].startTime.substring(0, 2), 0, 0, 0)
				list.s_end = new Date().setHours(arr[1].endTime.substring(0, 2), 0, 0, 0)
			} else if (arr.length === 3) {
				list.f_start = new Date().setHours(arr[0].startTime.substring(0, 2), 0, 0, 0)
				list.f_end = new Date().setHours(arr[0].endTime.substring(0, 2), 0, 0, 0)
				list.s_start = new Date().setHours(arr[1].startTime.substring(0, 2), 0, 0, 0)
				list.s_end = new Date().setHours(arr[1].endTime.substring(0, 2), 0, 0, 0)
				list.t_start = new Date().setHours(arr[2].startTime.substring(0, 2), 0, 0, 0)
				list.t_end = new Date().setHours(arr[2].endTime.substring(0, 2), 0, 0, 0)
			}
			return list
		}
	},
	//  工作时段转换
	reFormatWorkTime(obj) {
		if (obj && Object.keys(obj).length === 0) {
			return []
		} else {
			let list = []
			if (Object.keys(obj).length > 1) {
				list.push({
					startTime: this.data.timePeriodList[new Date(obj.f_start).getHours() - 0],
					endTime: this.data.timePeriodList[new Date(obj.f_end).getHours() - 0]
				})
			}
			if (Object.keys(obj).length > 3) {
				list.push({
					startTime: this.data.timePeriodList[new Date(obj.s_start).getHours() - 0],
					endTime: this.data.timePeriodList[new Date(obj.s_end).getHours() - 0]
				})
			}
			if (Object.keys(obj).length > 5) {
				list.push({
					startTime: this.data.timePeriodList[new Date(obj.t_start).getHours() - 0],
					endTime: this.data.timePeriodList[new Date(obj.t_end).getHours() - 0]
				})
			}
			return list
		}
	},
	//  福利转换
	formatWelfare(arr) {
		if (arr && arr.length === 0) {
			return []
		} else {
			let list = []
			arr.forEach(item => {
				if (item.checked) {
					let o = {}
					o.tag_id = item.id
					o.check_status = item.checked
					list.push(o)
				}
			})
			return list
		}
	},
	//  校验必填项
	verify() {
		console.log(this.data.contactTypeId)
		var patrn = /^[+-]?(0|([1-9]\d*))(\.\d+)?$/g;
		if (this.data.jobTitle.length < 5 || this.data.jobTitle.length > 20) {
			wx.showToast({
				title: '请输入5~20个字的职位名称',
				icon: 'none'
			})
			return false
		}else if(this.data.contactTypeId >= 6 && this.data.contactStock == 0){
			wx.showToast({
				title: '请选择联系方式',
				icon: 'none'
			})
			return false
		}
		// 招聘人数
		else if (!this.data.employNum || this.data.employNum <= 0) {
			wx.showToast({
				title: '请输入招聘总人数',
				icon: 'none'
			})
			return false
		}
		// 薪资
		else if (this.data.isFaceTalk == 0 && (!this.data.salary || this.data.salary <= 0)) {
			wx.showToast({
				title: '请输入薪资',
				icon: 'none'
			})
			return false
		} else if (this.data.isFaceTalk == 0 && !patrn.exec(this.data.salary)) {
			wx.showToast({
				title: '薪资格式为输入数字',
				icon: 'none'
			})
			return false
		} else if (this.data.isFaceTalk == 0 && (parseInt(this.data.salary) > parseInt(this.data.salaryMax))) {
			wx.showToast({
				title: '薪资最大金额必须大于或等于最小金额',
				icon: 'none'
			})
			return false
		}
		// 工作开始时间
		else if (this.data.jobFirstGrade == 2 && this.data.startTime === '工作开始时间') {
			wx.showToast({
				title: '请选择工作开始时间',
				icon: 'none'
			})
			return false
		}
		// 工作结束时间
		else if (this.data.jobFirstGrade == 2 && this.data.endTime === '工作结束时间') {
			wx.showToast({
				title: '请选择工作结束时间',
				icon: 'none'
			})
			return false
		}
		// 工作结束时间大于工作开始时间
		else if (new Date(this.data.startTime).getTime() > new Date(this.data.endTime).getTime()) {
			wx.showToast({
				title: '工作结束时间不可以小于工作开始时间',
				icon: 'none'
			})
			return false
		}
		// 报名截止日期
		/*       else if (this.data.deadTime === '报名截止日期') {
           wx.showToast({
               title: '请选择截止日期',
               icon: 'none'
           })
           return false
       } else if (new Date(this.data.deadTime).getTime() < new Date(this.data.startTime).getTime() || new Date(this.data.deadTime).getTime() > new Date(this.data.endTime).getTime()) {
           wx.showToast({
               title: '职位截止报名日期必填，且要在当前日期到职位结束日期范围内',
               icon: 'none'
           })
           return false
       }*/

		// 职位说明
		else if (this.data.jobContent.length < 1) {
			wx.showToast({
				title: '请输入职位说明',
				icon: 'none'
			})
			return false
		}
		// 工作城市
		else if (!this.data.cityId && this.data.jobFirstGrade != 1) {
			wx.showToast({
				title: '请选择城区',
				icon: 'none'
			})
			return false
		}
		// 详细地址
		else if (this.data.jobFirstGrade != 1 && (!this.data.address || this.data.address.length < 6 || this.data.address.length >
				32)) {
			wx.showToast({
				title: '请输入6~32个字详细地址',
				icon: 'none'
			})
			return false
		}
		// 联系人
		else if (!this.data.contactName || this.data.contactName.length < 2 || this.data.contactName.length > 10) {
			wx.showToast({
				title: '请输入2~10个字联系人信息',
				icon: 'none'
			})
			return false
		}
		// 电话号码
		else if (!this.data.contactNum) {
			wx.showToast({
				title: '请输入联系号码',
				icon: 'none'
			})
			return false
		}
		// 校验电话号码格式
		else if (false && !(new RegExp(/(?:^1\d{10}$)|(?:^0\d{2,3}-?\d{7,9}$)/).test(this.data.phone))) {
			wx.showToast({
				title: '请填写正确电话号码',
				icon: 'none'
			})
			return false
		}
		// 勾选协议
		else if (!this.data.agreement) {
			wx.showToast({
				title: '必须同意《宅任务职位发布协议》才能发布哦',
				icon: 'none'
			})
			return false
		} else if (this.data.clickFlag) {
			wx.showToast({
				title: '正在发布中',
				icon: 'loading'
			})
			return false
		} else if (this.data.contactNum == "" ||
			this.data.contactNum == "请添加号码") {
			wx.showToast({
				title: '请输入联系号码',
				icon: 'none'
			})
			return false
		}
		return true
	},
	//  编辑/发布选择上架方式
	publish() {
		if (this.verify()) {
			this.setData({
				showIninput: false
			})
			modalSelectUp = this.selectComponent("#modal-select-up");
			modalSelectUp.openModal()
		}
	},
	// 普通和精品区分提示
	publishNext(e) {
		let param = {
			city_id: (this.data.jobFirstGrade != 1 ? this.data.cityId : wx.getStorageSync('current_city').id)
		}
		if (e.detail == 0) {
			if (this.data.isEdit && this.publishJobType == 2) {
				this.publishNextUp(e)
			} else {
				queryAccurateJobNumApi(param).then(res => {

					if (res.data.errCode == 99) {
						this.setData({
							showIninput: false,
						})

						let modalAccurateBuyJob = this.selectComponent("#modal_accurate_buy_job");
						modalAccurateBuyJob.openModal()
						return
					} else if (res.data.errCode == 100) {
						this.setData({
							showIninput: false,
						})
						let vip = this.selectComponent("#vip");
						vip.openModal('套餐内精品岗位数不足，您可以联系招聘顾问升级套餐', '立即联系', 'ios');
						return
					} else
						this.publishNextUp(e)
				})
			}

		} else {
			if (this.isEdit && this.publishJobType == 1) {
				this.publishNextUp(e)
			} else {
				queryJobNumApi(param).then(res => {
					if (res.data.content.city_can_use_recruit_job_num > 0 || res.data.content.nation_can_use_recruit_job_num || (
							this.data.option && this.data.option.postId)) {
						this.publishNextUp(e)
					} else {
						this.contact()
					}
				})
			}
		}
	},
	// 编辑/发布
	publishNextUp(e) {
		console.log(e)
		this.setData({
			selectJobType: parseInt(e.detail)
		})
		// if (this.verify()) {
		this.setData({
			clickFlag: true
		})
		setTimeout(() => {
			this.setData({
				clickFlag: false
			})
			wx.hideLoading()
		}, 3000)

		// if (this.verify()) {
		let startTime = new Date(this.data.startTime).getTime()
		let endTime = new Date(this.data.endTime).getTime()
		// let deadTime = new Date(this.data.deadTime).getTime()
		let workingTimePeriod = this.formatWorkTime((this.data.workingTimePeriod))
		let editParam = {}
		let commonParam = {
			job_rank_type: app.globalData.publishInfo.jobRankType, // 职位类别
			job_first_classify_type: app.globalData.publishInfo.fistGradeId, // 职位一级id
			job_type_id: app.globalData.publishInfo.secondGradeId, // 职位二级ID
			// job_type: this.data.selectJobType == 0 ? 6 : 1, // 职位类型，1表示普通职位 默认是1,6是精品岗位
			job_type: 1,
			job_title: this.data.jobTitle, // 工作标题
			job_desc: this.data.jobContent, // 工作描述
			recruitment_num: this.data.employNum, // 招聘人数
			job_tags: this.formatWelfare(this.data.welfareList), //福利
			salary: {
				unit: this.data.unitId,
				value: this.data.salary,
				settlement: this.data.settlementType,
				// pay_type: this.data.payId,
				value_max: this.data.salaryMax,
				is_face_talk: this.data.isFaceTalk
			}, // 薪水
			contact: {
				name: this.data.contactName,
				contact_type: this.data.contactTypeId,
				contact_num: this.data.contactNum
			}, // 联系方式
			sex: this.data.limitSex, // 性别限制
			age_low: this.data.age_low,
			age_high: this.data.age_high,
			age_unLimited: this.data.age_unLimited,
			// age: this.data.limitAge, // 年龄限制
			identity_type: this.data.limitIdentity, //身份要求，没有默认不传【1在校生；2社会人】
			drive_license: this.data.licenseVerify, //驾照要求，默认0，1为需要
			rel_name_verify: this.data.relNameVerify, // 实名认证
			life_photo: this.data.lifePhotoVerify, // 生活照
			health_cer: this.data.healthCardVerify, // 健康证
			stu_id_card: this.data.studentCardVerify, // 学生证
			city_id: (this.data.jobFirstGrade != 1 ? this.data.cityId : wx.getStorageSync('current_city').id)
		}
		let param = {}
		// 全职
		if (app.globalData.publishInfo.jobRankType == 3) {
			param = {
				address_area_id: this.data.subCityId,
				working_place: this.data.address,
				work_experience: this.data.experienceId ? this.data.experienceId : '', // 工作经验要求，默认不限
				academic_certificate: this.data.educationId || this.data.educationId == 0 ? this.data.educationId : '', //学历要求，默认不限
			}
		}
		// 兼职
		else if (app.globalData.publishInfo.jobRankType == 2) {
			param = {
				working_time_start_date: startTime,
				working_time_end_date: endTime,
				working_time_period: workingTimePeriod,
				address_area_id: this.data.subCityId,
				working_place: this.data.address,
			}
		}
		// 线上
		else if (app.globalData.publishInfo.jobRankType == 1) {
			param = {
				settlement: this.data.settlementId
			}
		}
		// 编辑职位
		if (this.data.isEdit && !app.globalData.publishInfo.isRepublic) {
			editParam = {
				job_id: app.globalData.postInfo.job_id,
				status: app.globalData.postInfo.status,
				version: app.globalData.postInfo.version
			}
			param = {
				parttime_job: JSON.stringify(Object.assign(commonParam, param, editParam)),
				submit_type: (this.data.selectJobType == 0 ? 2 : 1),
				job_classify_id_list: this.data.jobList,
			}
			editParttimeJobApi(param).then(res => {
				this.setData({
					clickFlag: false
				})
				if (res.data.errCode == 0) {
					app.globalData.recordTime.endTime = new Date()
					app.td_app_sdk.event({
						id: 'editTime',
						label: '职位编辑时间',
						params: {
							time: app.globalData.recordTime.endTime - app.globalData.recordTime.startTime / 1000,
						}
					});
					wx.navigateTo({
						url: '/pages/publishSuccess/index?isEdit=true'
					})
				} else if (res.data.errCode == 91) {
					wx.showToast({
						title: '您当前发布的信息异常，请重新编辑',
						icon: 'none'
					})
				} else if (res.data.errCode == 99) {
					this.showBuyAccurate()
				} else if (res.data.errCode == 100) {
					let vip = this.selectComponent("#vip");
					vip.openModal(res.data.errMsg, '立即联系', 'ios');
				} else {
					wx.showToast({
						title: res.data.errMsg,
						icon: 'none'
					})
				}
			})
		}
		// 发布新职位
		else {
			param = {
				parttime_job: JSON.stringify(Object.assign(commonParam, param)),
				submit_type: (this.data.selectJobType == 0 ? 2 : 1),
				job_classify_id_list: this.data.jobList,
			}
			postJobInfoApi(param).then(res => {
				console.log(res)
				this.setData({
					clickFlag: false
				})
				if (res.data.errCode == 0) {
					wx.navigateTo({ // 发布成功
						url: '/pages/publishSuccess/index'
					})
				} else if (res.data.errCode == 99) {
					this.showBuyAccurate()
				} else if (res.data.errCode == 100) {
					let vip = this.selectComponent("#vip");
					vip.openModal(res.data.errMsg, '立即联系', 'ios');
				} else {
					wx.showToast({
						title: res.data.errMsg,
						icon: 'none'
					})
				}
			}).catch(err => {})
		}
		// }
	},
	onPullDownRefresh() {
		wx.stopPullDownRefresh();
	},
	onConfirm(event) {
		const {
			picker,
			value,
			index
		} = event.detail;
		Toast(`当前值：${value}, 当前索引：${index}`);
	},
	onCancel() {
		Toast('取消');
	},
	// 城市选择
	citySelect(e) {
		this.onClose()
		queryAccurateResumeNumApi({
			city_id: wx.getStorageSync('current_city').id || ''
		}).then(res => {
			if (res.data.errCode == 99 && this.data.jobList.length > 0) {
				this.setData({
					showIninput: false,
				})
				wx.showModal({
					title: '',
					content: '当前城市可用的精准简历数不足，我们将无法为您提供精准的求职者简历，您设置的精准简历要求将被清除，确认继续切换吗？',
					confirmText: '取消',
					cancelText: '切换城市',
					cancelColor: '#b1b1b1',
					confirmColor: '#00BCD4',
					success(res) {
						if (res.cancel) {
							this.setData({
								cityName: e.detail.city.name,
								cityId: e.detail.city.id,
								subCityName: e.detail.area ? (e.detail.area.name == '不限' ? '全市' : e.detail.area.name) : '全市',
								subCityId: e.detail.area ? e.detail.area.id : '',
								jobList: [],
								jobNameList: [],
							})
						}
					}
				})
			} else {
				this.setData({
					cityName: e.detail.city.name,
					cityId: e.detail.city.id,
					subCityName: e.detail.area ? (e.detail.area.name == '不限' ? '全市' : e.detail.area.name) : '全市',
					subCityId: e.detail.area ? e.detail.area.id : '',
				})
			}
		})

	},
	// 单位
	showUnit() {
		this.setData({
			showUnit: true,
			showIninput: false,
		})
	},
	// 单位选择
	bindUnitChange(e) {
		this.setData({
			unitId: e.detail.id,
			unitName: e.detail.name,
			showUnit: false,
		})
		this.onClose()
	},
	// 联系方式
	showContactType() {
		this.setData({
			showContactType: true,
			showIninput: false,
		})
	},
	// 点击编辑号码库
	eidtNumLibrary(e) {
		console.log(e)
		wx.navigateTo({
			url: '/pages/numLibrary/index' + "?id=" + this.data.contactTypeId,
			// events:{
			// 	 fnFefresh:function(data){
			// 	          console.log(data)
			// 	  }
			// }
		})
	},
	// 联系方式改变
	bindContactTypeChange(e) {
		console.log(e.detail.id)
		console.log(this.data.post_id)
		console.log(app.globalData.userInfo)
		if(this.data.post_id !== ""){
			let params = {
				job_id: app.globalData.publishInfo.job_id,
			}
			showJobContactInfo(params).then(res=>{
				// let concatType = res.data.content.contact_type;
				let data = res.data;
				console.log(res)
				if(e.detail.id == 1){
					this.setData({
						contactNum: data.content.contact_phone_num,
					})
				}if(e.detail.id == 2){
					this.setData({
						contactNum:data.content.wechat_number,
					})
				}if(e.detail.id == 3){
					this.setData({
						contactNum:data.content.wechat_public,
					})
				}if(e.detail.id == 4){
					this.setData({
						contactNum:data.content.qq_number,
					})
				}if(e.detail.id == 5){
					this.setData({
						contactNum:data.content.qq_group,
					})
				}
				// if(e.detail.id == 6){
				// 	this.setData({
				// 		contactNum:data.content.wechat_stock_count == 0?"请添加号码" :"随机微信号："  + data.content.wechat_stock_count + "个",
				// 	})
				// }
				// if(e.detail.id == 7){
				// 	this.setData({
				// 		contactNum:data.content.qq_stock_count == 0?"请添加号码" :"随机QQ号：" + data.content.qq_stock_count + "个",
				// 	})
				// }
				// if(e.detail.id == 8){
				// 	this.setData({
				// 		contactNum:data.content.qq_group_stock_count == 0?"请添加号码" :"随机QQ群号：" + data.content.qq_group_stock_count + "个",
				// 	})
				// }
			})	
		}
		if(this.data.post_id == ""){
			if(e.detail.id == 1){
				console.log("dddd")
				this.setData({
					contactNum:app.globalData.userInfo.contactTel
				})
			} else if(1<e.detail.id<6){
				this.setData({
					contactNum:""
				})
			} 
		}
		if (e.detail.id >= 6) {
			this.setData({
				numIsDisabled: true,
				showNumLibraryBtn: true,
			})
			let params = {
				number_type: e.detail.id - 5,
			}
			findNumberLibraryApi(params).then(res => {
				console.log(res)
				let numLength = res.data.content.number_library_list.length
				if (e.detail.id == 6) {
					this.setData({
						contactNum: numLength == 0 ? "请添加号码" : "随机微信号：" + numLength +
							"个",
					})
				}
				if (e.detail.id == 7) {
					this.setData({
						contactNum: numLength == 0 ? "请添加号码" : "随机QQ号：" + numLength +
							"个",
					})
				}
				if (e.detail.id == 8) {
					this.setData({
						contactNum: numLength == 0 ? "请添加号码" : "随机QQ群号：" + numLength +
							"个",
					})
				}
			})
		} else {
			this.setData({
				numIsDisabled: false,
				showNumLibraryBtn: false,
			})
		}
		this.setData({
			contactTypeId: e.detail.id,
			showContactType: false,
		})
		this.onClose()
	},
	// 经验要求
	showExperience() {
		this.setData({
			showExperience: true,
			showIninput: false,
		})
	},
	// 学历要求
	showEducation() {
		this.setData({
			showEducation: true,
			showIninput: false,
		})
	},
	// // 学历要求
	// bindEducationChange(e) {
	//     this.setData({
	//         educationId: e.detail.id,
	//         educationName: e.detail.name,
	//         showEducation: false,
	//     })
	//     this.onClose()
	// },
	// 结算方式
	showSettlement() {
		this.setData({
			showSettlement: true,
			showIninput: false,
		})
	},
	// 结算方式
	bindSettlementChange(e) {
		this.setData({
			settlementId: e.detail.id,
			settlementName: e.detail.name,
			showSettlement: false,
		})
		this.onClose()
	},
	// 工作时段展示
	jobIntervalShowPopup() {
		let jobIntervalArray = this.data.jobIntervalArray
		let jobIntervalIndex = this.data.jobIntervalIndex
		jobIntervalArray[0].defaultIndex = jobIntervalIndex[0]
		jobIntervalArray[1].defaultIndex = jobIntervalIndex[1]
		this.setData({
			jobIntervalshow: true,
			showIninput: false,
			jobIntervalArray
		});
	},
	//工作时段选择列
	bindjobIntervalColumnChange: function(e) {
		let value = e.detail.value
		let jobIntervalArray = this.data.jobIntervalArray
		let jobIntervalIndex = this.data.jobIntervalIndex
		jobIntervalArray[0].defaultIndex = jobIntervalArray[0].values.indexOf(value[0])
		jobIntervalArray[1].defaultIndex = jobIntervalArray[1].values.indexOf(value[1])
		if ((jobIntervalArray[1].defaultIndex < jobIntervalArray[0].defaultIndex) && e.detail.index == 0)
			jobIntervalArray[1].defaultIndex = jobIntervalArray[0].defaultIndex
		if ((jobIntervalArray[1].defaultIndex < jobIntervalArray[0].defaultIndex) && e.detail.index == 1)
			jobIntervalArray[0].defaultIndex = jobIntervalArray[1].defaultIndex

		//   jobIntervalIndex[0] = jobIntervalArray[0].defaultIndex
		//   jobIntervalIndex[1] = jobIntervalArray[1].defaultIndex
		this.setData({
			//   jobIntervalIndex,
			jobIntervalArray
		})
	},
	// 工作时段选择
	bindjobIntervalChange: function(e) {
		let jobIntervalArray = this.data.jobIntervalArray
		jobIntervalArray[0].defaultIndex = e.detail.index[0]
		jobIntervalArray[1].defaultIndex = e.detail.index[1]
		let t = {}
		t.startTime = jobIntervalArray[0].values[e.detail.index[0]]
		t.endTime = jobIntervalArray[1].values[e.detail.index[1]]
		this.data.workingTimePeriod.push(t)
		this.setData({
			jobIntervalIndex: e.detail.index,
			jobIntervalArray,
			jobIntervalshow: false,
			workingTimePeriod: this.data.workingTimePeriod
		})
		this.onClose()
	},
	//   选择器关闭
	onClose: function() {
		this.setData({
			jobIntervalshow: false,
			showStart: false,
			showEnd: false,
			showContactType: false,
			showWelfare: false,
			showEducation: false,
			showDeadTime: false,
			showUnit: false,
			showSettlement: false,
			showExperience: false,
			showIninput: true,
		})
	},
	// 什么是精准简历
	showAccurate() {
		this.setData({

			showIninput: false,
		})
		modalAccurate = this.selectComponent("#modal_accurate");
		modalAccurate.openModal()
	},
	// 购买精准简历
	showBuyAccurate() {
		this.setData({
			showIninput: false,
		})
		modalAccurateBuy = this.selectComponent("#modal_accurate_buy");
		modalAccurateBuy.openModal()
	},
	//前往选择精准简历
	goToSelectType() {
		queryAccurateResumeNumApi({
			city_id: wx.getStorageSync('current_city').id || ''
		}).then(res => {
			console.log(res)
			if (res.data.errCode == 99) {
				this.setData({
					showIninput: false,
				})
				modalAccurateBuy = this.selectComponent("#modal_accurate_buy");
				modalAccurateBuy.openModal()
			} else if (res.data.errCode == 100) {
				this.setData({
					showIninput: false,
				})
				let vip = this.selectComponent("#vip");
				vip.openModal('套餐内精准简历数不足，您可以联系招聘顾问升级套餐', '立即联系', 'ios');
			} else {
				wx.navigateTo({
					url: '/pages/jobExperience/index?type=onlyJob',
				})
			}
		})
	}
})
