import parseCity from '../../utils/parseCity.js'
import {$getMapInfo} from '../../utils/mapInfo'
import {getCityId,checkAndAddCityInfo} from "../../utils/util";
import {getCityList, getCityInfoApi} from "../../api/index";

let app = getApp()
Page({
    data: {
        searchList: [],
        hotCityList: [],
        groupList: [],
        currentCity: {id: '', name: '选择城市'},
        inputValue: '',
        onSearch: false,
        noShow: true,
        loadingHide: false,
    },
    onLoad(options) {
    },
    onShow(options) {
        this.cityInit()
    },
    //  获取城市展示列表数据
    showCityList(cityList) {
        var cityData = cityList;
        parseCity.init(cityData);
        var hotCityList = parseCity.getHotCities();
        var groupList = parseCity._getGroupList();
        this.setData({
            hotCityList: hotCityList,
            groupList: groupList,
            noShow: false,
            loadingHide: true
        });
    },
    //  搜索框输入事件
    inputEvent(e) {
        var value = e.detail.value;
        if (value.length > 0) {
            var list = parseCity._getMatchCities(value);
            this.setData({
                onSearch: true,
                searchList: list
            })
        } else {
            this.setData({
                onSearch: false
            })
        }
    },
    //  城市链接点击
    goThisCity(e) {
        let id = e.currentTarget.dataset.id;
        let name = e.currentTarget.dataset.name;
        if (name == '选择城市') {
            this.cityInit()
        } else {
            id = parseInt(id)
            wx.setStorageSync('current_city', {id: id, name: name});
            app.globalData.userInfo.cityName = name
            app.globalData.userInfo.cityId = id
            app.globalData.serviceCity = {name: name, id: id}
            checkAndAddCityInfo()
            wx.navigateBack();
        }
    },
    //  城市初始化
    cityInit() {
        wx.getStorage({
            key: 'city_list',
            success: res => {
                console.log(JSON.stringify(res.data))
                let cityList = res.data
                this.showCityList(cityList)
                wx.setStorageSync('city_list', cityList);
                $getMapInfo().then(res => {
                    res = res.address_component
                    if (res) {
                        let c = {name: res.city, id: getCityId(res.city, cityList)}
                        this.setLocationInfo(res,cityList)
                        this.setData({currentCity: c});
                    } else {
                        wx.showToast({
                            title: '获取位置信息失败',
                            icon: 'none'
                        })
                    }
                })
            },
            fail: () => {
                getCityList().then(list => {
                    let cityList = list.data.content.cities
                    this.showCityList(cityList)
                    wx.setStorageSync('city_list', cityList);
                    $getMapInfo().then(res => {
                        res = res.address_component
                        if (res) {
                            let c = {name: res.city, id: getCityId(res.city, cityList)}
                            this.setLocationInfo(res,cityList)
                            this.setData({currentCity: c});
                        } else {
                            wx.showToast({
                                title: '获取位置信息失败',
                                icon: 'none'

                            })
                        }
                    })
                })
            }
        })
    },

    //  设置位置信息
    setLocationInfo(location, cityList) {
        let cityInfo = {}
        if (location && getCityId(location.city, cityList)) {
            cityInfo = {
                name: location.city.replace('市', ''),
                id: getCityId(location.city, cityList)
            }
            app.globalData.serviceCity = cityInfo
        } else {
            cityInfo = {
                name: '选择城市',
                id: ''
            }
            app.globalData.serviceCity = {
                name: '请选择城市',
                id: '-1'
            }
        }
        wx.setStorageSync('locate_city', cityInfo);
        wx.setStorageSync('current_city', cityInfo);
        this.setData({
            currentCity: cityInfo
        });
        checkAndAddCityInfo()
    },
})

