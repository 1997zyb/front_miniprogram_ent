let app = getApp()


Page({
    data: {
        title:'发布成功'
    },
    onLoad(option) {
        if(option&&option.isEdit){
            this.setData({title:'编辑成功'})
        }
    },
    onPullDownRefresh() {
        wx.stopPullDownRefresh();
    },
    gotoIndexPage(){
        app.globalData.jobChange = true
        wx.switchTab({ url: '/pages/index/index' });
    }



})

