let jobSelectNum = 0
let citySelectNum = 0
let app = getApp()
import { getSubscribeConfigApi, updateStuSubscribeConfigApi, getJobClassifyFirstSecondListApi, updateAccurateResumeApi } from '../../api/index'
let modalAccurateBuy={}
let modalLogin = {}
Page({
    data: {
        jobList: [],
        cityList: [],
        cityId: -1,
        showLogin: false,
        type: '',
        onlyOneJobIndex: 0,
        title:'工作经历要求'
    },
    onLoad(option) {
        console.log(option)
        this.setData({
            windowWidth: wx.getSystemInfoSync().windowWidth,
            windowHeight: wx.getSystemInfoSync().windowHeight - app.globalData.navHeight,
            navHeight: app.globalData.navHeight
        })
        if (option.type) {
            this.setData({ type: option.type, jobId: option.jobId || '' })
        }
    },
    onShow() {
        this.getInfo()
    },
    getInfo() {
        this.getJobClassifyList()
        // wx.getStorage({
        //     key: 'current_city',
        //     success: res => {
        //         this.getSubscribeConfig(res.data.id)
        //         this.setData({ cityId: res.data.id })
        //     },
        //     fail: res => {
        //         this.getSubscribeConfig('211')
        //         this.setData({ cityId: '211' })
        //     }
        // });
    },
    // 获取岗位分类
    getJobClassifyList() {
        getJobClassifyFirstSecondListApi({ classify_type :3}).then((res) => {
            let jobList = res.data.content.first_level_job_classify_list
            let jobListSelect = []
            if (this.data.type == 'onlyJob') {
                let page=getCurrentPages()
                let lastPage = page[page.length-2]
                console.log(lastPage.data.jobList)
                let listContent = lastPage.data.jobList.length > 0 ? lastPage.data.jobList : [parseInt(app.globalData.publishInfo.secondGradeId)]
                listContent.forEach(item => {
                    jobListSelect.push(parseInt(item))
                })
            } else {
                jobListSelect = wx.getStorageSync('selectedJobList') || ''
                jobListSelect = jobListSelect=='-1'?[]:jobListSelect.split(',').map(Number)
                wx.removeStorageSync('selectedJobList')
            }
            console.log(jobListSelect)
            let selectJobList = []
            let selectJobNameList = []
            if (this.data.type != 'onlyOneJob') {
                jobList.forEach((item, index) => {
                    item.second_level_job_classify_list.forEach((items, indexs) => {
                        if (jobListSelect.indexOf(parseInt(items.id)) != -1) {
                            selectJobList.push({
                                jobIndex: index,
                                jobIndexs: indexs,
                            })
                            selectJobNameList.push(items.job_classify_name)
                            items.check = true
                        }
                    })
                })
                this.setData({
                    jobList,
                    selectJobList,
                    selectJobNameList
                })
            } else {
                // jobList.push({
                //     first_level_job_classify_name: '其它',
                //     first_level_job_classify_id: '其它',
                //     second_level_job_classify_list: [{
                //         job_classify_name: '其它',
                //         id: '-100',
                //     }]
                // })
                // jobList.forEach((item, index) => {
                //     item.second_level_job_classify_list.forEach((items, indexs) => {
                //         if (items.id==this.data.jobId) {
                //             this.setData({
                //             jobIndex :index,
                //             jobIndexs : indexs
                //             })
                //         }
                //     })
                // })
                console.log(jobList)
                this.setData({
                    jobList
                })
            }
        })
    },
    // 获取并设置基础信息
    getSubscribeConfig(cityId) {
        getSubscribeConfigApi({ city_id: cityId }).then(res => {
            let jobList = res.data.content.job_classifier_list
            let cityList = res.data.content.child_area
            cityList.unshift(res.data.content.city_info)
            let jobSelectedList = res.data.content.job_classify_id_list
            if (this.data.type == 'onlyJob') {
                jobSelectedList = []
                app.globalData.userInfo.subscribe_job_info.subscribe_job_classify_arr.forEach(item => {
                    jobSelectedList.push(item.id)
                })
            } else if (this.data.type == 'onlyOneJob') {
                jobSelectedList = [parseInt(this.data.jobId)]
                console.log(jobSelectedList)
            }
            console.log(jobSelectedList)
            let citySelectedList = res.data.content.address_area_id_list
            jobSelectNum = jobSelectedList.length
            citySelectNum = citySelectedList.length
            jobList.forEach((item, index) => {
                jobSelectedList.forEach(selected => {
                    if (item.job_classfier_id === selected) {
                        item.selected = true
                        this.setData({
                            onlyOneJobIndex: index
                        })
                    }
                })
            })
            if (citySelectNum === 0) {
                cityList[0].selected = true
            }
            cityList.forEach(item => {
                citySelectedList.forEach(selected => {
                    if (item.id === selected) {
                        item.selected = true
                    }
                })
            })
            this.setData({
                jobList: jobList,
                cityList: cityList,
            })
        })
    },
    // 展示职位输入框
    showPopup(index) {
        let onlyOneJobIndex = this.data.onlyOneJobIndex
        let jobList = this.data.jobList
        if (this.data.jobList[index].job_classfier_name != '其它') {
            if (index == onlyOneJobIndex) {
                jobList[index].selected = !jobList[index].selected
            } else {
                jobList[index].selected = true
                jobList[onlyOneJobIndex].selected = false
            }
            this.setData({
                jobList,
                onlyOneJobIndex: index,
                jobTitle: jobList[index].job_classfier_name,
                classifyId: jobList[index].job_classfier_id,
            })
        } else if (this.data.jobList[index].job_classfier_name == '其它' && !jobList[index].selected) {
            this.setData({
                show: true,
                otherIndex: index,
                classifyId: jobList[index].job_classfier_id,
            });
        } else {
            jobList[index].selected = false
            this.setData({
                onlyOneJobIndex: index,
                jobList
            });
        }
    },
    select() {
        let jobSelectList = []
        let jobSelectNameList=[]
        this.data.jobList.forEach(item => {
            item.second_level_job_classify_list.forEach(items => {
                if (items.check) {
                    jobSelectList.push(items.id)
                    jobSelectNameList.push(items.job_classify_name)
                    // jobSelectList.push({
                    //     name: items.job_classify_name,
                    //     id: items.id
                    // })
                }
            })
        })
        let pages=getCurrentPages()
        let lastPages=pages[pages.length-2]
        lastPages.setData({
            jobList: jobSelectList,
            jobNameList: jobSelectNameList
        })
        wx.navigateBack()
    },
    // 关闭职位输入框
    onClose() {
        this.setData({ show: false });
    },
    // 确认职位名称按钮
    choose() {
        let jobList = this.data.jobList
        let onlyOneJobIndex = this.data.onlyOneJobIndex
        let otherIndex = this.data.otherIndex
        if (this.verify()
            // && !jobList[otherIndex].selected
        ) {
            // if (otherIndex == onlyOneJobIndex) {
            //     jobList[otherIndex].selected = !jobList[otherIndex].selected
            // } else {
            //     jobList[otherIndex].selected = true
            //     jobList[onlyOneJobIndex].selected = false
            // }
            this.setData({
                show: false,
                jobList,
                // onlyOneJobIndex: this.data.otherIndex,
                jobTitle: this.data.jobTitle,
            })
        }
    },
    // 校验
    verify() {
        let flag = true
        let toastTitle = ''
        if (!this.data.jobTitle) {
            toastTitle = '请输入职位名称'
            flag = false
        } else if (this.data.jobTitle.length < 2 || this.data.jobTitle.length > 10) {
            toastTitle = '职位名称必须为2-10个字'
            flag = false
        }
        if (!flag)
            wx.showToast({
                title: toastTitle,
                icon: 'none'
            })
        return flag
    },
    // 输入职位名称
    inputJobTitle(e) {
        console.log(e)
        this.setData({ jobTitle: e.detail.value })
    },
    // 职位选择
    jobClick(e) {
        let id = e.target.dataset.id
        let index = e.target.dataset.index
        if (this.data.type == 'onlyOneJob') {
            this.showPopup(index)
            return
        }
        for (let i = 0; i < this.data.jobList.length; i++) {
            if (id === this.data.jobList[i].job_classfier_id) {
                if (this.data.jobList[i].selected) {
                    this.data.jobList[i].selected = false
                    jobSelectNum--
                } else if (jobSelectNum < 5) {
                    this.data.jobList[i].selected = true
                    jobSelectNum++
                } else if (jobSelectNum === 5) {
                    wx.showToast({
                        title: '最多可选5个',
                        icon: 'none'

                    })
                }
                this.setData({ jobList: this.data.jobList })
                return
            }
        }
    },
    // 首页意向设置保存
    save() {
        let citySelectList = []
        let jobSelectList = []
        this.data.cityList.forEach(item => {
            if (item.selected) {
                // console.log(item)
                citySelectList.push(item.id)
            }
        })
        this.data.jobList.forEach(item => {
            item.second_level_job_classify_list.forEach(items => {
                if (items.check) {
                    jobSelectList.push(items.id)
                }
            })
        })
        if (citySelectList[0] == this.data.cityId) {
            citySelectList = ''
        }
        let param = {
            job_classify_id_list: jobSelectList,
            job_id: wx.getStorageSync('sendJobId')
        }
        updateAccurateResumeApi(param).then(res => {
            if(res.data.errCode==100){
                let vip = this.selectComponent("#vip");
                vip.openModal('套餐内精准简历数不足，您可以联系招聘顾问升级套餐', '立即联系', 'ios');
            }else{
                wx.showToast({
                    title: '修改成功',
                    success: () => {
                        setTimeout(() => {
                            let page = getCurrentPages()
                            page[page.length - 2].init()
                            page[page.length - 2].getTabInfo()
                            // console.log(page)
                            // if (page[page.length - 2].getInfo())
                            //     page[page.length - 2].getInfo()
                            wx.navigateBack()
                        }, 500)
                    }
                })
            }
        })
    },
    //  打开登录弹窗
    openLogin() {
        this.setData({ showLogin: true })
        modalLogin = this.selectComponent("#modal_login");
        modalLogin.openModal();
    },
    //  关闭登录弹窗
    closeLogin() {
        this.setData({ showLogin: false })
        modalLogin = this.selectComponent("#modal_login");
        modalLogin.closeModal();
    },
    // 选择工作
    selectJob(e) {
        e = e.detail
        let flag = false
        e.selectJobNameList.forEach(item => {
            if (item.length >= 5)
                flag = true
        })
        this.setData({
            jobList: e.selectJob,
            selectJobList: e.selectJobList,
            selectJobNameList: e.selectJobNameList,
            flag
        })
    },
    // 取消点击
    cancelClick(e) {
        let name = e.currentTarget.dataset.name
        this.cancelJob(name)
    },
    //取消选择
    cancelJob(name) {
        let selectJobNameList = this.data.selectJobNameList
        let selectJobList = this.data.selectJobList
        let selectJob = this.data.jobList
        let indexOf = selectJobNameList.indexOf(name)
        let index = selectJobList[indexOf].jobIndex
        let indexs = selectJobList[indexOf].jobIndexs
        selectJob[index].second_level_job_classify_list[indexs].check = false
        selectJobNameList.splice(indexOf, 1)
        selectJobList.splice(indexOf, 1)
        this.setData({
            selectJobNameList,
            selectJobList,
            jobList: selectJob
        })
    },


    // 输入框失去焦点事件
    inputBlur(e) {
        this.setData({
            jobTitle: e.detail.value
        })
    }
})