import {
  logoutApi,
  getUserInfoApi,
  getVerifyInfoApi,
  getVipUseInfoApi,
  postUserInfoApi,
  getClaimUserTelphoneApi
} from '../../api/index'
import {
  getResume,
  parseTime,
  checkLogin
} from '../../utils/util'

let app = getApp();
let modalLogin = {}
Page({
  data: {
    userName: "",
    avatarUrl: "",
    isLogin: false,
    identificationPerson: '',
    identificationCompany: '',
    showLogin: false,
    showFlag: true,
    env: app.globalData.domain == 'https://m.jianke.cc' ? 'pro' : 'dev',
    vipName: '',
    vipTime: '',
    helpUrl: 'https://m.jianke.cc/wap/toHelpCenterPage',
    contractName: '',
    contractNum: '',
    collectNum: 0,
    fansNum: 0


  },
  onShow(option) {
    if (wx.getStorageSync('is_login')) {
      this.getInfo()
      if (this.data.showLogin) {
        this.closeLogin()
      }
    } else {
      this.initInfo()
    }
  },
  //  跳转个人/企业认证
  gotoIdentificationPage(e) {
    this.closeIdentificationModal()
    if (e.currentTarget.dataset.type == 1) {
      if (this.data.identificationPerson == 1 || this.data.identificationPerson == 4) {
        wx.navigateTo({
          url: '/pages/identification/index?type=1'
        })
      } else if (this.data.identificationPerson == 2) {
        wx.showToast({
          title: '您的个人身份正在认证中',
          icon: 'none'
        })
      } else if (this.data.identificationPerson == 3) {
        wx.showToast({
          title: '您的个人身份已认证,如需修改请联系客服',
          icon: 'none'
        })
      }
    } else {
      if (this.data.identificationCompany == 1 || this.data.identificationCompany == 4 || this.data.identificationCompany == 2) {
        wx.navigateTo({
          url: '/pages/identification/index?type=2'
        })
        // } else if (this.data.identificationCompany == 2) {
        //   wx.showToast({
        //     title: '您的企业认证正在认证中',
        //     icon: 'none'
        //   })
      } else if (this.data.identificationCompany == 3) {
        queryEnterpriseVerifyQueryJobApi().then(res => {
          console.log(res)
          if (res.data.errCode == 0) {

            wx.navigateTo({
              url: '/pages/identification/index?type=2'
            })
          } else {
            wx.showToast({
              icon: 'none',
              title: res.data.errMsg,
            })
          }
        })
      }
      // } else if (this.data.identificationCompany == 3) {
      //     wx.showToast({
      //         title: '您的企业已认证,如需修改请联系客服',
      //         icon: 'none'
      //     })
    }

  },
  //  获取页面数据
  getInfo() {
    this.setData({
      isLogin: wx.getStorageSync('is_login'),
      userName: app.globalData.userInfo.name ? app.globalData.userInfo.name : '立即登录 >',
    });
    getVerifyInfoApi().then(res => {
      let info = res.data.content.account_info
      this.setData({
        identificationPerson: info.id_card_verify_status,
        identificationCompany: info.verifiy_status
      })
      getResume().then(res => {
        this.initInfo()
      })
    })
    getVipUseInfoApi().then(res => {
      let name = res.data.content ? res.data.content.city_vip_info[0].vip_package_name : ''
      let time = res.data.content ? parseTime(res.data.content.city_vip_info[0].vip_dead_time, '{y}-{m}-{d}') : ''
      this.setData({
        vipName: name,
        vipTime: time
      })
    })
    getClaimUserTelphoneApi().then(res => {
      this.setData({
        contractName: res.data.content.contractName ? res.data.content.contractName.substring(0, 1) + '经理' : '',
        contractNum: res.data.content.contractPhone
      })
    })

  },
  //  页面信息初始化
  initInfo() {
    let userInfo = wx.getStorageSync('user_globalData')
    let isUpLoad = false
    if (userInfo.profileUrl && userInfo.profileUrl.indexOf("Upload") != -1) {
      isUpLoad = true
    }
    this.setData({
      identification: userInfo.id_card_verify_status ? userInfo.id_card_verify_status : '0',
      isLogin: wx.getStorageSync('is_login'),
      userName: userInfo.trueName ? userInfo.trueName : '立即登录 >',
      avatarUrl: userInfo.profileUrl ? userInfo.profileUrl : '../../images/logo_white.png',
      complete: userInfo.complete ? userInfo.complete : '--',
      isUpLoad: isUpLoad,
      collectNum: userInfo.collect_num,
      fansNum: userInfo.focus_num,
    });
  },
  //  打开登录弹窗
  openLogin() {
    this.setData({
      showLogin: true
    })
    modalLogin = this.selectComponent("#modal_login");
    modalLogin.openModal();
  },
  //  关闭登录弹窗
  closeLogin() {
    this.setData({
      showLogin: false
    })
    modalLogin = this.selectComponent("#modal_login");
    modalLogin.closeModal();
  },
  // 跳转兼客招聘小程序
  gotoJiankeMiniprogram() {
    let env = 'trial'
    if (app.globalData.domain == 'https://m.jianke.cc') {
      env = 'release'
    }
    wx.navigateToMiniProgram({
      appId: 'wxd3cd9077dc98d7f6',
      path: 'pages/index/index',
      envVersion: env,
      success(res) {
        // 打开成功
      }
    })
  },
  //  打开弹窗
  openIdentificationModal() {
    if (wx.getStorageSync('is_login')) {
      this.setData({
        showFlag: false
      })
    } else {
      this.openLogin()
    }

  },
  //  关闭弹窗
  closeIdentificationModal() {
    this.setData({
      showFlag: true
    })
  },
  //  退出登录
  loginOut() {
    wx.showModal({
      title: '',
      content: '确定要退出登录吗？',
      cancelColor: '#b1b1b1',
      confirmColor: '#00BCD4',
      success: (res) => {
        if (res.confirm) {
          logoutApi().then(res => {
            if (res.data.errCode == 0) {
              wx.removeStorageSync('user_globalData')
              wx.setStorageSync('is_login', false)
              wx.setStorageSync('im_connect', false)
              this.setData({
                isLogin: wx.getStorageSync('is_login'),
                userName: '立即登录 >',
              });
            }
          })
        } else {}
      }
    })
  },
  //  跳转用户信息页面
  gotoUserInfoPage() {
    if (wx.getStorageSync('is_login')) {
      wx.navigateTo({
        url: '/pages/personalInformation/index'
      })
    } else {
      this.openLogin()
    }

  },
  //  跳转购买VIP页面
  gotoBuyVipPage() {
    if (wx.getStorageSync('is_login')) {
      wx.navigateTo({
        url: '/pages/buyVip/index'
      })
    } else {
      this.openLogin()
    }

  },
  //跳转至vip详情界面
  gotoVipInfoPage() {
    if (wx.getStorageSync('is_login')) {
      wx.navigateTo({
        url: '/pages/vipInfo/index'
      })
    } else {
      this.openLogin()
    }
  },
  gotoLoginPage() {
    logoutApi().then(res => {
      if (res.data.errCode == 0) {
        wx.removeStorageSync('user_globalData')
        wx.setStorageSync('is_login', false)
        this.initInfo()
        // createSession()
      }
    })
    wx.navigateTo({
      url: '/pages/login/index'
    })
  },
  //  跳转外链新手帮助页面
  gotoWebViewPage(e) {
    wx.navigateTo({
      url: '/pages/webView/index?url=' + this.data.helpUrl
    })
  },
  //  跳转设置页面
  gotoSettingPage() {
    wx.navigateTo({
      url: '/pages/setting/index'
    })
  },
  // 联系
  contact() {
    if (!wx.getStorageSync('is_login')) {
      this.openLogin()
      return
    }
    let value = this.data.contractNum;
    wx.makePhoneCall({
      phoneNumber: value,
      success: function() {},
      fail: function() {}
    })
  },
  //  上传头像
  upLoadImage() {
    checkLogin(this, () => {
      wx.chooseImage({
        count: 1, //选择的图片数量
        success: (res) => {
          wx.showLoading({
            title: '正在加载中....',
            mask: true,
          })
          const tempFilePaths = res.tempFilePaths
          wx.uploadFile({
            url: app.globalData.domain + app.globalData.upLoadUrl, //服务端地址
            filePath: tempFilePaths[0],
            name: 'file', //图片的key
            formData: {}, //除了图片的其他参数
            success: (res) => { //请求成功回调 这里收到的是服务端下发 的数据
              var data = res.data
              if (typeof data == 'string') {
                data = JSON.parse(data)
              }
              // 获取图片url
              this.setData({
                avatarUrl: data.content.fileUrl,
                isUpLoad: true
              })
              wx.hideLoading();
              this.save(data.content.fileUrl);
            }
          })
        }
      })
    })
  },
  //  保存
  save(image_url) {
    let userInfo = app.globalData.userInfo
    let param = {
      true_name: wx.getStorageSync('user_globalData').trueName,
      city_id: wx.getStorageSync('user_globalData').bean.cityId,
      industry_id: wx.getStorageSync('user_globalData').bean.industryId,
      profile_url: image_url
    }
    postUserInfoApi(param).then(res => {
      setTimeout(() => {
        wx.showToast({
          title: '保存成功',
          success: () => {}
        })
      }, 500)

    })

  },
  //  跳转收藏列表页面
  gotoCollectListPage() {
    wx.navigateTo({
      url: '/pages/collectList/index'
    })
  },
  //  跳转粉丝列表页面
  gotoFansListPage() {
    wx.navigateTo({
      url: '/pages/fansList/index'
    })
  }

})