import {
    postUserInfoApi,
    getUserInfoApi,
    getIndustyList,
    uploadCompanyPhotoApi,
    deleteCompanyPhotoApi,
    getEnterpriseShowNameEnum
} from '../../api/index'

let app = getApp()

Page({
    data: {
        navHeight: app.globalData.navHeight,
        solid: false,
        showCity: false,
        showCity2: false,
        cityType:2,// 2:招聘城市 3:工作地区
        name: '',
        cityName: '',
        cityId: '',
        avatarUrl: '',
        companyName: '',
        companyShortName: '',
        companyDesc: '',
        industryName: '',
        industryId: '',
        industryDesc: '',
        showIndustrySesc: false,
        industryList: [],
        otherName: '',
        isUpLoad: false,
        photoList: [],
        companyCityId: '',
        companyCityName: '',
        companySubCityId: '',
        companySubCityName: '',
        companyAddress: '',
        textHieght: 400,
        showNameList: [
            {
                id: 1,
                name: '姓名'
            },
            {
                id: 2,
                name: '先生',
            },
            {
                id: 3,
                name: '女士'
            }
        ],
        showNameIndex: 0,
        showName: '',
        showNameId: '',
    },
    onLoad(option) {
        this.initUserInfo()
        getIndustyList().then(res => {
            let list = res.data.content.industyList
            let nlist = []
            list.forEach(item => {
                let arr = {id: item.industry_id, name: item.industry_name}
                nlist.push(arr)
            })
            this.setData({industryList: nlist})
        })
        getEnterpriseShowNameEnum().then(res => {
            let data = res.data.content.showNameTypeEnum
            let arr = []
            data.forEach(e => {
                let obj = { id: e.value, name: e.label }
                arr.push(obj)
            })
            this.setData({showNameList: arr})
        })
    },
    onShow() {
        // this.setData({
        //     cityName: app.globalData.userInfo.cityName ? app.globalData.userInfo.cityName : '',
        //     cityId: app.globalData.userInfo.cityId ? app.globalData.userInfo.cityId : '',
        //     companyCityId: app.globalData.comPanyInfo.cityId ? app.globalData.comPanyInfo.cityId : '',
        //     companyCityName: app.globalData.comPanyInfo.cityName ? app.globalData.comPanyInfo.cityName : '',
        //     companySubCityId: app.globalData.comPanyInfo.subCityId ? app.globalData.comPanyInfo.subCityId : '',
        //     companySubCityName: app.globalData.comPanyInfo.subCityName ? app.globalData.comPanyInfo.subCityName : '',
        // })
    },

    //  初始化字段
    initUserInfo() {
        getUserInfoApi().then(res => {
            let resume = res.data.content.bean
            app.globalData.userInfo = res.data.content.bean
            app.globalData.userInfo.name = res.data.content.trueName
            app.globalData.userInfo.complete = res.data.content.trueName && app.globalData.userInfo.industryId && app.globalData.userInfo.cityId
            this.setData({
                name: app.globalData.userInfo.name ? app.globalData.userInfo.name : '',
                cityName: app.globalData.userInfo.cityName ? app.globalData.userInfo.cityName : '',
                cityId: app.globalData.userInfo.cityId ? app.globalData.userInfo.cityId : '',
                companyName: app.globalData.userInfo.enterpriseName ? app.globalData.userInfo.enterpriseName : '',
                companyShortName: app.globalData.userInfo.entShortName ? app.globalData.userInfo.entShortName : '',
                companyDesc: app.globalData.userInfo.desc ? app.globalData.userInfo.desc : '',
                industryName: app.globalData.userInfo.industryName ? app.globalData.userInfo.industryName : '',
                industryId: app.globalData.userInfo.industryId ? app.globalData.userInfo.industryId : '',
                industryDesc: app.globalData.userInfo.industryDesc ? app.globalData.userInfo.industryDesc : '',
                isUpLoad: app.globalData.userInfo.profile_url && app.globalData.userInfo.profile_url.indexOf("Upload") != -1 ? true : false,
                photoList: res.data.content.live_photo,
                showIndustrySesc: app.globalData.userInfo.industryId == 41 ? true : false,
                companyCityId: resume.companyCityId ? resume.companyCityId : '',
                companyCityName: res.data.content.company_city_name ? res.data.content.company_city_name : '',
                companySubCityId: resume.companyAreaId ? resume.companyAreaId : '',
                companySubCityName: res.data.content.company_area_name ? res.data.content.company_area_name : '',
                companyAddress: res.data.content.company_address ? res.data.content.company_address : '',
                showName: app.globalData.userInfo.showNameTypeName ? app.globalData.userInfo.showNameTypeName : '',
                showNameId: app.globalData.userInfo.showNameType ? app.globalData.userInfo.showNameType : '',
            })
        }).catch(e => {
        })
    },
    //  名字改变事件
    nameChange(e) {
        this.setData({name: e.detail.value})
    },
    //  跳转城市区域选择页面
    gotoCitySelectPage() {
        this.setData({cityType:2,showCity:true})
        // wx.navigateTo({
        //     url: '/pages/citySelect/index?isFromResum=true'
        // })
    },
    gotoCitySelectPage2() {
        this.setData({cityType:3,showCity2:true})
    },
    //  涉及行业改变事件
    bindIndustryChange(e) {
        if (this.data.industryList[e.detail.value].id == 41) {
            this.setData({showIndustrySesc: true})
        } else {
            this.setData({industryDesc: ''})
            this.setData({showIndustrySesc: false})
        }
        this.setData(
            {
                industryId: this.data.industryList[e.detail.value].id,
                industryName: this.data.industryList[e.detail.value].name
            },
        )

    },
    // 其他行业的名称
    bindIndustrySescChange(e) {
        this.setData({otherName: e.detail.value})
    },
    //  公司名称填写
    bindCompanyNameChange(e) {
        this.setData({companyName: e.detail.value})
    },
    //  公司名称填写
    bindCompanyShortNameChange(e) {
        this.setData({companyShortName: e.detail.value})
    },
    //  公司简介填写
    bindCompanyDescChange(e) {
        if (e.detail.value.length > 150) {
            this.setData({textHieght: 1000})
        }
        this.setData({companyDesc: e.detail.value})
    },
    //  公司地址填写
    companyAddressChange(e) {
        this.setData({companyAddress: e.detail.value})
    },
    getLocation() {
        wx.chooseLocation({
            success: (data) => {
                this.setData({companyAddress: data.name})
            }
        })
    },
    //  上传公司照片
    upCompanyLoadImage() {
        wx.chooseImage({
            count: 1,//选择的图片数量
            success: (res) => {
                wx.showLoading({
                    title: '正在加载中....',
                    mask: true,
                })
                const tempFilePaths = res.tempFilePaths
                wx.uploadFile({
                    url: app.globalData.domain + app.globalData.upLoadUrl, //服务端地址
                    filePath: tempFilePaths[0],
                    name: 'file',//图片的key
                    formData: {},//除了图片的其他参数
                    success: (res) => {//请求成功回调 这里收到的是服务端下发 的数据
                        var data = res.data
                        if (typeof data == 'string') {
                            data = JSON.parse(data)
                        }
                        wx.hideLoading();
                        uploadCompanyPhotoApi({photo_url: data.content.fileUrl}).then(res => {
                            if (res.data.errCode == 0) {
                                this.data.photoList.push({
                                    id: res.data.content.life_photo_id,
                                    live_photo: data.content.fileUrl
                                })
                                this.setData({photoList: this.data.photoList})
                            } else {
                                wx.showToast({
                                    title: res.data.errMsg,
                                    icon: 'none'

                                })
                            }


                        })
                    }
                })
            }
        })
    },
    // 预览照片
    previewImageLivePhoto(e) {
        let list = []
        this.data.photoList.forEach(item => {
            list.push(item.live_photo)
        })
        var current = e.currentTarget.dataset.src;
        wx.previewImage({
            current: current, // 当前显示图片的http链接
            urls: list // 需要预览的图片http链接列表
        })
    },
    // 删除公司照片
    deletePhoto(e) {
        deleteCompanyPhotoApi({live_photo_id: e.currentTarget.dataset.id}).then(res => {
            if (res.data.errCode == 0) {
                this.data.photoList.splice(e.currentTarget.dataset.index, 1)
                this.setData({photoList: this.data.photoList})
            }
        })
    },
    //  校验必填项
    verify() {
        let NAME = /^([A-Za-z]|[\u4E00-\u9FA5]){2,10}$/
        let ENTNAME = /^([A-Za-z]|[\u4E00-\u9FA5]){2,32}$/
        let flag = true
        let toastTitle = ''
        if (!this.data.name) {
            toastTitle = '请输入姓名'
            flag = false
        } else if (!NAME.test(this.data.name)) {
            toastTitle = '请输入正确的名字'
            flag = false
        } else if (!this.data.cityId || !this.data.cityName) {
            toastTitle = '请选择招聘城市'
            flag = false
        } else if (!this.data.industryId) {
            toastTitle = '请选择涉及行业'
            flag = false
        } else if (this.data.industryId == 41 && (!this.data.industryDesc && !this.data.otherName)) {
            toastTitle = '请填写行业名称'
            flag = false
        } else if (!ENTNAME.test(this.data.companyName)) {
            toastTitle = '请输入2~32字的公司名称'
            flag = false
        }
        if (!flag) {
            wx.showToast({
                title: toastTitle,
                icon: 'none'
            })
        }
        return flag
    },
    //  保存
    save() {
        if (this.verify()) {
            let param = {
                true_name: this.data.name,
                city_id: this.data.cityId,
                industry_id: this.data.industryId,
                industry_desc: this.data.otherName ? this.data.otherName : this.data.industryName,
                enterprise_name: this.data.companyName,
                ent_short_name: this.data.companyShortName,
                desc: this.data.companyDesc,
                company_city_id: this.data.companyCityId,
                company_area_id: this.data.companySubCityId,
                company_address: this.data.companyAddress,
                show_name_type: this.data.showNameId,

            }
            postUserInfoApi(param).then(res => {
                if (res.data.errCode == 0) {
                    this.initUserInfo()
                    setTimeout(() => {
                        wx.showToast({
                            title: '保存成功',
                            success: () => {
                                setTimeout(() => {
                                    wx.navigateBack()
                                }, 1000)
                            }

                        })
                    }, 500)
                } else {
                    wx.showToast({
                        title: res.data.errMsg,
                        icon: 'none'
                    })
                }


            })
        }

    },
    // 城市选择
    citySelect(e) {
        console.log(e)
        if(this.data.cityType==2){
            this.setData({
                cityName:e.detail.city.name,
                cityId:e.detail.city.id
            })
        }else {
            this.setData({
                companyCityId: e.detail.city.id,
                companyCityName: e.detail.city.name,
                companySubCityId: e.detail.area?e.detail.area.id:'',
                companySubCityName: e.detail.area?e.detail.area.name:'',
            })
        }
        this.setData({
            showCity: false,
            showCity2: false,
        })

    },

    // 关闭选择器
    onClose(){
        this.setData({
            showCity:false,
            showCity2:false,
        })
    },

    // 选择展示姓名回调
    bindShowNameChange(e){
        let index = parseInt(e.detail.value)
        this.setData({
            showName: this.data.showNameList[index].name,
            showNameId: this.data.showNameList[index].id
        })
    }
})
