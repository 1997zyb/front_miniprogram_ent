let app = getApp();
Page({

    data: {},

    onLoad: function(options) {
        this.setData({
            version: app.globalData.version
        })
    },
    onShow: function() {

    },
    //   用户协议
    gotoServiceAgreementPage() {
      let url = encodeURIComponent(app.globalData.domain + '/wap/userAgreementJkzp')
        wx.navigateTo({
            url: '/pages/webView/index?url=' + url
        })
    },
    // 隐私政策
    gotoPrivacyAgreementPage() {
      let url = encodeURIComponent(app.globalData.domain + '/wap/privacyPolicyJkzp')
        wx.navigateTo({
            url: '/pages/webView/index?url=' + url
        })
    },

    // 查看证件信息
    gotoCredentials(){
        let url = encodeURIComponent(app.globalData.domain+'/m/toLicenseUrlPage')
        wx.navigateTo({
            url: '/pages/webView/index?url=' + url
        })
    }
})
