let app = getApp()
import {
    getCityInfoApi,
    getJobClassifyFirstSecondListApi,
    getInfoByAccountIdApi,
    getTalentListApi,
    collectTalentApi,
    cancelCollectTalentApi,
    getTalentContactApi, getEnumApi
} from '../../api/index'
var modalLogin={}
Page({
    data: {
        navHeight: app.globalData.navHeight,
        title: '人才',
        cityId: '',
        subCityId: '',
        jobClassifyId: '',
        experienceId: '',
        ageId: '',
        loading: true,
        noData: true,
        talentList: [],
        currentTalent: null,
        noMoreData: false,
        pageNum: 1,
        pageSize: 10,
        telephone: '',
        firstNavType: '',
        firstNavList: [{name: '全部区域', id: 'area'}, {name: '职位不限', id: 'job'}, {
            name: '经验不限',
            id: 'experience'
        }, {name: '年龄不限', id: 'age'}],
        subCityList: [],
        JobClassifyList: [],
        experienceList: [{id: 0, name: '不限'}, {id: 1, name: '3-5年'}, {id: 2, name: '5-8年'}],
        ageList: [{id: 0, name: '年龄不限'}, {id: 2, name: '18-24岁'}, {id: 3, name: '25-29岁'}, {
            id: 4,
            name: '30-39岁'
        }, {id: 5, name: '40-49岁'}, {id: 6, name: '50岁以上'}],
        showSubCity: false,
        showJob: false,
        showExperience: false,
        showAge: false,
        hide:false,
        loadPage:true,
    },
    onLoad(option) {
        getEnumApi().then(res => {
            let emus = res.data.content.enumList
            emus.listWorkExperienceEnum.unshift({id: '', name: '经验不限'})
            emus.listJobApplyAgeLimitEnum.unshift({id: '', name: '年龄不限'})
            this.setData({
                experienceList: emus.listWorkExperienceEnum,
                cityId: app.globalData.serviceCity.id
            })
        })
        // this.getTalentList()
    },
    onShow() {
        if (app.globalData.serviceCity.id != this.data.cityId || this.data.loadPage) {
            this.setData({
                loadPage:false
            })
        this.initData()
        this.setData({ cityId: app.globalData.serviceCity.id })
            this.getTalentList()
        }
    },
    onUnload() {
    },
    initData(){
        this.setData({
            subCityId: '',
            jobClassifyId: '',
            experienceId: '',
            ageId: '',
            loading: true,
            noData: true,
            talentList: [],
            currentTalent: null,
            noMoreData: false,
            pageNum: 1,
            pageSize: 10,
            telephone: '',
            firstNavList: [{name: '全部区域', id: 'area'}, {name: '职位不限', id: 'job'}, {
                name: '经验不限',
                id: 'experience'
            }, {name: '年龄不限', id: 'age'}],
            subCityList: [],
            JobClassifyList: [],
            firstNavType: '',
            showSubCity: false,
            showJob: false,
            showExperience: false,
            showAge: false,
        })
    },
    onPullDownRefresh() {
        wx.stopPullDownRefresh();
    },
    //  下拉加载
    onReachBottom() {
        if (!this.data.noMoreData) {
            this.data.pageNum++
            this.getTalentList()
        }
    },
    //  初始化搜索条件
    initCondition() {
        this.setData({
            pageNum: 1,
            noData: false,
            noMoreData: false,
            talentList: [],
            currentTalent: null,
            telephone: '',
            hide:false
        })
    },
    // 导航搜索一级菜单点击事件
    fistNavClick(e) {
        // 城市筛选
        if (e.currentTarget.dataset.type == 'area') {
            this.setData({showJob: false, showExperience: false, showAge: false})
            getCityInfoApi({id: this.data.cityId}).then(res => {
                if (res.data.errCode == 0) {
                    res.data.content.child_area.unshift({id: '', name: '全部区域'})
                    this.setData({
                        subCityList: res.data.content.child_area,
                        showSubCity: !this.data.showSubCity
                    })
                } else {
                    wx.showToast({
                        title: '城市信息不存在',
                        icon: 'none'
                    })

                }

                this.sethide(e)

            })
        }
        // 职位筛选
        else if (e.currentTarget.dataset.type == 'job') {
            this.setData({showSubCity: false, showExperience: false, showAge: false})
            getJobClassifyFirstSecondListApi({use_type:1}).then(res => {
                res.data.content.first_level_job_classify_list.unshift({
                    "first_level_job_classify_id": '',
                    "first_level_job_classify_name": "职位不限",
                    "second_level_job_classify_list": [
                        {"id": "", "job_classify_name": "职位不限",}
                    ],
                })
                this.setData({showJob: !this.data.showJob})
                this.setData({JobClassifyList: res.data.content.first_level_job_classify_list})
                this.sethide(e)
            })

        }
        // 经验筛选
        else if (e.currentTarget.dataset.type == 'experience') {
            this.setData({showSubCity: false, showJob: false, showAge: false})
            this.setData({showExperience: !this.data.showExperience})
            this.sethide(e)
        }
        // 年龄筛选
        else if (e.currentTarget.dataset.type == 'age') {
            this.setData({showSubCity: false, showJob: false, showExperience: false})
            this.setData({showAge: !this.data.showAge})
            this.sethide(e)
        }else{
            this.sethide(e)
        }
    },
    sethide(e){
        this.setData({ firstNavType: e.currentTarget.dataset.type, hide: (this.data.showSubCity || this.data.showJob ||this.data.showExperience || this.data.showAge) })
    },
    // 二级城市点击事件
    subCityClick(e) {
        this.setData({
            subCityId: e.currentTarget.dataset.id,
            showSubCity: false,
            'firstNavList[0].name': e.currentTarget.dataset.name
        })
        this.initCondition()
        this.getTalentList()
    },
    // 职位点击事件
    jobClick(e) {
        this.setData({jobClassifyId: e.detail.job, showJob: false, 'firstNavList[1].name': e.detail.name})
        this.initCondition()
        this.getTalentList()
    },
    // 经验点击事件
    experienceClick(e) {
        this.setData({
            showExperience: false,
            experienceId: e.currentTarget.dataset.id,
            'firstNavList[2].name': e.currentTarget.dataset.name
        })
        this.initCondition()
        this.getTalentList()
    },
    // 年龄点击事件
    ageClick(e) {
        this.setData({
            showAge: false,
            ageId: e.currentTarget.dataset.id,
            'firstNavList[3].name': e.currentTarget.dataset.name
        })
        this.initCondition()
        this.getTalentList()
    },
    // 获取人才信息
    getTalentList() {
        let param = {
            page_size: this.data.pageSize,
            page_num: this.data.pageNum,
            city_id: this.data.cityId?this.data.cityId:'-1',
            list_type: 1,
            address_area_id: this.data.subCityId,
            job_classify_id: this.data.jobClassifyId,
            work_exp: this.data.experienceId,
            age_limit: this.data.ageId
        }
        getTalentListApi(param).then(res => {
            this.data.talentList = this.data.talentList.concat(res.data.content.telent_list)
            if (this.data.talentList.length == 0) {
                this.setData({noData: true})
            } else {
                this.setData({noData: false})
            }
            if (res.data.content.telent_list.length < this.data.pageSize) {
                this.setData({noMoreData: true})
            }
            this.setData({loading: false})
            this.setData({talentList: this.data.talentList})
        })
    },
    // 关闭筛选框
    closeNav() {
        this.setData({
            showSubCity: false,
            showJob: false,
            showExperience: false,
            showAge: false,
            hide:false
        })

    },
    changTalent(e) {
        this.data.talentList[e.detail].is_collect == 1 ? this.data.talentList[e.detail].is_collect = 0 : this.data.talentList[e.detail].is_collect = 1
        this.setData({talentList: this.data.talentList})
    },

    //  打开登录弹窗
    openLogin() {
        this.setData({ showLogin: true })
        modalLogin = this.selectComponent("#modal_login");
        modalLogin.openModal();
    },
    //  关闭登录弹窗
    closeLogin() {
        modalLogin = this.selectComponent("#modal_login");
        modalLogin.closeModal();
        this.setData({ showLogin: false })
    },
})

