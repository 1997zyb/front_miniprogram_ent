import {
  getVerifyInfoApi,
  postVerifyInfoApi,
  postEnterpriseVerifyInfoApi,
  uploadPhotoApi
} from '../../api/index'
import {
  $ifLogin
} from '../../utils/login'

var app = getApp();
let modalLogin = {}
let pageOption = {}
Page({
  data: {
    title: '证件',
    frontPicDefault: '../../images/credentials/front.png', //身份证正面图片 默认
    backPicDefault: '../../images/credentials/back.png', //身份证反面图片 默认
    handPicDefault: '../../images/credentials/hand.png', //身份证手持图片 默认
    businessLicense: '../../images/credentials/business_license.png', //身份证手持图片 默认
    name: '', //姓名
    idNum: '', //身份证
    frontPic: '', //身份证正面图片
    backPic: '', //身份证反面图片
    handPic: '', //身份证手持图片
    showLogin: false,
    type: 0, //当前证件类型  1 实名 2 学生 3健康
    enterpriseName: '',
    registNum: '',
    businessLicenceUrl: '',
    extraVerifyPicUrl: '',
    agreement: true,
  },
  onLoad: function(option) {
    pageOption = option
    $ifLogin()

  },
  getInfo() {
    //  获取证件类型
    let type = ''
    if (pageOption.type) {
      type = pageOption.type
      this.setData({
        title: type == '1' ? '个人认证' : '企业认证'
      })
    }
    wx.setNavigationBarTitle({
      title: this.data.title
    })
    this.setData({
      type: type
    });
    if (type == 1) {
      this.getPersonData()
    } else if (type == 2) {
      this.getCompanyData()
    }
  },
  //  获取个人认证信息
  getPersonData() {
    let params = {}
    getVerifyInfoApi(params).then(res => {
      let data = res.data.content.last_id_card_verify_info
      this.setData({
        name: data.true_name,
        idNum: data.id_card_no,
        frontPic: data.id_card_url_front,
        backPic: data.id_card_url_back,
        handPic: data.id_card_url_third,
      });
    })
  },
  // 输入事件
  inputEvent_name: function(e) {
    this.setData({
      name: e.detail.value,
    });
  },
  inputEvent_idNum: function(e) {
    this.setData({
      idNum: e.detail.value,
    });
  },
  getImgUrl(url, type) {
    // 有传type 表示是实名认证。 type的值为1，2，3分别表示前，后，手持 照片4:营业执照,5:其他证件
    if (type == 1) {
      this.setData({
        frontPic: url
      });
    } else if (type == 2) {
      this.setData({
        backPic: url
      });
    } else if (type == 3) {
      this.setData({
        handPic: url
      });
    } else if (type == 4) {
      this.setData({
        businessLicenceUrl: url
      });
    } else if (type == 5) {
      this.setData({
        extraVerifyPicUrl: url
      });
    }

  },
  //  当上传图片类型为 证件类型时候会传参数进来 代表前，后，手持身份证图片
  choose(e) { //上传图片操作
    var self = this
    wx.chooseImage({
      count: 1, //选择的图片数量
      success(res) {
        wx.showLoading({
          title: '正在加载中....',
          mask: true,
        })
        const tempFilePaths = res.tempFilePaths
        wx.uploadFile({
          url: app.globalData.domain + app.globalData.upLoadUrl, //我们服务端地址
          filePath: tempFilePaths[0],
          name: 'file', //图片的key
          formData: {}, //除了图片的其他参数
          success: res => { //请求成功回调 这里收到的是服务端下发 的数据
            var data = res.data
            if (typeof data == 'string') {
              data = JSON.parse(data)
            }
            // 获取图片url
            self.getImgUrl(data.content.fileUrl, e.currentTarget.dataset.type)
            wx.hideLoading();
          }
        })
      }
    })
  },
  //  弹出提示文字
  isMeet(flag, msg) {
    if (flag) {
      wx.showToast({
        title: msg,
        icon: 'none',
        mask: true,
      })
      return true
    }
    return false
  },
  //  实名认证校验
  verifyIdCard() {
    //2-12个汉字
    var regName = /^[\u4e00-\u9fa5]{2,12}$/;
    //15或者18位数字，或者17位数字加一个字母
    var regIdNum = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
    if (this.isMeet(!regName.test(this.data.name), '请输入正确的姓名')) {
      return false;
    }
    if (this.isMeet(!regIdNum.test(this.data.idNum), '请输入正确的身份证号')) {
      return false;
    }
    // 统计未上传图片数量
    var picLength = 0
    if (!this.data.frontPic) {
      picLength++
    }
    if (!this.data.backPic) {
      picLength++
    }
    // if (!this.data.handPic) {
    //     picLength++
    // }
    if (this.isMeet(picLength != 0, '您还有' + picLength + '张照片没上传呢，请完善后上传。')) {
      return false;
    } else if (!this.data.agreement) {
      wx.showToast({
        title: '必须同意《雇主承诺书》才能提交认证哦',
        icon: 'none'
      })
      return false
    }
    return true
  },
  //  提交实名认证信息
  sendIdCard() {
    var params = {
      id_card_url_front: this.data.frontPic,
      id_card_url_back: this.data.backPic,
      id_card_url_third: this.data.handPic,
      true_name: this.data.name,
      id_card_no: this.data.idNum,
    }
    if (!params.id_card_url_third) {
      delete params.id_card_url_third
    }
    postVerifyInfoApi(params).then(res => {
      var code = res.data.errCode
      var tip = (code == 0) ? '提交成功' : res.data.errMsg
      wx.showToast({
        title: tip,
        icon: 'none',
        mask: true,
      })
      if (code == 0) { //返回上一页
        setTimeout(() => {
          wx.navigateBack()
        }, 500)
      }
    })
  },
  // 实名认证点击保存
  idCardOkBtn(e) {
    if (!this.verifyIdCard()) {
      return;
    }
    this.sendIdCard()
  },
  //  获取企业认证信息
  getCompanyData() {
    let params = {}
    getVerifyInfoApi(params).then(res => {
      let data = res.data.content.last_business_licence_verify_info
      this.setData({
        enterpriseName: data.enterprise_name ? data.enterprise_name : '',
        registNum: data.regist_num ? data.regist_num : '',
        businessLicenceUrl: data.business_licence_url ? data.business_licence_url : '',
        extraVerifyPicUrl: data.extra_verify_pic_url ? data.extra_verify_pic_url : '',
      });
    })
  },
  //  企业名称填写
  bindCompanyNameChange(e) {
    this.setData({
      enterpriseName: e.detail.value
    })
  },
  //  营业执照填写
  bindRegistNumChange(e) {
    this.setData({
      registNum: e.detail.value
    })
  },
  //  勾选同意职位发布协议
  changeAgreementState() {
    this.setData({
      agreement: !this.data.agreement
    })
  },
  //  校验企业信息
  verifyCompanyInfo() {
    if (!this.data.enterpriseName) {
      wx.showToast({
        title: '请输入企业名称',
        icon: 'none'
      })
      return false;
    }
    if (!this.data.registNum) {
      wx.showToast({
        title: '请输入营业执照号',
        icon: 'none'

      })
      return false;
    }
    if (!this.data.businessLicenceUrl) {
      wx.showToast({
        title: '请上传营业执照',
        icon: 'none'
      })
      return false;
    }
    // if (!this.data.extraVerifyPicUrl) {
    //     wx.showToast({
    //         title: '请上传法人身份证',
    //         icon: 'none'
    //     })
    //     return false;
    // }
    if (!this.data.agreement) {
      wx.showToast({
        title: '必须同意《雇主承诺书》才能提交认证哦',
        icon: 'none'
      })
      return false
    }
    return true
  },
  //  提交企业认证信息
  postCompanyInfo() {
    if (this.verifyCompanyInfo()) {
      let param = {
        enterprise_name: this.data.enterpriseName,
        regist_num: this.data.registNum,
        business_licence_url: this.data.businessLicenceUrl,
        extra_verify_pic_url: this.data.extraVerifyPicUrl,
      }
      if (!param.extra_verify_pic_url) {
        delete param.extra_verify_pic_url
      }
      postEnterpriseVerifyInfoApi(param).then(res => {
        let code = res.data.errCode
        let tip = (code == 0) ? '提交成功' : res.data.errMsg
        wx.showToast({
          title: tip,
          icon: 'none',
          mask: true,
        })
        if (code == 0) {
          setTimeout(() => {
            wx.navigateBack()
          }, 500)
        }
      })
    }

  },
  //  跳转雇主协议页面
  gotoWebViewPage() {
    wx.navigateTo({
      url: '/pages/webView/index?url=https://www.jianke.cc/wap/entPromise&title=雇主承诺书'
    })
  },
  //  打开登录弹窗
  openLogin() {
    this.setData({
      showLogin: true
    })
    modalLogin = this.selectComponent("#modal_login");
    modalLogin.openModal();
  },
  //  关闭登录弹窗
  closeLogin() {
    this.setData({
      showLogin: false
    })
    modalLogin = this.selectComponent("#modal_login");
    modalLogin.closeModal();
  },


})