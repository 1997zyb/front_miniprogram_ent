// pages/vipUserRule/index.js
import {
    getJobClassifyListApi,
    savaClueApi 
} from "../../api/index";

const app = getApp()
var modalContact = {}

Page({

    /**
     * 页面的初始数据
     */
    data: {
        title: 'VIP套餐适用规则',
        jobList: ''
    },

    onLoad(options) {
        this.setData({
            type: options.type
        })
        getJobClassifyListApi().then(res => {
            console.log(res)
            let jobList = ''
            res.data.content.job_classifier_list.forEach((item, index) => {
                jobList = jobList + item.job_classfier_name + (index == res.data.content.job_classifier_list.length - 1 ? '' : '、')
            })
            this.setData({
                jobList
            })
        })
    },

    // 前往用户协议
    gotoServiceAgreementPage() {
        let url = encodeURIComponent(app.globalData.domain + '/wap/userAgreementJkzp')
        wx.navigateTo({
            url: '/pages/webView/index?url=' + url
        })
    },

    // openModal(){

    //     modalContact = this.selectComponent("#modal_contact");
    //     modalContact.openModal()
    // }
    openModal(e) {
        let type = e.currentTarget.dataset.type
        var text = (type == 1 ? '更多岗位服务：有意向开通特殊岗位包代招服务，提交联系方式' : '开通多个VIP城市：有意向开通多个VIP城市（或全国）') + wx.getStorageSync('user_globalData').bean.contactTel
        savaClueApi({
            city_id: wx.getStorageSync('current_city').id||'',
            is_need_contact: 1,
            clue_desc: text
        }).then(res => {
            wx.showModal({
                title: '',
                content: '您的业务咨询已传达给招聘顾问，顾问会在一个工作日内与您联系，请您保持手机通畅',
                showCancel: false,
                confirmText: '知道了',
            })
        })
    }
})