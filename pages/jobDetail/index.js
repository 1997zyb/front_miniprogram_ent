import {getJobDetailApi,} from '../../api/index'
import {parseTime} from '../../utils/util'
let app = getApp();
let successUrl = '';
let jobUuid = '';
let modalLogin = {}

Page({
    data: {
        jobDetail: {},
        noShow: true,
        loadingHide: false,
        advertising: {},
        lookMoreList: [],
        isFold: true,
        showLogin: false,
        loadOption: {},
        closeAd: false,
        animationData: {},
    },
    onLoad(option) {
        this.getInfo(option)

    },
    getInfo(option) {
        let id = option.job_uuid
        this.getJobDetail(id)
    },
    onShow() {
    },
    
    //  获取职位详情
    getJobDetail(id) {
        let para = {
            job_uuid: id,
            city_id: 211
        }
        getJobDetailApi(para).then(res => {
            //  获取看了又看职位列表
            let data = res.data.content.parttime_job;
            jobUuid = res.data.content.parttime_job.job_uuid
            data.apply_dead_time = parseTime(data.apply_dead_time)
            this.setData({
                jobDetail: data,
                noShow: false,
                loadingHide: true,
            });
            /**
             * type : 1.公众号 2.微信号 3.电话
             */
            var contactData = {};
            if (data.wechat_public) {
                contactData = {
                    type: 1,
                    contact: data.wechat_public
                }
            } else if (data.wechat_number) {
                contactData = {
                    type: 2,
                    contact: data.wechat_number
                }
            } else if (data.qq_number) {
                contactData = {
                    type: 3,
                    contact: data.qq_number
                }
            } else {
                contactData = {
                    type: 4,
                    contact: data.contact.phone_num
                }
            }
        })
    },
    //  跳转地图页面
    gotoMapPage() {
        if (this.data.jobDetail.map_coordinates) {

            let locate = this.data.jobDetail.map_coordinates.split(',')
            const latitude = locate[0] - 0
            const longitude = locate[1] - 0
            const name = this.data.jobDetail.working_place
            wx.openLocation({
                latitude,
                longitude,
                name,
                scale: 18
            })
        }

    },


})
