let app = getApp()
import {
    getFansListApi
} from '../../api/index'

Page({
    data: {
        show:false,
        title: '我的粉丝',
        addressAreaId: '',
        jobClassifyId: '',
        sex: '',
        ageLimit: '',
        loading: true,
        noData: true,
        talentList: [],
        currentTalent: null,
        noMoreData: false,
        pageNum: 1,
        pageSize: 10,
        telephone: ''
    },
    onLoad(option) {
        this.getTalentList()
    },
    onShow() {
    },
    onUnload() {
    },
    onPullDownRefresh() {
        wx.stopPullDownRefresh();
    },
    // 下拉加载
    onReachBottom() {
        if (!this.data.noMoreData) {
            this.data.pageNum++
            this.getTalentList()
        }
    },
    changTalent(e) {
        this.data.talentList[e.detail].is_collect == 1 ? this.data.talentList[e.detail].is_collect = 0 : this.data.talentList[e.detail].is_collect = 1
        this.setData({talentList:this.data.talentList})
    },
    getTalentList() {
        let param = {
            page_size: this.data.pageSize,
            page_num: this.data.pageNum,
        }
        getFansListApi(param).then(res => {
            this.data.talentList = this.data.talentList.concat(res.data.content.focus_fans_list)
            if (this.data.talentList.length == 0) {
                this.setData({noData: true})
            }else {
                this.setData({noData: false})
            }
            if (res.data.content.focus_fans_list.length < this.data.pageSize) {
                this.setData({noMoreData: true})
            }
            this.setData({loading: false})
            this.setData({talentList: this.data.talentList})
        })
    },

})


