const app = getApp();
import {
    loginApi,
    getUserInfoApi,
    getImTokenApi
} from '../api/index'
import {
    createSession
} from './request'
import {
    getResume
} from './util'
/**
 * 获取当前页面
 */
let that;
let open_id = ''
let oauth_id = ''
let ifLogin = () => {
    const pages = getCurrentPages();
    that = pages[pages.length - 1];
    wxLogin();
}
const period = 3600 * 1000

let wxLogin = () => {
    wx.getStorage({
        key: 'creat_time',
        success: res => {
            let time = new Date().getTime()
            if (time - res.data >= period || !wx.getStorageSync('is_login')) {
                wx.login({
                    success: (res) => {
                        createSession().then(res2 => {
                            let param = {
                                code: res.code,
                                login: 1
                            }
                            loginApi(param).then(res => {
                                if (res.data.errCode != 0) {
                                    console.log('登录失败')
                                    wx.setStorageSync('is_login', false)
                                } else {
                                    if (res.data.content.is_login == 1) {
                                        open_id = res.data.content.open_id
                                        oauth_id = res.data.content.oauth_id
                                        wx.setStorage({
                                            key: "open_id",
                                            data: open_id
                                        })
                                        wx.setStorage({
                                            key: "oauth_id",
                                            data: oauth_id
                                        })
                                        wx.setStorageSync('is_login', true)
                                        getUserInfoApi().then(res => {
                                            checkAndAddCityInfo(() => {
                                                let pages = getCurrentPages()
                                                let currentPage = pages[pages.length - 1]
                                                if (res.data.content.trueName && res.data.content.bean.industryId && res.data.content.bean.cityId) {
                                                    app.globalData.userInfo = res.data.content.bean
                                                    app.globalData.userInfo.name = res.data.content.trueName
                                                    app.globalData.userInfo.complete = res.data.content.trueName && app.globalData.userInfo.industryId && app.globalData.userInfo.cityId
                                                } else if (currentPage.route != 'pages/personalInformation/index') {
                                                    wx.navigateTo({
                                                        url: '/pages/personalInformation/index'
                                                    })
                                                }
                                                getImTokenApi({
                                                    accountId: res.data.content.bean.accountId
                                                }).then(res2 => {
                                                    let info = res2.data.content.im_account_info
                                                    let userList = wx.getStorageSync('imUserList') ? wx.getStorageSync('imUserList') : []
                                                    userList.push({
                                                        name: info.account_name,
                                                        type: 1,
                                                        token: info.im_token,
                                                        id: info.uuid,
                                                        avatar: info.account_img_url,
                                                        accountId: info.account_id,
                                                    })
                                                    wx.setStorageSync('user_list', userList)
                                                    connect()
                                                }).catch(err => {
                                                    console.log(err)
                                                })
                                            })
                                        }).catch(e => {
                                            console.log(e)
                                        })
                                    } else {
                                        wx.setStorageSync('is_login', false)
                                    }
                                }
                                if (that.getInfo) {
                                    that.getInfo()
                                } else {
                                    that.onLoad()
                                }

                            })
                        })
                    },
                })
            } else {
                getUserInfoApi().then(res => {
                    if (res.data.content.trueName && res.data.content.bean.industryId && res.data.content.bean.cityId) {
                        app.globalData.userInfo = res.data.content.bean
                        app.globalData.userInfo.name = res.data.content.trueName
                        app.globalData.userInfo.complete = res.data.content.trueName && app.globalData.userInfo.industryId && app.globalData.userInfo.cityId
                    } else {
                        wx.navigateTo({
                            url: '/pages/personalInformation/index'
                        })
                    }
                    // getImTokenApi({accountId:res.data.content.bean.accountId}).then(res2=>{
                    // }).catch(err=>{
                    //     console.log(err)
                    // })
                }).catch(e => {
                    console.log(e)
                })
                if (that.getInfo) {
                    that.getInfo()
                } else {
                    that.onLoad()
                }
            }
        },
        fail: res => {
            wx.login({
                success: (res) => {
                    createSession().then(res2 => {
                        let param = {
                            code: res.code,
                            login: 1
                        }
                        loginApi(param).then(res => {
                            if (res.data.errCode != 0) {
                                console.log('登录失败')
                                wx.setStorageSync('is_login', false)
                            } else {
                                if (res.data.content.is_login == 1) {
                                    open_id = res.data.content.open_id
                                    oauth_id = res.data.content.oauth_id
                                    wx.setStorage({
                                        key: "open_id",
                                        data: open_id
                                    })
                                    wx.setStorage({
                                        key: "oauth_id",
                                        data: oauth_id
                                    })
                                    wx.setStorageSync('is_login', true)
                                    getResume()
                                } else {
                                    wx.setStorageSync('is_login', false)
                                }
                            }
                            if (that.getInfo) {
                                that.getInfo()
                            } else {
                                that.onLoad()
                            }
                        })
                    })
                },
            })
        }
    });

}

module.exports = {
    $ifLogin: ifLogin,
}