import {
    getInfoByUserIdApi,
    getImTokenApi,
    getInfoByAccountIdApi
} from '../api/index'

const RongIMLib = require('../lib/RongIMLib.miniprogram-1.1.3.min.js')
const RongIMClient = RongIMLib.RongIMClient;
//引入service层，主要负责数据交换，收发消息
export const Service = require('../services.js')({
    // appkey: 'y745wfm84ix8v' //正式
    appkey: 'sfci50a7cr27i'//测试
});
export const Status = Service.Status
export const Conversation = Service.Conversation
// 获取imtoken
export const getToken = function(accountId, backcall) {
    wx.removeStorageSync('user_list')
    let param = {
        accountId: accountId
    }
    getImTokenApi(param).then(res => {
        let info = res.data.content.im_account_info
        let userList = wx.getStorageSync('user_list')
        if (!userList) {
            userList = []
            userList.push({
                name: info.account_name,
                type: 1,
                token: info.im_token,
                id: info.uuid,
                avatar: info.account_img_url,
                accountId: info.account_id
            })
        } else {
            userList[userList.length - 1] = {
                name: info.account_name,
                type: 1,
                token: info.im_token,
                id: info.uuid,
                avatar: info.account_img_url,
                accountId: info.account_id
            }
        }
        wx.setStorageSync('user_list', userList)
        backcall()
    })
}
// 统计会话总数,并查找没有信息的用户
export const setTabBarBadge = function(conversationList, context) {
    let count = 0
    let userList = wx.getStorageSync('user_list')
    let thisUser = userList[userList.length - 1]
    // console.log(conversationList)
    conversationList.forEach((conversation, index) => {
        if (conversation.target.flag && conversation.latestMessage.conversationType == 1 && conversation.latestMessage.content.extra) {
            let content = JSON.parse(conversation.latestMessage.content.extra.substr(69))
            let type = (content.fromUser == thisUser.accountId ? 'to' : 'from')
            if (content.bizType == 20007) { //设置系统消息
                content.content = JSON.parse(content.content)
                var temp = {
                    name: content.content.name,
                    type: 1,
                    avatar: content.content.headUrl,
                    id: conversation.targetId,
                    accountId: content.fromUser,
                }
                conversation.target = temp
                let userList = wx.getStorageSync('user_list')
                userList.unshift(temp)
                wx.setStorageSync('user_list', userList)
                if (context && context.route == 'pages/conversation/list') {
                    context.setData({
                        conversationList
                    })
                }
                wx.setStorageSync('conversationList', conversationList)
            } else {
                let content = JSON.parse(conversation.latestMessage.content.extra.substr(69))
                let type = (content.fromUser == thisUser.accountId ? 'to' : 'from')
                let params = {
                    accountId: content[type + 'User']
                }
                getInfoByAccountIdApi(params).then(res => {
                    res = res.data.content.friend
                    var temp = {
                        name: res.nickname,
                        type: 1,
                        avatar: res.headUrl,
                        id: res.uuid,
                        accountId: res.accountId
                    }
                    conversation.target = temp
                    userList.unshift(temp)
                    wx.setStorageSync('user_list', userList)
                    if (context && context.route == 'pages/conversation/list') {
                        context.setData({
                            conversationList
                        })
                    }
                    wx.setStorageSync('conversationList', conversationList)
                })
            }
        } else {
            if (context && context.route == 'pages/conversation/list') {
                context.setData({
                    conversationList
                })
            }
            wx.setStorageSync('conversationList', conversationList)
        }
        if (conversation.latestMessage.conversationType == 1)
            count += conversation.unReadCount
    })
    if (count) {
        count = count > 99 ? '99+' : count.toString()
        wx.setTabBarBadge({
            index: 2,
            text: count
        })
    } else {
        wx.removeTabBarBadge({
            index: 2,
        })
    }
}
// 开启对话监听
export const watchConversation = function() {
    Conversation.watch((conversationList) => {
        wx.setStorageSync('conversationList', conversationList)
        var pages = getCurrentPages();
        var that = pages[pages.length - 1];
        setTabBarBadge(conversationList, that)
    });
}
// 重新连接
export const reconnect = function(res) {
    if (wx.getStorageSync('is_login') && wx.getStorageSync('im_connect'))
        Status.reconnect(res)
}
// 监听状态
export const watchStatus = function() {
    Status.watch((status) => {
        if (status == 3) {
            Status.connect();
        }
    })
}
// 连接服务器
export const connect = function() {
    watchConversation();
    watchStatus();
    Status.connect().then(() => {
        wx.setStorageSync('im_connect', true)
        console.log('connect successfully');
    }, (error) => {
        wx.showToast({
            title: error.msg,
            icon: 'none',
            duration: 3000
        })
    })
}
