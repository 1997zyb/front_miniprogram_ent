import {getUserInfoApi, recordAdClickApi,postEnterpriseRecruitmentCityApi} from '../api/index';

export function formatTime(timestamp, format) {
    　　const formateArr = ['Y', 'M', 'D', 'h', 'm', 's'];
    　　let returnArr = [];
    　　let date = new Date(timestamp); //13位的时间戳,    如果不是13位的,  就要乘1000,就像这样 let date = new Date(timestamp*1000)
    　　let year = date.getFullYear()
    　　let month = date.getMonth() + 1
    　　let day = date.getDate()
    　　let hour = date.getHours()
    　　let minute = date.getMinutes()
    　　let second = date.getSeconds()
    　　returnArr.push(year, month, day, hour, minute, second);
    　　returnArr = returnArr.map(formatNumber);
    　　for (var i in returnArr) {
        　　format = format.replace(formateArr[i], returnArr[i]);
    }
    　　return format;
}
const formatNumber = n => {
    n = n.toString()
    return n[1] ? n : '0' + n
}
// module.exports = {
//     　　formatTime: formatTime
// }
// export function formatTime(date) {
//     let year = date.getFullYear()
//     let month = date.getMonth() + 1
//     let day = date.getDate()
//     let hour = date.getHours()
//     let minute = date.getMinutes()
//     let second = date.getSeconds()
//     return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
// }

export function parseTime(time, cFormat) {
    if (arguments.length === 0) {
        return null
    }
    const format = cFormat || '{y}-{m}-{d} {h}:{i}:{s}'
    let date
    if (typeof time === 'object') {
        date = time
    } else {
        if ((typeof time === 'string') && (/^[0-9]+$/.test(time))) {
            time = parseInt(time)
        }
        if ((typeof time === 'number') && (time.toString().length === 10)) {
            time = time * 1000
        }
        date = new Date(time)
    }
    const formatObj = {
        y: date.getFullYear(),
        m: date.getMonth() + 1,
        d: date.getDate(),
        h: date.getHours(),
        i: date.getMinutes(),
        s: date.getSeconds(),
        a: date.getDay()
    }
    const time_str = format.replace(/{(y|m|d|h|i|s|a)+}/g, (result, key) => {
        let value = formatObj[key]
        // Note: getDay() returns 0 on Sunday
        if (key === 'a') {
            return ['日', '一', '二', '三', '四', '五', '六'][value]
        }
        if (result.length > 0 && value < 10) {
            value = '0' + value
        }
        return value || 0
    })
    return time_str
}

// export function formatNumber(n) {
//     n = n.toString()
//     return n[1] ? n : '0' + n
// }

//  根据城市名称获取城市ID
export function getCityId(name, list) {
    for (let i = 0; i < list.length; i++) {
        if (name.indexOf(list[i].name) !== -1) {
            return list[i].id
        } else if (i === list.length) {
            return false
        }
    }
}

//  根据城市ID获取城市名称
export function getCityName(id, list) {
    for (let i = 0; i < list.length; i++) {
        if (id == list[i].id) {
            return list[i].name
        }
    }
}

//  获取用户简历信息
export function getResume() {
    return new Promise((resolve, reject) => {
        getUserInfoApi().then(res => {
            if (res) {
                let resume = res.data.content,
                    hasSex = false;
                if (resume.sex == 0 || resume.sex == 1) {
                    hasSex = true;
                }
                if (!resume.true_name || !resume.account_telphone || !resume.birthday || !hasSex) {
                    resume.no_complete = 1;
                }
                wx.setStorageSync('user_globalData', resume);
                wx.setStorageSync('is_login', true);
                let app = getApp()
                app.globalData.isRefresh = true;
                app.globalData.userInfo = res.data.content;

                let pages = getCurrentPages()
                let currentPage = pages[pages.length - 1]
                if (res.data.content.trueName && res.data.content.bean.industryId && res.data.content.bean.cityId) {
                    app.globalData.userInfo = res.data.content.bean
                    app.globalData.userInfo.name = res.data.content.trueName
                    app.globalData.userInfo.complete = res.data.content.trueName && app.globalData.userInfo.industryId && app.globalData.userInfo.cityId
                } else if (currentPage.route !='pages/personalInformation/index'){
                    
                    wx.navigateTo({
                        url: '/pages/personalInformation/index'
                    })
                }
                resolve(res)
            }
        })
    })
}

//  根据广告类型去不同页面  type==1打开h5,type==6,打开职位详情,type==7,打开职位列表
export function openAdDetail(data) {
    if (data.ad_type) {
        if (data.ad_type == 1) {//  外链
            let sessionId = wx.getStorageSync('sessionid');
            // let nurl = putParameterToUrl({app_user_token:sessionId},data.ad_detail_url)
            let nurl = putParameterToUrl({app_user_token: '84aaf439ae614c798ca9043a7e861db3'}, data.ad_detail_url)
            let url = encodeURIComponent(nurl)
            wx.navigateTo({
                url: '/pages/webView/index?url=' + url
            })
        } else if (data.ad_type == 6) {//  小程序页面
            wx.navigateTo({
                url: data.ad_detail_url
            })
        } else {
            wx.showToast({
                title: '广告类型错误'
            })
        }
    }
}

//  将参数拼接到路径上
export function putParameterToUrl(values, sURL) {
    let params = "";
    for (let v in values) {
        if (v !== undefined) {
            params =  v + '=' + values[v];
        }
    }
    if (sURL.indexOf('&') === -1) {
        params = '&' + params;
    }
    if (sURL.indexOf('?') === -1) {
        params = '?' + params;
    }
    return sURL + params;
}

//  根据出生日期算年纪
export function getAge(strBirthday) {
    let returnAge;
    let strBirthdayArr = strBirthday.split("-");
    let birthYear = strBirthdayArr[0];
    let birthMonth = strBirthdayArr[1];
    let birthDay = strBirthdayArr[2];
    let d = new Date();
    let nowYear = d.getFullYear();
    let nowMonth = d.getMonth() + 1 < 10 ? '0' + (d.getMonth() + 1) : d.getMonth() + 1;
    let nowDay = d.getDate() < 10 ? '0' + d.getDate() : d.getDate();
    if (nowYear == birthYear) {
        returnAge = 0;//同年 则为0岁
    } else {
        let ageDiff = nowYear - birthYear; //年之差
        if (ageDiff > 0) {
            if (nowMonth == birthMonth) {
                let dayDiff = nowDay - birthDay;//日之差
                if (dayDiff < 0) {
                    returnAge = ageDiff - 1;
                } else {
                    returnAge = ageDiff;
                }
            } else {
                let monthDiff = nowMonth - birthMonth;//月之差
                if (monthDiff < 0) {
                    returnAge = ageDiff - 1;
                } else {
                    returnAge = ageDiff;
                }
            }
        } else {
            returnAge = -1;//返回-1 表示出生日期输入错误 晚于今天
        }
    }

    return returnAge;//返回周岁年龄

}

// //  手机号格式验证
// export function checkPhone(){
//     if(!(/^1[345678]\d{9}$/.test(phone))){
//         return false;
//     }
// }
// //  邮箱格式验证
// export function checkPhone(){
//     if(!(/^([A-Za-z0-9_\-\.\u4e00-\u9fa5])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,8})$/.test(phone))){
//         return false;
//     }
// }

//  获取当前页面url上的参数
//  @name String  参数名
//  Return String
export function getQueryString(params, name) {
    let reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    let r = params.match(reg);//search,查询？后面的参数，并匹配正则
    if (r != null) return decodeURIComponent(r[2]);
    return null;
}

//  获取删除url上的参数
//  @url String  地址
//  @name String  参数名
//  @nvalue String  参数名
//  Return String
export function urlDelParam(url, name) {
    let urlArr = decodeURIComponent(url).split('?');
    if (urlArr.length > 1 && urlArr[1].indexOf(name) > -1) {
        let query = urlArr[1];
        let arr = query.split("&");
        let urlte = urlArr[0] + '?'
        let paramStr = ''
        for (let i = 0; i < arr.length; i++) {
            let key = arr[i].split("=")[0]
            let value = arr[i].split("=")[1];
            if (key !== name) {
                paramStr = i === 0 ? paramStr : paramStr + '&'
                paramStr = paramStr + key + '=' + value
            }
        }
        return urlte + paramStr;
    } else {
        return url;
    }
}

// 获取数组内ID名称
export function getArryNameById(id,arry){
    arry.forEach((item,index)=>{
        if(item.id == id){
            return item.name
        }else if (index == arry.length-1){
            return ''
        }
    })
}

// 登录判断
export function checkLogin(context, callback) {
    if (!wx.getStorageSync('is_login')) {
        context.openLogin()
    } else {
        callback()
    }
}


// 城市填写项判断
export function checkAndAddCityInfo(callback){
    let user_globalData=wx.getStorageSync('user_globalData')
    let currentCityInfo=wx.getStorageSync('current_city')
    if(user_globalData&&user_globalData.bean&&!user_globalData.bean.cityId&&currentCityInfo&&parseInt(currentCityInfo.id)>0){
        postEnterpriseRecruitmentCityApi({
            recruitment_city_id:currentCityInfo.id
        }).then(res=>{
            (callback && typeof(callback) === "function") && callback()
        })
        // .catch(res=>{
        //     (callback && typeof(callback) === "function") && callback()
        // })
    }else{
        (callback && typeof(callback) === "function") && callback()
    }
}