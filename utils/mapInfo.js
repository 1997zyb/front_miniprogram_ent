//获取位置信息
let QQMapWX = require('./qqmap-wx-jssdk.min');
let qqmapsdk;
// 实例化腾讯地图API核心类
qqmapsdk = new QQMapWX({
    key: '6ZWBZ-EWYWQ-CBG5I-GME4Y-SMO5S-M6BST' // 必填
});
const mapInfo = () => {
    return new Promise((resolve, reject) => {
        wx.getSetting({
            success: (res) => {
                if (res.authSetting.hasOwnProperty('scope.userLocation') && res.authSetting['scope.userLocation']===false) {
                    wx.showModal({
                        title: '是否授权地理位置信息',
                        content: '需要获取您的地理位置信息，请确认授权，否则无法推送本地职位',
                        success: function (tip) {
                            if (tip.confirm) {
                                wx.openSetting({
                                    success: function (data) {
                                        if (data.authSetting["scope.userLocation"]) {
                                            wx.getLocation({
                                                success: function (res) {
                                                    qqmapsdk.reverseGeocoder({
                                                        location: {
                                                            latitude: res.latitude,
                                                            longitude: res.longitude
                                                        },
                                                        success: function (addressRes) {
                                                            let address = addressRes.result
                                                            resolve(address);
                                                        }
                                                    })
                                                },
                                                fail: function () {
                                                }
                                            })
                                        } else {
                                            wx.showToast({
                                                title: '授权失败',
                                                icon: 'none',
                                                duration: 2000
                                            })
                                        }
                                    }
                                })
                            }
                            else {
                                let address = {city:'选择城市'}
                                resolve(address);
                            }
                        },
                    })
                } else {
                    wx.getLocation({
                        success: function (res) {
                            qqmapsdk.reverseGeocoder({
                                location: {
                                    latitude: res.latitude,
                                    longitude: res.longitude
                                },
                                success: function (addressRes) {
                                    let address = addressRes.result
                                    resolve(address);
                                }
                            })
                        },
                        fail: function () {
                            wx.showModal({
                                title: '获取地理位置信息失败',
                                content: '请确认是否打开手机定位功能',
                                success: function (tip) {
                                    let address = {city:'选择城市'}
                                    resolve(address);
                                },
                            })

                        }
                    })
                }
            }
        })
    })
}
module.exports = {
    $getMapInfo: mapInfo,
}
