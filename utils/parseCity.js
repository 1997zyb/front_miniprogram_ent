/**
 * 城市模块租组件
 * @module modParseCity
 * @requires jquery
 * @requires modCommon
 * @author fancy
 */
var common = require('common.js');
module.exports = {
    init: function (data) {
        var self = this;
        self._data = data;
        return self;
    },
    /*通过城市ID，获取城市对象*/
    _getCityById: function (cityId) {
        var self = this, city;
        cityId = parseInt(cityId, 10);
        if (!self._data) {
            return city;
        }
        self._data.forEach(function (v, i) {
          if (v['id'] === cityId) {
            city = v;
            return false;
          }
        })
        return city;
    },
    /*通过城市名称，获取城市对象*/
    _getCityByName: function (name) {
        var self = this, city=null;
        if (!self._data ) {
            return city;
        }
        self._data.forEach(function (v, i) {
          if (v['name'] === name) {
            city = v;
            return false;
          }
        })
        /*如果城市为空，默认为福州*/
        if (city===null) {
            city = {
              name: common.MAP.CITY_INFO.FUZHOU.NAME,
              id: common.MAP.CITY_INFO.FUZHOU.ID
            }
        }

        return city;
    },
    /*
    @通过城市名称 获取 城市ID
    @如果没有定位到 城市 ，默认为 城市 为福州,id为 枚举 FUZHOU_ID
    *@参数：name：城市名称；{string}
    */
    getCityIdByName: function (name) {
        var self = this;
        /*如果没有匹配到城市，默认城市为福州*/
        var id = common.MAP.CITY_INFO.FUZHOU.ID;
        var city = self._getCityByName(name);
        if (city) {
            id = city['id'];
        }
        return id;
    },
    _isMatch: function (key, s) {
        key = key.toLowerCase();
        try {
            s = s.toLowerCase();
        }catch (e) {
            s='a'
        }

        if (key.length > s.length) {
            return false;
        }
        if (key === s.slice(0, key.length)) {
            return true;
        }
    },
    /*
    *通过关键字，获取城市数据列表
    */
    getCitiesByKeyword: function (keyword) {
        var self = this, cities = [];
        if (!self._data) {
          return cities;
        }
        self._data.forEach(function (v, i) {
          if (self._isMatch(keyword, v['name'])) {
            cities.push(v);
            return true;
          }
          if (self._isMatch(keyword, v['jianpin'])) {
            cities.push(v);
            return true;
          }
          if (self._isMatch(keyword, v['spelling'])) {
            cities.push(v);
            return true;
          }
        })
        return cities;
    },
    /*
    *通过城市ID，后去区域数据列表
    *@改成异步
    */
    getCityArea: function (cityId) {
        var self = this;
        cityId = parseInt(cityId, 10);
        var city = self._getCityById(cityId);
        var area = [];
        return area;
    },
     /*
    *获取热门城市
    */
    getHotCities: function () {
        var self = this;
        var list = [];
        if (!self._data) {
            return list;
        }
        self._data.forEach(function (v, i) {
          if (v.hot_cities == 1) {
            list.push({
              name: v.name,
              id: v.id
            });
          }
        })
        return list;
    },
    /*
    *获取城市列表
    */
    _getGroupList: function () {
      var self = this;
      var index = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
      var group;
      var list = [];
      for (var i = 0, len = index.length; i < len; i++) {
        group = self.getCitiesByKeyword(index.charAt(i));
        if (group.length) {
          list.push({
            dt: index.charAt(i),
            dd: self._convertCities(group)
          })
        }
      }
      return list;
    },
    _convertCities: function (cities) {
      var result = [];
      cities.forEach(function (v, i) {
        result.push({
          name: v['name'],
          id: v['id']
        });
      })
      return result;
    },
    /*
    *获取搜索城市结果
    */
    _getMatchCities: function (val) {
      var self = this;
      var list = self._convertCities(self.getCitiesByKeyword(val))
      return list;
    },
};
