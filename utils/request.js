
let app = getApp()
// let BASEURL =  "https://m.jianke.cc"//  正式
// let BASEURL =  "http://m.shijianke.com"//  测试
// let BASEURL =  "http://172.16.1.113:8080"//  本地
let BASEURL =  app.globalData.domain //  本地
const createSession = (data) => {
    return new Promise((resolve, reject) => {
            wx.request({
                url: BASEURL + '/m/wechatApplet/createSession',
                data: {},
                success: (res) => {
                    //  记录通讯的sessionid
                    wx.setStorageSync('sessionid', res.data.content.sessionId);
                    //  登录、注册公钥
                    wx.setStorageSync('pub_key_modulus', res.data.content.pub_key_modulus);
                    wx.setStorageSync('pub_key_exp', res.data.content.pub_key_exp);
                    wx.setStorageSync('pub_key_base64', res.data.content.pub_key_base64);
                    //  密码加密随机数
                    wx.setStorageSync('challenge', res.data.content.challenge);
                    wx.setStorageSync('creat_time', Date.parse(new Date()));
                    wx.setStorageSync('user_globalData', '');
                    wx.setStorageSync('is_login', false);
                    resolve(res)
                }
            });
    })
}
const request = (data) => {
    if(!data.hideLoading){
        wx.showLoading({
            title: '加载中...',
            mask: true
        })
    }
    return new Promise((resolve, reject) => {
        let sessionId = wx.getStorageSync('sessionid');
        if ((!sessionId || sessionId == "") ) {
            createSession().then(res => {
            })
        }
        if (!data.params) {
            data.params={}
        }
        data.params.is_from_wa = 1
        data.params.sessionId_sjk = sessionId
        data.params.user_type = 1
        let sendRequest = wx.request({
            url: BASEURL + data.url,
            method: data.method,
            data: data.params,
            header: {
                'content-type': data.header ? data.header : 'application/x-www-form-urlencoded',
            },
            success: (res) => {
                if(res.statusCode==500){
                   return
                }
                if(res.data.errCode == 0){//  成功
                    resolve(res)
                } else if (res.data.errCode == 2) {//  session过期的场景
                    createSession().then(res2 => {
                        resolve(res)
                    })
                    return;
                } else if (res.data.errCode == 15) {//  需要登录
                    wx.setStorage({
                      key:"is_login",
                      data:false
                    });
                    let pages = getCurrentPages();
                    let currentPage = pages[pages.length - 1];
                    currentPage.openLogin()
                    // return Promise.reject( 'Error')
                }
                else {
                    //  接口数据错误
                    // setTimeout(()=>{
                    //     let errMsg = '网路错误'
                    //     if(res.data.errMsg)
                    //     {
                    //                     //         errMsg = res.data.errMsg
                    //     }
                    //     wx.showToast({
                    //         title: errMsg,
                    //         icon:'none',
                    //         duration:2000
                    //     })
                    // },1000)
                    resolve(res)
                }

            },
            fail: (err) => {
                wx.showModal({
                    title: '请求失败',
                    content: '网络出错，请稍后重试',
                    cancelText:'返回上一页',
                    showCancel: false,
                    success: function (res) {
                        if(res.confirm){
                            let pages = getCurrentPages();
                            let currentPage = pages[pages.length - 1];
                            currentPage.onLoad()
                        }
                        else if (res.cancel) {
                           wx.navigateBack()
                        }

                    },
                })
                reject(err)
            },
            complete: (e) => {
                // setTimeout(() => {
                    wx.hideLoading()
                // }, 1500)
            }
        })
    })
}
module.exports = {
    request: request,
    createSession: createSession
}
